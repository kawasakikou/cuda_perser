#include <gtkmm.h>
#include <nistk.h>
#include "cmdline.hpp"

int main(int argc, char *argv[])
{
  cmdline::parser a;
  
  a.add<std::string>("compile", 'c', "compile name", true, "");
  a.parse_check(argc, argv);

  std::cout << "../user_app/chapter04/" << a.get<std::string>("compile") << std::endl;

}

