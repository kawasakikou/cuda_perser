#include<gtkmm.h>
#include"th_buttons.h"
#include<iostream>

/*
  ThButtonBaseクラスをメンバとして持ち、NistkThreadクラスを
  継承してウィンドウをthreadとして走らせることが出来るように
  するクラスの実装
 */

// コンストラクタ
ThButtons::ThButtons()
 {
 }

// デストラクタ
ThButtons::~ThButtons()
 {
 }

// スレッドのスロット関数でのmain関数
void ThButtons::nistk_slot_main()
{
  // ここでMain::runを実行してボタンウィンドウを表示実行
  Gtk::Main::run(m_buttons);
}

// ウィンドウ終了フラグをセットする関数
void ThButtons::send_th_end()
{
  sleep(1);
  th_mutex.lock();                  // mutexをロック
  if(in_main == true){              // スロットのmain関数が実行中かどうか？
    m_buttons.set_window_close();   // ここでフラグをセット
  }
  std::cout << "In main slot ID=" << nistk_thread_id << "is end \n";
  th_mutex.unlock();                // mutexを解除
}

// 以下は通常threadを走らせる前にするのでmutexはかけない。
// ボタンウィンドウのタイトルをセットする関数
void ThButtons::set_title(char *m_title)
{
  m_buttons.set_title(m_title);
}

// ウィンドウ番号をセットする関数
void ThButtons::set_num(int num)
{
  m_buttons.set_button_num(num);
}

