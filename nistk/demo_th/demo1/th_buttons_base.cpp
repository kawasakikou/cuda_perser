#include<gtkmm.h>
#include"th_buttons_base.h"
#include<iostream>

/*
  ベースとなるボタンウィンドウクラスをthreadに対応できるベースクラスを
  作る為のクラスの実装。
 */

// コンストラクタ
ThButtonsBase::ThButtonsBase()
{
}

// デストラクタ
ThButtonsBase::~ThButtonsBase()
{
}

// delete buttonが押されたときの処理。押されてもウィンドウを閉じないようにする。
bool ThButtonsBase::on_delete_event(GdkEventAny*)
{
  return true;
}

// タイムアウト処理用関数。今回はすることが無いので空にする。
void ThButtonsBase::time_out_win_func()
{
}
// ウィンドウ終了用関数
void ThButtonsBase::close_window()
{
  button_close();
}
