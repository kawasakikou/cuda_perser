#include"buttons.h"
#include<iostream>

/*
  ベースとなるボタン２つで構成されるウィンドウクラスの実装
 */

// コンストラクタ
Buttons::Buttons()
{
  // ボタンウィジットの初期設定
  m_button.set_label("cool button");
  m_button_close.set_label("close window");
  set_title("Pixmap'd buttons!");
  set_border_width(10);
  // ボタンウィンドウ番号の初期化
  button_num=0;
  // ボタンウィジットとシグナルハンドラの接続
  m_button_close.signal_clicked().connect(sigc::mem_fun(*this,
					  &Buttons::on_button_close));
  m_button.signal_clicked().connect(sigc::mem_fun(*this,
					  &Buttons::on_button_clicked));
  // ボタンをボックスに格納
  m_hbox.pack_start(m_button);
  m_hbox.pack_start(m_button_close);
  // ボックスをウィンドウに格納
  add(m_hbox);
  // ウィンドウを表示（内部的に）
  show_all_children();
}

// デストラクタ
Buttons::~Buttons()
{
  // ウィンドウを消去
  hide();
}

// もう一つのボタンが押されたときの処理関数
void Buttons::on_button_clicked()
{
  std::cout << "The Button" << button_num << " was clicked." << std::endl;
}

// closeボタンが押されたときの処理関数
void Buttons::on_button_close()
{
  hide();
}

// ボタンウィンドウ終了関数
void Buttons::button_close()
{
  hide();
}

// ボタンウィンドウに番号を付ける関数
void Buttons::set_button_num(int num)
{
  button_num = num;
}


