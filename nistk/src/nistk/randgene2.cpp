/**
 * @file  randgene2.cpp
 * @brief class for random generator follow various distribution(実装)
 *
 * @author Masakazu Komori
 * @date 2016-03-22
 * @version $Id: randgene2.cpp,v.20160322 $
 *
 * Copyright (C) 2016 Masakazu Komori
 */

#include<cstdlib>
#include<cmath>
#include<ctime>
#include<randgene2.h>

/** コンストラクタ
 */
Nistk::RandGene2::RandGene2()
{
  srand((unsigned)time(NULL));
}

/** デストラクタ
 */
Nistk::RandGene2::~RandGene2()
{
}

/** 0から1までの実数型乱数の発生
 *
 * 0から1までの実数の乱数を発生させる関数。
 * やっていることはrand関数で発生させた値を
 * RAND_MAXで割っているだけである。
 *
 * @return 0から1までの実数の乱数
 */
double Nistk::RandGene2::get_rand_d()
{
  return ((double)rand())/RAND_MAX;
}

/** 指数分布による乱数の発生
 *
 * ランダムに発生する事象の発生間隔の確率分布である
 * 指数分布に従う乱数列を生成する。指数分布の確率密度関数は
 * 以下の通り。<br>
 \f[
    f(x) = \alpha e^{- \alpha x}
 \f] 
 * この時、期待値は\f$1/\alpha\f$となり、当然ながら
 * \f$x\f$とともに正の値となる。乱数の発生は、一様乱数[0~1]
 * を\f$y\f$とすると逆関数法を使って
 \f[
    F(x) = \int ^{x} _{0} \alpha e^{- \alpha x} dx
 \f] 
 \f[
    F(x) = 1 - e^{- \alpha x}
 \f] 
 * より、
 \f[
    y = 1 - e^{- \alpha x}
 \f] 
 * 両辺自然対数をとって
 \f[
    \ln{(1-y)} =- \alpha x
 \f] 
 \f[
    x = - \frac{1}{\alpha} \ln{(1-y)}
 \f] 
 * となる。
 *
 * @param alpha 期待値の逆数
 * @return 乱数値
 */
double Nistk::RandGene2::get_rand_exp(double alpha)
{
  return (-1.0 / alpha) * log(1 - get_rand_d());
}

/** ポアソン分布による乱数の発生
 * 
 * 単位時間中に平均m回発生する事象がk回発生する
 * 確率であるポアソン分布による乱数を発生する
 * 関数。\f$m\f$、\f$k\f$とも正であり、
 * 離散確率となる。確率分布関数は
 * 以下の通り。<br>
 \f[
    f(k) = e^{-m}\frac{m^k}{k!}
 \f] 
 * 乱数の発生は、一様乱数[0~1]を\f$y\f$とすると
 \f[
    \sum ^{x-1}_{k=0}e^{-m}\frac{m^k}{k!} \leq y < \sum ^{x}_{k=0}e^{-m}\frac{m^k}{k!}
 \f] 
 * を満たす\f$x\f$を求めることにより生成する。
 *
 * @param m 期待値
 * @return 乱数値
 */
int Nistk::RandGene2::get_rand_poisson(double m)
{
  int x;
  double e_m;   // eの-m乗用
  double m_k;   // mのk乗用
  double k;     // kの階乗用
  double rand_num;
  double tmp;

  e_m = exp(-m);
  m_k = 1.0;
  k = 1.0;
  x = 0;
  rand_num = get_rand_d();
  tmp = e_m * (m_k / k);

  for(;;){
    if(rand_num < tmp) break;
    x += 1.0;
    m_k *= m;
    k *= x;
    tmp += e_m * (m_k / k);
  }

  return x;
}

/** アーラン分布による乱数の発生
 * 
 * 待ち行列の待ち時間を計算するための確率分布である
 * アーラン分布による乱数を発生する関数。
 * 現実のデータで、到着間隔やサービス時間が完全にランダムで
 * なく、前の到着に影響を受け、指数分布に従わない場合などに
 * 使う。確率密度関数は次の通り。<br>
 \f[
    f(x;n,\theta) = \frac{\lambda ^nx^{n-1}e^{-\lambda x}}{(n-1)!}
 \f] 
 * ここで、\f$n\f$,\f$x\f$は正の数で、\f$n\f$は生整数である。
 * 累積分布関数は<br>
 \f[
    F(x) = \int ^x_0f(t;n,\lambda )dt = 1-\sum ^{n-1}_{k=0}\frac{(\lambda x)^k}{k!}e^{-\lambda x}
 \f] 
 * となる。指数分布との関係で行けば、互いに独立で同一の指数分布に従う
 * 確率変数の和と関係がある。互いに独立でパラメータ\f$\lambda \f$
 * の指数分布に従う\f$n\f$個の確率変数\f$X_1,X_2,\cdots ,X_n\f$
 * を使えばアーラン分布の確率変数\f$S_n\f$は<br>
 \f[
    S_n = X_1 + X_2 + \cdots + X_n
 \f] 
 * とすれば、パラメータ\f$\lambda \f$,\f$n\f$のアーラン分布に従う。
 * これを使って、
 \f[
     x = \sum ^n_{k=1} t_k=-\frac{1}{\lambda}\sum ^n_{k=1}\ln (1-k_n) = -\frac{1}{\lambda}(\ln \prod ^n_{k=1}(1-k_n))  
 \f] 
 * で求めることができる。期待値は\f$n/\lambda \f$となる。\f$n\f$
 * は位相というらしい。合っているかちょっと不安。。。
 *
 * @param lamda パラメータλ
 * @param n パラメータn
 * @return 乱数値
 */
int Nistk::RandGene2::get_rand_erlang(double lambda, int n)
{
  int x;
  double prod;
  double rand_num;
  int i;

  prod = 1.0;
 
  for(i = 0; i < n; i++){
    rand_num = get_rand_d();
    prod *= (1 - rand_num);
  }
  x = (-1.0 / lambda) * log(prod);

  return x;
}

/** 正規分布による乱数の発生
 * 
 * 期待値\f$\mu\f$、分散\f$\sigma ^2\f$による正規分布
 * に従う乱数を発生する関数。正規分布の確率密度関数は
 * 次のとおり。<br>
 \f[
    f(x) = \frac{1}{\sqrt{2\pi}\sigma}e^{-\frac{(x-\mu )^2}{2\sigma ^2}}
 \f]
 * 逆関数法を使うのは得策ではないので、中心極限定理を使った
 * 方法によって乱数を発生させる。<br><br>
 * 中心極限定理<br> 
 * 平均値\f$\mu ^*\f$、分散\f$\sigma ^{*2}\f$を持つ任意の分布
 * に従う乱数列<br>
 \f[
    X_1,X_2,X_3,\cdots ,X_n
 \f]
 * があるとき、その平均値<br>
 \f[
    \overline{X_n} = \frac{1}{n}(X_1+X_2+\cdots +X_n)
 \f]
 * の確率分布は\f$n\f$が大きくなる時、平均値\f$\mu ^*\f$、分散\f$\sigma ^{*2}/n\f$
 * である正規分布\f$N(\mu ^*,\sigma ^{*2}/n)\f$に収束する。すなわち<br>
 \f[
    \frac{\overline{X_n} - \mu ^*}{\frac{\sigma ^*}{\sqrt{n}}}
 \f]
 * は\f$n\f$が大きい時、平均値0、分散1の正規分布\f$N(0,1)\f$とみなしてよい。
 * <br><br>
 * この時、平均値\f$\mu ^*=1/2\f$、分散\f$\sigma ^{*2}=1/12\f$であるから、
 * [0~1]の一様乱数を使って標準正規分布に従う乱数\f$Z_n\f$は
 \f[
    Z_n = \frac{\overline{X_n} - \frac{1}{2}}{\sqrt{\frac{1}{12n}}}
 \f]
 * で得られる。一様乱数を\f$r_i\f$とすると
 \f[
    Z_n = \sqrt{\frac{12}{n}}(\sum ^n_{i=1}r_i - \frac{n}{2})
 \f]
 * となる。(算術平均の\f$n\f$を総和の外に出している)ここから、任意の
 * 正規分布に変換するには期待値EX、分散VXとすると、
 * \f$z=(x-\mu )/\sigma \f$であるから、
 \f[
    x = \sqrt{VX}\times \sqrt{\frac{12}{n}}(\sum ^n_{i=1}r_i - \frac{n}{2}) + EX
 \f]
 * となる。この方法で\f$n=12\f$の時は、-2乗から-3乗のオーダーの誤差となる。
 * テストしてみるとどうも\f$n=12\f$ではあまり良くないようなのでデフォルトは
 * 240にしてある。
 *
 * @param ex 期待値
 * @param vx 分散
 * @param n 乱数生成のために使う一様乱数の数(デフォルト値240)
 * @return 乱数値
 */
double Nistk::RandGene2::get_rand_gaussian(double ex, double vx, int n)
{
  double x;
  double total_sum;
  double rand_num;
  int i;

  total_sum = 1.0;
 
  for(i = 0; i < n; i++){
    rand_num = get_rand_d();
    total_sum += rand_num;
  }
  x = sqrt(vx) * sqrt(12.0 / (double)n) * (total_sum - ((double)n / 2)) + ex;

  return x;
}

/** 度数分布による乱数の発生
 * 
 * 確率分布関数がわかっていないが度数分布はわかっている場合に
 * 乱数を発生させる場合に一様乱数[0~1]より乱数を発生させる関数。
 * 関数では、累積分布からの逆関数法を用いる。
 * 今、確率変数を\f$x_i\f$、確率値を\f$p_i\f$とし、その
 * 累積分布の値を\f$g_i\f$、一様乱数の値を\f$u_i\f$とすると<br>
 \f[
    \sum ^l_{i=0}g_i \leq u_i < \sum ^{l+1}_{i=0}g_i
 \f]
 * となる\f$l\f$を求めて、区間を線形近似して求めることにする。
 * つまり、<br>
 \f[
    x = \frac{u_i - g_l}{g_{l+1}-g_l}(x_{l+1}-x_l)+x_l
 \f]
 * を使う。度数分布の刻みは一定でなくても良いように
 * してある。度数分布に関しては、当たり前であるが度数ではなく確率値で
 * あることに注意する。その時に、配列の最初は確率値が0となるものを
 * 必ず入れておくように!また、累積分布の最終値が1になっていないものは、
 * 動作は保証しない。
 * <br> br>
 * ちなみに一般的でない確率密度関数\f$f\f$の場合は、同じようにして、
 \f[
    \sum ^l_{i=0}(x_{i+1}-x_i)f_i \leq u_i < \sum ^{l+1}_{i=0}(x_{i+1}-x_i)f_i
 \f]
* となる\f$l\f$を求めて<br>
 \f[
    x = \frac{x_{l+1}-x_l}{2}
 \f]
 * とすることで求めることができる。ここで、線形近似を使っても良いと思う。
 * 
 *
 * @param *x 度数分布の確率変数値
 * @param *p 度数分布の確率値
 * @param num 期待値
 * @return 乱数値
 */
double Nistk::RandGene2::get_rand_hist(double *x, double *p, int num)
{
  double x_rand;
  double rand_num;
  double g_i,g_i2;
  int l;
  int i;

  // lを求める
  rand_num = get_rand_d();
  g_i = p[0];
  for(i = 1; i < num; i++){
    g_i2 = g_i + p[i];
    if(rand_num < g_i2){
      l = i - 1;
      break;
    }
    g_i = g_i2;
  }
  // lより乱数を計算
  x_rand = ((rand_num - g_i) / (g_i2 - g_i)) * (x[l+1] - x[l]) + x[l];

  return x_rand;
}
