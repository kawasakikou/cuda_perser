/**
 * @file  nistk_main_win.cpp
 * @brief class for main window class(実装)
 *
 * @author Masakazu Komori
 * @date 2011-08-30
 * @version $Id: nistk_main_win.cpp,v.20110830 $
 *
 * Copyright (C) 2011 Masakazu Komori
 */

#include<iostream>
#include<gtkmm.h>
#include<nistk/imagedata.h>
#include<nistk/nistk_main_win.h>
#include<nistk/imageview_button.h>
#include<nistk/imagearray_button.h>
#include<nistk/plotview_button.h>

/** コンストラクタ
 */
Nistk::NistkMainWin::NistkMainWin() : 
  image_view_table(1,1),image_array_table(1,1),plot_view_table(1,1)
{
  // 変数初期設定
  image_view_num = 0;
  is_imageview_packed = false;
  image_array_num = 0;
  is_imagearray_packed = false;
  plot_view_num = 0;
  is_plotview_packed = false;

  // ボタンウィジットの初期設定
  m_button_close.set_label("Close");
  m_button_close.set_flags(Gtk::CAN_DEFAULT);

  // Main Windowの初期設定
  set_title("Nistk Main Window");
  set_border_width(10);
  set_size_request(550,300);
  // Image View用の初期設定
  image_view_frame.set_label("Image View");
  image_view_table.set_row_spacings(3);
  image_view_table.set_col_spacings(3);
  image_view_scrowin.set_border_width(10);
  image_view_scrowin.set_policy(Gtk::POLICY_AUTOMATIC,Gtk::POLICY_ALWAYS);
  // Image Array用の初期設定
  image_array_frame.set_label("Image Array");
  image_array_table.set_row_spacings(3);
  image_array_table.set_col_spacings(3);
  image_array_scrowin.set_border_width(10);
  image_array_scrowin.set_policy(Gtk::POLICY_AUTOMATIC,Gtk::POLICY_ALWAYS);
  // Plot View用の初期設定
  plot_view_frame.set_label("Plot View");
  plot_view_table.set_row_spacings(3);
  plot_view_table.set_col_spacings(3);
  plot_view_scrowin.set_border_width(10);
  plot_view_scrowin.set_policy(Gtk::POLICY_AUTOMATIC,Gtk::POLICY_ALWAYS);

  // ボタンウィジットとシグナルハンドラの接続
  m_button_close.signal_clicked().connect(sigc::mem_fun(*this,
					  &NistkMainWin::on_button_close));
  // ボタンをボックスに格納
  // Image View
  image_view_scrowin.add(image_view_table);  
  image_view_frame.add(image_view_scrowin);
  // Image array
  image_array_scrowin.add(image_array_table);  
  image_array_frame.add(image_array_scrowin);
  // Plot View
  plot_view_scrowin.add(plot_view_table);  
  plot_view_frame.add(plot_view_scrowin);
  // Main Window
  get_vbox()->pack_start(m_vbox);
  Gtk::Box *pBox=get_action_area();
  if(pBox) pBox->pack_start(m_button_close);
  show_all_children();
}

/** デストラクタ
 */
Nistk::NistkMainWin::~NistkMainWin()
{
  if(image_view_num !=0) delete[] button_image_view;
  if(image_array_num !=0) delete[] button_image_array;
  if(plot_view_num !=0) delete[] button_plot_view;
  hide();
}

/** closeボタンが押されたときの処理関数
 */
void Nistk::NistkMainWin::on_button_close()
{
  Nistk::ImageView *iv_ptr;
  Nistk::ImageArray *ia_ptr;
  Nistk::PlotView *pv_ptr;
  int i;

  // ImageViewの開いているWidowを閉じる
  for(i = 0; i < image_view_num; i++){
    iv_ptr = button_image_view[i].get_imageview_ptr();
    if(iv_ptr->is_window_show() == true){
      iv_ptr->hide();
      iv_ptr->set_show_window(false);
    }
  }

  // ImageArrayの開いているWidowを閉じる
  for(i = 0; i < image_array_num; i++){
    ia_ptr = button_image_array[i].get_imagearray_ptr();
    if(ia_ptr->is_window_show() == true){
      ia_ptr->hide();
      ia_ptr->set_show_window(false);
    }
  }

  // PlotViewの開いているWidowを閉じる
  for(i = 0; i < plot_view_num; i++){
    pv_ptr = button_plot_view[i].get_plotview_ptr();
    if(pv_ptr->is_window_show() == true){
      pv_ptr->hide();
      pv_ptr->set_show_window(false);
    }
  }

  // MainWindowを閉じる
  hide();
}

/** ImageViewを作成する関数
 *
 * ImageViewを作成する関数。これによって、引数numによって指定される
 * 数のImageViewを作成することができる。同時にメインウィンドウに個数分の
 * ボタンが配置される。第2引数のcol_numは、ボタンがテーブル状に配置される
 * ので、その列の個数を指定している。ただ、col_numは省略することが出来る
 * ようになっている。（デフォルト引数）省略した場合は値が5になる。
 * また、すでにImageViewを作成した状態でこの関数を利用すると、前に
 * 作成したImageViewは削除され、新しく作成されるようになっている。
 *
 * @param num 作成するImageViewの数
 * @param col_num 配置するボタンの列の数
 */
void Nistk::NistkMainWin::create_imageview(int num, int col_num)
{
  int row_num;  // テーブルの行の数
  int i,j,count;

  if((num % col_num) == 0) row_num = num / col_num; // 行の数の計算
  else row_num = (num / col_num) +1;
  if(num < col_num) col_num = num;

  // ボタンの確保と設定
  if(image_view_num !=0) delete[] button_image_view;
  button_image_view = new ImageViewButton[num];
  image_view_num = num;
  image_view_table.resize(row_num, col_num);
  // ボタンのテーブルへの格納
  count = 0;
  for(i = 0; i< row_num; i++){
    for(j = 0; j < col_num; j++){ 
      image_view_table.attach(button_image_view[count],j,j+1,i,i+1);
      count++;
      if(count == num) break;
    }
  }

  // フレームのメインウィンドウへの格納
  if(is_imageview_packed == false){
    m_vbox.pack_start(image_view_frame);
    is_imageview_packed = true;
  }
  show_all_children();
}

/** ImageViewとボタンに名前をセットする関数
 *
 * create_imageview関数で作成されたnum番目のImageViewと対応するボタンに
 * 名前をセットする関数。よって、この関数はImageViewが作成された後に
 * 実行しなければいけない。
 *
 * @param num セットするImageViewの番号（０から始まる）
 * @param name セットする名前のchar型ポインタ
 */
void Nistk::NistkMainWin::set_imageview_name(int num, const char *name)
{
  button_image_view[num].set_name(name);
  show_all_children();
}

/** ImageViewのポインタを取得する関数
 * 
 * create_imageview関数で作成されたnum番目のImageViewの
 * ポインタを取得する関数。この関数でImageViewのポインタを
 * 取得することによって、ImageViewが持っているメンバ関数を
 * 利用することができる。
 *
 * @param num 取得するImageViewの番号（０から始まる）
 * @return ImageView型のポインタ
 */
Nistk::ImageView* Nistk::NistkMainWin::get_imageview_ptr(int num)
{
  return button_image_view[num].get_imageview_ptr();
}

/** NistkMainWinからImageViewのフレーム取り除く関数
 *
 * NistkMainWinからImageViewのフレーム取り除く関数。
 * create_imageviewによって作成されたボタンを格納
 * したフレームをNistkMainWinの表示から取り除く関数。
 * これを実行したからといってボタンとImageViewが
 * deleteされるのではない。add_imageviewによって
 * 再度追加することができる。もともと、NistkMainWin
 * にフレームがない場合は何もしない。
 *
 */
void Nistk::NistkMainWin::remove_imageview()
{
  if(is_imageview_packed == true){
    m_vbox.remove(image_view_frame); 
    is_imageview_packed = false;
  }
  return ;
}

/** NistkMainWinにImageViewのフレーム追加する関数
 *
 * NistkMainWinにImageViewのフレーム追加する関数。
 * create_imageviewによって作成されたボタンを格納
 * したフレームをNistkMainWinの表示に追加する。
 * remove_imageviewを使わない限り使わない関数。
 * create_imageviewを実行した時に自動で追加されるからである。
 * もともとフレームがある時は何もしない。
 *
 */
void Nistk::NistkMainWin::add_imageview()
{
  if(is_imageview_packed == false){
    m_vbox.pack_start(image_view_frame); 
    is_imageview_packed = true;
  }
  return ;
}

/** ImageViewの数を取得する関数
 *
 * create_imageviewによって作成されたImageViewの
 * 個数を取得する関数。
 *
 * @return 作成したImageViewの個数
 */
int Nistk::NistkMainWin::get_imageview_num()
{
  return image_view_num;
}

/** ImageArrayを作成する関数
 *
 * ImageArrayを作成する関数。これによって、引数numによって指定される
 * 数のImageArrayを作成することができる。同時にメインウィンドウに個数分の
 * ボタンが配置される。第2引数のcol_numは、ボタンがテーブル状に配置される
 * ので、その列の個数を指定している。ただ、col_numは省略することが出来る
 * ようになっている。（デフォルト引数）省略した場合は値が5になる。
 * また、すでにImageArrayを作成した状態でこの関数を利用すると、前に
 * 作成したImageArrayは削除され、新しく作成されるようになっている。
 *
 * @param num 作成するImageArrayの数
 * @param col_num 配置するボタンの列の数
 */
void Nistk::NistkMainWin::create_imagearray(int num, int col_num)
{
  int row_num;  // テーブルの行の数
  int i,j,count;

  if((num % col_num) == 0) row_num = num / col_num; // 行の数の計算
  else row_num = (num / col_num) +1;
  if(num < col_num) col_num = num;

  // ボタンの確保と設定
  if(image_array_num !=0) delete[] button_image_array;
  button_image_array = new ImageArrayButton[num];
  image_array_num = num;
  image_array_table.resize(row_num, col_num);
  // ボタンのテーブルへの格納
  count = 0;
  for(i = 0; i< row_num; i++){
    for(j = 0; j < col_num; j++){ 
      image_array_table.attach(button_image_array[count],j,j+1,i,i+1);
      count++;
      if(count == num) break;
    }
  }

  // フレームのメインウィンドウへの格納
  if(is_imagearray_packed == false){
    m_vbox.pack_start(image_array_frame);
    is_imagearray_packed = true;
  }
  show_all_children();
}

/** ImageArrayとボタンに名前をセットする関数
 *
 * create_imagearray関数で作成されたnum番目のImageArrayと対応するボタンに
 * 名前をセットする関数。よって、この関数はImageArrayが作成された後に
 * 実行しなければいけない。
 *
 * @param num セットするImageArrayの番号（０から始まる）
 * @param name セットする名前のchar型ポインタ
 */
void Nistk::NistkMainWin::set_imagearray_name(int num, const char *name)
{
  button_image_array[num].set_name(name);
  show_all_children();
}

/** ImageArrayのポインタを取得する関数
 * 
 * create_imagearray関数で作成されたnum番目のImageArrayの
 * ポインタを取得する関数。この関数でImageArrayのポインタを
 * 取得することによって、ImageArrayが持っているメンバ関数を
 * 利用することができる。
 *
 * @param num 取得するImageArrayの番号（０から始まる）
 * @return ImageArray型のポインタ
 */
Nistk::ImageArray* Nistk::NistkMainWin::get_imagearray_ptr(int num)
{
  return button_image_array[num].get_imagearray_ptr();
}

/** NistkMainWinからImageArrayのフレーム取り除く関数
 *
 * NistkMainWinからImageArrayのフレーム取り除く関数。
 * create_imagearrayによって作成されたボタンを格納
 * したフレームをNistkMainWinの表示から取り除く関数。
 * これを実行したからといってボタンとImagearrayが
 * deleteされるのではない。add_imagearrayによって
 * 再度追加することができる。もともと、NistkMainWin
 * にフレームがない場合は何もしない。
 *
 */
void Nistk::NistkMainWin::remove_imagearray()
{
  if(is_imagearray_packed == true){
    m_vbox.remove(image_array_frame); 
    is_imagearray_packed = false;
  }
  return ;
}

/** NistkMainWinにImageArrayのフレーム追加する関数
 *
 * NistkMainWinにImageArrayのフレーム追加する関数。
 * create_imagearrayによって作成されたボタンを格納
 * したフレームをNistkMainWinの表示に追加する。
 * remove_imagearrayを使わない限り使わない関数。
 * create_imagearrayを実行した時に自動で追加されるからである。
 * もともとフレームがある時は何もしない。
 *
 */
void Nistk::NistkMainWin::add_imagearray()
{
  if(is_imagearray_packed == false){
    m_vbox.pack_start(image_array_frame); 
    is_imagearray_packed = true;
  }
  return ;
}

/** ImageArrayの数を取得する関数
 *
 * create_imagearrayによって作成されたImageArrayの
 * 個数を取得する関数。
 *
 * @return 作成したImageArrayの個数
 */
int Nistk::NistkMainWin::get_imagearray_num()
{
  return image_array_num;
}

/** PlotViewを作成する関数
*
 * PlotViewを作成する関数。これによって、引数numによって指定される
 * 数のPlotViewを作成することができる。同時にメインウィンドウに個数分の
 * ボタンが配置される。第2引数のcol_numは、ボタンがテーブル状に配置される
 * ので、その列の個数を指定している。ただ、col_numは省略することが出来る
 * ようになっている。（デフォルト引数）省略した場合は値が5になる。
 * また、すでにPlotViewを作成した状態でこの関数を利用すると、前に
 * 作成したPlotViewは削除され、新しく作成されるようになっている。
 *
 * @param num 作成するPlotViewの数
 * @param col_num 配置するボタンの列の数
 */
void Nistk::NistkMainWin::create_plotview(int num, int col_num)
{
  int row_num;  // テーブルの行の数
  int i,j,count;

  if((num % col_num) == 0) row_num = num / col_num; // 行の数の計算
  else row_num = (num / col_num) +1;
  if(num < col_num) col_num = num;


  // ボタンの確保と設定
  if(plot_view_num !=0) delete[] button_plot_view;
  button_plot_view = new PlotViewButton[num];
  plot_view_num = num;
  plot_view_table.resize(row_num, col_num);
  // ボタンのテーブルへの格納
  count = 0;
  for(i = 0; i< row_num; i++){
    for(j = 0; j < col_num; j++){ 
      plot_view_table.attach(button_plot_view[count],j,j+1,i,i+1);
      count++;
      if(count == num) break;
    }
  }

  // フレームのメインウィンドウへの格納
  if(is_plotview_packed == false){
    m_vbox.pack_start(plot_view_frame);
    is_plotview_packed = true;
  }

  show_all_children();
}

/** PlotViewとボタンに名前をセットする関数
*
 * create_plotview関数で作成されたnum番目のPlotViewと対応するボタンに
 * 名前をセットする関数。よって、この関数はPlotViewが作成された後に
 * 実行しなければいけない。
 *
 * @param num セットするPlotViewの番号（０から始まる）
 * @param name セットする名前のchar型ポインタ
 */
void Nistk::NistkMainWin::set_plotview_name(int num, const char *name)
{
  button_plot_view[num].set_name(name);
  show_all_children();
}

/** plotViewのポインタを取得する関数
* 
 * create_plotview関数で作成されたnum番目のPlotViewの
 * ポインタを取得する関数。この関数でPlotViewのポインタを
 * 取得することによって、PlotViewが持っているメンバ関数を
 * 利用することができる。
 *
 * @param num 取得するPlotViewの番号（０から始まる）
 * @return PlotView型のポインタ
 */
Nistk::PlotView* Nistk::NistkMainWin::get_plotview_ptr(int num)
{
  return button_plot_view[num].get_plotview_ptr();
}

/** num番目のPlotViewの位置x,yのグラフのPlotDataのポインタを取得する関数  
 * 
 * create_plotview関数で作成されたnum番目のPlotViewの位置x,yの
 * グラフのPlotDataのポインタを取得する関数。この関数でPlotDataのポインタを
 * 取得することによって、PlotDataが持っているメンバ関数を
 * 利用することができる。
 *
 * @param num 取得するPlotDataのPlotViewの番号（０から始まる）
 * @param x 取得するPlotDataポインタのx位置
 * @param y 取得するPlotDataポインタのy位置
 * @return num番目のPlotViewの位置(x,y)のグラフに対応するPlotDataのポインタ
 */
Nistk::PlotData* Nistk::NistkMainWin::pd_ptr(int num, int x, int y)
{
  return button_plot_view[num].get_plotview_ptr()->pd_ptr(x,y);
}

/** NistkMainWinからPlotViewのフレーム取り除く関数
 *
 * NistkMainWinからPlotViewのフレーム取り除く関数。
 * create_plotviewによって作成されたボタンを格納
 * したフレームをNistkMainWinの表示から取り除く関数。
 * これを実行したからといってボタンとPlotViewが
 * deleteされるのではない。add_plotviewによって
 * 再度追加することができる。もともと、NistkMainWin
 * にフレームがない場合は何もしない。
 *
 */
void Nistk::NistkMainWin::remove_plotview()
{
  if(is_plotview_packed == true){
    m_vbox.remove(plot_view_frame); 
    is_plotview_packed = false;
  }
  return ;
}

/** NistkMainWinにplotViewのフレーム追加する関数
 *
 * NistkMainWinにplotViewのフレーム追加する関数。
 * create_plotviewによって作成されたボタンを格納
 * したフレームをNistkMainWinの表示に追加する。
 * remove_plotviewを使わない限り使わない関数。
 * create_plotviewを実行した時に自動で追加されるからである。
 * もともとフレームがある時は何もしない。
 *
 */
void Nistk::NistkMainWin::add_plotview()
{
  if(is_plotview_packed == false){
    m_vbox.pack_start(plot_view_frame); 
    is_plotview_packed = true;
  }
  return ;
}

/** PlotViewの数を取得する関数
*
 * create_plotviewによって作成されたPlotViewの
 * 個数を取得する関数。
 *
 * @return 作成したPlotViewの個数
 */
int Nistk::NistkMainWin::get_plotview_num()
{
  return plot_view_num;
}
