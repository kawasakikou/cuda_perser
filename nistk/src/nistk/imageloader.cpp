/**
 * @file  imageloader.cpp
 * @brief class for imageloader of simulation(実装)
 *
 * @author Masakazu Komori
 * @date 2011-08-09
 * @version $Id: imageloader.cpp,v.20110809 $
 *
 * Copyright (C) 2008-2011 Masakazu Komori
 */

#include<imageloader.h>
#include<stdio.h>

/** コンストラクタ 引数無し
 */
Nistk::ImageLoader::ImageLoader()
{
  f_num = 0;
  f_count = 0;
}

/** コンストラクタ 引数(リストファイル名)
 *
 * 生成と同時にリストを読み込む
 *
 * @param f 読み込むイメージのファイルリストのファイル名
 */
Nistk::ImageLoader::ImageLoader(const char *f)
{
  set_input_file(f);
}

/** コピーコンストラクタ
 */
Nistk::ImageLoader::ImageLoader(const Nistk::ImageLoader &obj)
{
  int i,f_length;

  f_num = obj.f_num;     // f_numをコピー
  f_count = obj.f_count; // f_countをコピー
  i_file = obj.i_file;   // i_fileをコピー

  // 入力ファイル名をコピー
  // for(i=0; i<f_num; i++) f_name[i] = obj.f_name[i];
  f_name = obj.f_name;

  // リストファイル名をコピー
  f_length = obj.i_file.length();
  lf_name = new char[f_length+1];
  for(i=0; i< f_length; i++) lf_name[i] = obj.i_file[i];
  lf_name[f_length] = '\0';

  // 最初の読み込みファイル名を文字列にセット
  f_length = obj.f_name[0].length();
  if_name = new char[f_length+1];
  for(i=0; i< f_length; i++) if_name[i] = obj.f_name[0][i];
  if_name[f_length] = '\0';

}

/** デストラクタ
 */
Nistk::ImageLoader::~ImageLoader()
{
  delete[] lf_name;
  delete[] if_name;
}

/** 代入演算子=のオーバーロード
 */
Nistk::ImageLoader &Nistk::ImageLoader::operator=(Nistk::ImageLoader &obj)
{
  int i,f_length;

  this->f_num = obj.f_num;     // f_numをコピー
  this->f_count = obj.f_count; // f_countをコピー
  this->i_file = obj.i_file;   // i_fileをコピー

  // 入力ファイル名をコピー
  // for(i=0; i<f_num; i++) f_name[i] = obj.f_name[i];
  this->f_name = obj.f_name;

  // リストファイル名をコピー
  delete[] this->lf_name;
  f_length = obj.i_file.length();
  this->lf_name = new char[f_length+1];
  for(i=0; i< f_length; i++) this->lf_name[i] = obj.i_file[i];
  this->lf_name[f_length] = '\0';

  // 最初の読み込みファイル名を文字列にセット
  delete[] this->lf_name;
  f_length = obj.f_name[0].length();
  this->if_name = new char[f_length+1];
  for(i=0; i< f_length; i++) this->if_name[i] = obj.f_name[0][i];
  this->if_name[f_length] = '\0';

  return *this;
}

/** 入力ファイルリストをセットする関数
 *
 * @param f 読み込むイメージのファイルリストのファイル名
 */
void Nistk::ImageLoader::set_input_file(const char *f)
{
  int i;
  std::string buf;
  int f_length;
  int c_check=0; // コメント用チェッカ
  
  // リストファイル名をstring型のi_fileにセットし、ファイルを開く
  i_file = f;
  std::ifstream fin(i_file.c_str());
  while(1){
    fin >> buf;
    if(c_check == 2) c_check = 0; // コメントが終了しているかチェック
    // コメント開始チェック
    if((buf[0] == '/') && (buf[1] == '*')){
      c_check = 1;  
    }
    // コメント終了チェック
    if((buf[buf.length()-2] == '*') && (buf[buf.length()-1] == '/')) {
      c_check = 2;  
    } 
    if(c_check == 0){
      f_num = atoi(buf.c_str());
      f_name.resize(f_num);
      break;
    }
  }

  // 配列のサイズをセットし、ファイル名を取得してカウンタを0にセット
  f_name.resize(f_num);
  i=0;
  while(i<f_num){
    fin >> buf;
    if(c_check == 2) c_check = 0; // コメントが終了しているかチェック
    // コメント開始チェック
    if((buf[0] == '/') && (buf[1] == '*')){
      c_check = 1;  
    }
    // コメント終了チェック
    if((buf[buf.length()-2] == '*') && (buf[buf.length()-1] == '/')) {
      c_check = 2;  
    } 
    if(c_check == 0){
       f_name[i] = buf;
       i++;
    }
  }
  fin.close();
  f_count = 0;
  
  // リストファイル名を文字列にセット
  // stringではうまいこといかないので
  f_length = i_file.length();
  lf_name = new char[f_length+1];
  for(i=0; i< f_length; i++) lf_name[i] = i_file[i];
  lf_name[f_length] = '\0';

  // 最初の読み込みファイル名を文字列にセット
  // stringではうまいこといかないので
  f_length = f_name[0].length();
  if_name = new char[f_length+1];
  for(i=0; i< f_length; i++) if_name[i] = f_name[0][i];
  if_name[f_length] = '\0';

  return;
}

/** リストファイル名を返す関数
 *
 *  @return リストファイル名
 */
char* Nistk::ImageLoader::get_list_file_name()
{
  return lf_name;
}

/** 読み込みファイル名を返す関数
 *
 * 読み込んだファイルのファイル名を返す
 * 関数。
 *
 *  @return 読み込んだファイル名
 */
char* Nistk::ImageLoader::get_input_file_name()
{
  return if_name;
}

/** 読み込みファイル数を返す関数
 *
 * リストにかかれているファイル数を返す
 * 関数。つまり、ImageLoaderがいくつ
 * ファイルを読み込めば最初に戻るか
 * を示すことになる。
 *
 *  @return リストにかかれているファイル数
 */
int Nistk::ImageLoader::get_input_file_num()
{
  return f_num;
}

/** 今、何番目のファイルか(0から始まる)を返す関数
 *
 *  @return 現在何番目のファイルか
 */
int Nistk::ImageLoader::get_input_file_count()
{
  int tmp;
  
  if((f_count-1) < 0) tmp = f_num-1;
  else tmp = f_count-1;

  return tmp;
}

/** 読み込みカウンタをリセット
 *
 * 次に読み込むファイルカウンタを0に
 * セットする。つまり、先頭に戻る。
 *
 */
void Nistk::ImageLoader::count_reset()
{
  f_count = 0;
}

/** 読み込みファイルを一つスキップする関数
 *
 * 次に読み込むファイルを読まずに飛ばす。
 * 最後であれば先頭に戻る。
 *
 */
void Nistk::ImageLoader::skip_input_file()
{
  f_count++;
  if(f_count == f_num) f_count = 0;
}

/** 次のイメージを読む関数
 *
 *  @return 読み込まれたイメージのGlib::RefPtr<Gdk::Pixbuf>型ポインタ
 */
Glib::RefPtr<Gdk::Pixbuf> Nistk::ImageLoader::load_image_next()
{
  Glib::RefPtr<Gdk::Pixbuf> i_buf;
  int f_length;

  i_buf = Gdk::Pixbuf::create_from_file(f_name[f_count].c_str());
  
  // ファイル名をセットしなおす。
  f_length = f_name[f_count].length();
  delete[] if_name; 
  if_name = new char[f_length+1];
  for(int i=0; i< f_length; i++) if_name[i] = f_name[f_count][i];
  if_name[f_length] = '\0';
  
  // 読み込みカウンタを更新
  f_count++;
  if(f_count == f_num) f_count = 0;

  return i_buf;
}
