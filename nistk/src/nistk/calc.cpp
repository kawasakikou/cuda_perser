/**
 * @file  calc.cpp
 * @brief class for calculation tool(実装)
 *
 * @author Masakazu Komori
 * @date 2013-03-15
 * @version $Id: calc.cpp,v.20130315 $
 *
 * Copyright (C) 2010-2013 Masakazu Komori
 */

#include<calc.h>
#include<simdata.h>
#include<simdataarray.h>

/** SimData型の入力データの位置x,yを中心として荷重和を取る関数
 *
 * \image html w_sum.jpg
 * \image latex w_sum.eps "calculation image of weighted_sum"
 * <br>
 * SimData型の二次元入力データinの位置x,y（入力データの左上を0,0とする）
 * を中心として、SimData型の二次元荷重データweightに対する荷重和を取る関数。
 * 具体的には次の計算をする。 wheightの高さをh,幅をwとして、その中心を
 * \f$h_c=h/2\f$,\f$w_c=w/2\f$とすると、<br>
 \f[
    \sum^{h/2}_{i=-h/2} \sum^{w/2}_{j=-w/2} weight(wc+j,hc+i)*in(x+j,y+i)
 \f] 
 *
 * となる。ここで、weight(x,y)は荷重データの中心を原点としており、in(x,y)
 * は位置x,yを中心とした座標である。式より分るように荷重データの高さと幅は
 * 奇数となっていないと中心が二次元データの中心に来ないので注意が必要である。
 * また、計算がinおよびweightの範囲を超てしまう場合は、その部分に関しての
 * 計算を行わないようにしてあるので注意する必要がある。
 * 
 * @param *in 二次元入力データのNistk::SimData型ポインタ
 * @param *weight 二次元荷重データのNistk::SimData型ポインタ
 * @param x 二次元入力データ上の左上を原点としたx座標
 * @param y 二次元入力データ上の左上を原点としたy座標
 * @return 荷重和
 */
double Nistk::Calc::weighted_sum(Nistk::SimData *in, Nistk::SimData *weight,
                                                                 int x, int y)
{
  int in_height;      // 入力二次元データの高さ
  int in_width;       // 入力二次元データの幅
  int w_height;       // 二次元荷重データの高さ
  int w_width;        // 二次元荷重データの幅
  int wc_x;           // 二次元荷重データの中心のx座標 
  int wc_y;           // 二次元荷重データの中心のy座標
  double w_sum;       // 荷重和
  int i,j;

  // in,weightの高さと幅の取得
  in_height = in->get_height();
  in_width = in->get_width();
  w_height = weight->get_height();
  w_width = weight->get_width();
  // weightの中心位置の計算
  wc_x = w_width/2;
  wc_y = w_height/2;

  // 荷重和の計算
  w_sum = 0.0;
  // yに関するループ
  for(i = -1*(int)(w_height/2); i <= (int)(w_height/2); i++){
    // yの範囲がオーバーしていないか
    if(((y+i) >= 0) && ((y+i) < in_height) && ((wc_y+i) >= 0) 
                                           && ((wc_y+i) < w_height)){
      // xに関するループ
      for(j = -1*(int)(w_width/2); j <= (int)(w_width/2); j++){
	// xの範囲がオーバーしていないか
	if(((x+j) >= 0) && ((x+j) < in_width) && ((wc_x+j) >= 0) 
 	                                      && ((wc_x+j) < w_width)){
	  w_sum += weight->get_data(wc_x+j,wc_y+i) * in->get_data(x+j,y+i);
	}
      }
    }
  }

  return w_sum;
}

/** SimData型の入力データの位置x,yを中心として荷重和を取る関数2
 *
 * SimData型の二次元入力データinの位置x,y（入力データの左上を0,0とする）
 * を中心として、SimData型の二次元荷重データweightに対する荷重和を取る関数
 * weighted_sumの計算する二次元荷重データの計算範囲を指定するバージョン。
 * 自己組織化マップでの学習の際に結合荷重の範囲が変化させる場合があることから、
 * この関数を作った。
 * 具体的には次の計算をする。 wheightの高さをh,幅をwとして、その中心を
 * \f$h_c=h/2\f$,\f$w_c=w/2\f$とすると、<br>
 \f[
    \sum^{wy_{end}}_{i=wy_{start}} \sum^{wx_{end}}_{j=wx_{start}} weight(wc+j,hc+i)*in(x+j,y+i)
 \f] 
 *
 * となる。ここで、weight(x,y)は荷重データの中心を原点としており、in(x,y)
 * は位置x,yを中心とした座標である。また、wx_start,wx_end,wy_start,wy_end
 * は荷重データの中心を原点とした時のx,yの開始位置と終了位置を表している。
 * 式より分るように荷重データの高さと幅は
 * 奇数となっていないと中心が二次元データの中心に来ないので注意が必要である。
 * また、計算がinおよびweightの範囲を超てしまう場合は、その部分に関しての
 * 計算を行わないようにしてあるので注意する必要がある。
 * 
 * @param *in 二次元入力データのNistk::SimData型ポインタ
 * @param *weight 二次元荷重データのNistk::SimData型ポインタ
 * @param x 二次元入力データ上の左上を原点としたx座標
 * @param y 二次元入力データ上の左上を原点としたy座標
 * @param wx_start 二次元荷重データの中心を原点とした時の計算開始x座標
 * @param wx_end 二次元荷重データの中心を原点とした時の計算終了x座標
 * @param wy_start 二次元荷重データの中心を原点とした時の計算開始y座標
 * @param wy_end 二次元荷重データの中心を原点とした時の計算終了y座標
 * @return 荷重和
 */
double Nistk::Calc::weighted_sum2(Nistk::SimData *in, Nistk::SimData *weight,
				  int x, int y, int wx_start, int wx_end, 
			                             int wy_start, int wy_end)
{
  int in_height;      // 入力二次元データの高さ
  int in_width;       // 入力二次元データの幅
  int w_height;       // 二次元荷重データの高さ
  int w_width;        // 二次元荷重データの幅
  int wc_x;           // 二次元荷重データの中心のx座標 
  int wc_y;           // 二次元荷重データの中心のy座標
  double w_sum;       // 荷重和
  int i,j;

  // in,weightの高さと幅の取得
  in_height = in->get_height();
  in_width = in->get_width();
  w_height = weight->get_height();
  w_width = weight->get_width();
  // weightの中心位置の計算
  wc_x = w_width/2;
  wc_y = w_height/2;

  // 荷重和の計算
  w_sum = 0.0;
  // yに関するループ
  for(i = wy_start; i <= wy_end; i++){
    // yの範囲がオーバーしていないか
    if(((y+i) >= 0) && ((y+i) < in_height) && ((wc_y+i) >= 0) 
                                           && ((wc_y+i) < w_height)){
      // xに関するループ
      for(j = wx_start; j <= wx_end; j++){
	// xの範囲がオーバーしていないか
	if(((x+j) >= 0) && ((x+j) < in_width) && ((wc_x+j) >= 0) 
 	                                      && ((wc_x+j) < w_width)){
	  w_sum += weight->get_data(wc_x+j,wc_y+i) * in->get_data(x+j,y+i);
	}
      }
    }
  }

  return w_sum;
}


/** SimData型の入力データに対して同一の荷重で荷重和を計算する関数
 *
 * \image html input_single_weight.png
 * \image latex input_single_weight.eps "calculation image"
 * <br>
 * SimData型の二次元入力データinの位置start_x,start_y（入力データの左上を
 * 0,0とする）から同一のSimData型の二次元荷重データweightを使って
 * 位置をstepずつずらしながら荷重和を取り、出力用SimData型に計算結果を格納する関数。
 * 荷重和計算には関数weighted_sumを使っているので、最初の位置start_x,start_y
 * は、最初の計算の荷重データの中心を原点となるので、二次元荷重データのサイズ
 * を考慮して設定しないと入力データの範囲からはみ出るので注意が必要である。
 * 計算は出力用SimData型のoutのサイズに合せて行うので、これもそれぞれの
 * サイズを考えて設定する必要がある。荷重データの高さと幅は
 * 奇数となっていないと中心が二次元データの中心に来ないので注意が必要である。
 * また、もし仮に計算の中心位置がinの範囲を超てしまう場合は、その部分に関しての
 * 計算を行わず0.0が入力されるようにしてあるので注意する必要がある。
 * 
 * @param *in 二次元入力データのNistk::SimData型ポインタ
 * @param *weight 二次元荷重データのNistk::SimData型ポインタ
 * @param *out 計算結果格納用のNistk::SimData型ポインタ
 * @param start_x 二次元入力データ上の左上を原点とした計算開始x座標
 * @param start_y 二次元入力データ上の左上を原点とした計算開始y座標
 * @param step 二次元入力データ上の座標を幾つ飛ばしで荷重和計算するか
 */
void Nistk::Calc::input_single_weight(Nistk::SimData *in, 
			 Nistk::SimData *weight, Nistk::SimData *out, 
                                   int start_x, int start_y, int step)
{
  int in_height;      // 入力二次元データの高さ
  int in_width;       // 入力二次元データの幅
  int out_height;     // 出力二次元データの高さ
  int out_width;      // 出力二次元データの幅
  int x,y;            // 入力上の位置
  double tmp;
  int i,j;

  // in,outの高さと幅の取得
  in_height = in->get_height();
  in_width = in->get_width();
  out_height = out->get_height();
  out_width = out->get_width();

  for(i = 0; i < out_height; i++){  // y
    // 入力上での位置yの計算
    y = step * i + start_y;
    // yに関して範囲をチェック
    if((y >= 0) && (y < in_height)){
      for(j = 0; j < out_width; j++){  // x
	// 入力上での位置xの計算
	x = step * j + start_x;
	// xが範囲内であれば荷重和を計算して格納
	if((x >= 0) && (x < in_width))
	  tmp = Nistk::Calc::weighted_sum(in, weight, x, y);
	else tmp = 0.0;
	out->put_data(j,i,tmp);
      }
    }
    else{
      for(j = 0; j < out_width; j++){
	tmp = 0.0;
	out->put_data(j,i,tmp);
      }
    }
  }

  return;
}

/** SimData型の入力データに対してSimDataArray型の荷重で荷重和を計算する関数
 *
 * SimData型の二次元入力データinの位置start_x,start_y（入力データの左上を
 * 0,0とする）からSimDataArray型の二次元荷重データweightを使って
 * 位置をstepずつずらしながら荷重和を取り、出力用SimData型に計算結果を格納する関数。
 * 荷重がSimDataArrayになっていることにより、input_single_weight関数と違って
 * それぞれの位置に対応する異なる二次元荷重データを使って荷重をとる。
 * それ以外はinput_single_weight関数と同じである。
 * 荷重和計算には関数weighted_sumを使っているので、最初の位置start_x,start_y
 * は、最初の計算の荷重データの中心を原点となるので、二次元荷重データのサイズ
 * を考慮して設定しないと入力データの範囲からはみ出るので注意が必要である。
 * 計算は出力用SimData型のoutのサイズに合せて行うので、これもそれぞれの
 * サイズを考えて設定する必要がある。荷重データの高さと幅は
 * 奇数となっていないと中心が二次元データの中心に来ないので注意が必要である。
 * また、もし仮に計算の中心位置がinの範囲を超てしまう、もしくはweightの範囲を超てしまう
 * 場合は、その部分に関しての計算を行わず0.0が入力されるようにしてあるので注意する必要がある。
 * 
 * @param *in 二次元入力データのNistk::SimData型ポインタ
 * @param *weight 二次元荷重データのNistk::SimDataArray型ポインタ
 * @param *out 計算結果格納用のNistk::SimData型ポインタ
 * @param start_x 二次元入力データ上の左上を原点とした計算開始x座標
 * @param start_y 二次元入力データ上の左上を原点とした計算開始y座標
 * @param step 二次元入力データ上の座標を幾つ飛ばしで荷重和計算するか
 */
void Nistk::Calc::input_array_weight(Nistk::SimData *in, 
		      Nistk::SimDataArray *weight, Nistk::SimData *out, 
				        int start_x, int start_y, int step)
{
  int in_height;      // 入力二次元データの高さ
  int in_width;       // 入力二次元データの幅
  int out_height;     // 出力二次元データの高さ
  int out_width;      // 出力二次元データの幅
  int array_height;   // 荷重用SimDataArray型の配列の高さ
  int array_width;    // 荷重用SimDataArray型の配列の幅
  int x,y;            // 入力上の位置
  double tmp;
  int i,j;

  // in,out,weight配列の高さと幅の取得
  in_height = in->get_height();
  in_width = in->get_width();
  out_height = out->get_height();
  out_width = out->get_width();
  array_height = weight->get_height();
  array_width = weight->get_width();

  for(i = 0; i < out_height; i++){  // y
    // 入力上でのyの位置計算
    y = step * i + start_y;
    if((y >= 0) && (y < in_height) && (i < array_height)){
      for(j = 0; j < out_width; j++){  // x
	// 入力上でのxの位置計算
	x = step * j + start_x;
	// 中心位置が範囲内であれば荷重和を計算して格納
	if((x >= 0) && (x < in_width) && (j < array_width))
	  tmp = Nistk::Calc::weighted_sum(in, 
					  weight->get_simdata_ptr(j,i), x, y);
	else tmp = 0.0;
	out->put_data(j,i,tmp);
      }
    }
    else{
      for(j = 0; j < out_width; j++){
	tmp = 0.0;
	out->put_data(j,i,tmp);
      }
    }
  }

  return;
}
