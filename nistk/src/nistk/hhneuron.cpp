/**
 * @file  hhneuron.cpp
 * @brief class for Hodgikin-Huxley model(実装)
 *
 * @author Masakazu Komori
 * @date 2011-08-01
 * @version $Id: hhneuron.cpp,v.20110801 $
 *
 * Copyright (C) 2011 Masakazu Komori
 */

#include<cmath>
#include<nistk/runge.h>
#include<nistk/hhneuron.h>
#include<nistk/hhparameter.h>
#include<nistk/hhvariable.h>

/** Nistk::Neuron::HHVariable,Nistk::Neuron::HHParameterの初期化用関数
 *
 * Nistk::Neuron::HHVariable,Nistk::Neuron::HHParameterの初期化用関数。<br>
 * Nistk::Neuron::HHVariable及び
 * Nistk::Neuron::HHParameterを利用する場合には、
 * 必ずこの関数で初期化する必要がある。この関数では、
 * まず、
 * Nistk::Neuron::HHParameterのT_scale
 * を設定温度により計算し、C_m,G_m,G_Na,G_K
 * をC_m_bar,G_m_bar,G_Na_bar,G_K_bar,area_sizeにより値を計算している。
 * \f$V_L\f$に関しては、
 \f[
      V_L - V_{rest} = 
      (-G_{Na}m(0)^3h(0)(E_{Na} - V_{rest})-G_Kh(0)^4(E_K - V_{rest})/G_m
 \f]
 \f[
      m(0) = \frac{\alpha_m(0)}{\alpha_m(0) + \beta_m(0)}
 \f]
 \f[
      h(0) = \frac{\alpha_h(0)}{\alpha_h(0) + \beta_h(0)}
 \f]
 \f[
      n(0) = \frac{\alpha_n(0)}{\alpha_n(0) + \beta_n(0)}
 \f]
 * を使い、計算により求めている。ここで、m(0),h(0),n(0)は、V=0,
 * dm/dt=dh/dt=dn/dt=0より決定される。
 * 次に、HHVariabelについては、V=V_rest,I_Na=I_K=I_leak=I_inj=0とし、
 * m=m(0),h=h(0),n=n(0)としている。
 *
 * @param *hhv Hodgikin-Huxley modelの変数クラスのポインタ
 * @param *hhp Hodgikin-Huxley modelのパラメータクラスのポインタ
 */
void Nistk::Neuron::HHNeuron::init(Nistk::Neuron::HHVariable *hhv, 
				   Nistk::Neuron::HHParameter *hhp)
{
  double a0,b0;          // α(0),β(0)の値用
  double m0,h0,n0;       // V=0,d○/dt=0の時のm,h,nの値
  double GNa_0,GK_0;     // G_Na(0),G_K(0)の値
  
  // HHParameterの初期化
  // 設定温度によるα、βの係数の計算
  hhp->T_scale = pow(3.0, (hhp->T-6.3)/10);
  // 計算対象の膜容量の計算
  hhp->C_m = hhp->C_m_bar * hhp->area_size;
  // 計算対象の最大膜コンダクタンスの計算
  hhp->G_m = hhp->G_m_bar * hhp->area_size;
  // 計算対象のNaイオンチャネル最大コンダクタンスの計算
  hhp->G_Na = hhp->G_Na_bar * hhp->area_size;
  // 計算対象のKイオンチャネル最大コンダクタンスの計算
  hhp->G_K = hhp->G_K_bar * hhp->area_size;
  // Leakの反転電位の計算
  // m
  a0 = Nistk::Neuron::HHNeuron::calc_alpha_m(0.0, hhp);
  b0 = Nistk::Neuron::HHNeuron::calc_beta_m(0.0, hhp);
  m0 = a0 /(a0 + b0);
  // h
  a0 = Nistk::Neuron::HHNeuron::calc_alpha_h(0.0, hhp);
  b0 = Nistk::Neuron::HHNeuron::calc_beta_h(0.0, hhp);
  h0 = a0 /(a0 + b0);
  // n
  a0 = Nistk::Neuron::HHNeuron::calc_alpha_n(0.0, hhp);
  b0 = Nistk::Neuron::HHNeuron::calc_beta_n(0.0, hhp);
  n0 = a0 /(a0 + b0);
  GNa_0 = hhp->G_Na * pow(m0, 3) * h0;
  GK_0 = hhp->G_K * pow(n0, 4);
  hhp->V_L = -1.0 
    * ((GNa_0 * (hhp->E_Na - hhp->V_rest) + GK_0 * (hhp->E_K -hhp->V_rest))) / hhp->G_m;
  hhp->V_L = hhp->V_L + hhp->V_rest;

  // HHVariableの初期設定
  // 膜電位V
  hhv->V = hhp->V_rest;
  // Naイオン電流
  hhv->I_Na = 0.0;
  // Kイオン電流
  hhv->I_K = 0.0;
  // leak電流
  hhv->I_leak = 0.0;
  // 入力電流
  hhv->I_inj = 0.0;
  // イオンチャネルの開口状態m
  hhv->m = m0;
  // イオンチャネルの開口状態h
  hhv->h = h0;
  // イオンチャネルの開口状態n
  hhv->n = n0;

  return;
}

/** Nistk::Neuron::HHParameterのm,h,n,α,βのMSAV単位への変換
 *
 * \f$\alpha \f$,\f$\beta \f$,\f$m\f$,\f$h\f$,\f$n\f$
 * のパラメータの単位を電圧[V],電流[A],長さ[m],時間[s],容量[F],
 * コンダクタンス[S]にあうように変換する関数。詳しくは、
 * クラスの説明を参照すること。
 *
 * @param *hhp Hodgikin-Huxley modelのパラメータクラスのポインタ
 */
void Nistk::Neuron::HHNeuron::convert_mhn_to_MSAV(Nistk::Neuron::HHParameter *hhp)
{
  double scale;

  scale = pow(10,-3);
  hhp->mhn_scale = 1000.0;
  hhp->am_1 = hhp->am_1 * scale;
  hhp->am_2 = hhp->am_2 * scale;
  hhp->am_3 = hhp->am_3 * scale;
  hhp->am_4 = hhp->am_4 * scale;
  hhp->bm_2 = hhp->bm_2 * scale;
  hhp->ah_2 = hhp->ah_2 * scale;
  hhp->bh_2 = hhp->bh_2 * scale;
  hhp->bh_3 = hhp->bh_3 * scale;
  hhp->an_1 = hhp->an_1 * scale;
  hhp->an_2 = hhp->an_2 * scale;
  hhp->an_3 = hhp->an_3 * scale;
  hhp->an_4 = hhp->an_4 * scale;
  hhp->bn_2 = hhp->bn_2 * scale;

  return;
}

/** Nistk::Temp::Runge4SiによるHodgikin-Huxley modelを計算する関数
 *
 * Nistk::Temp::Runge4SiによるHodgikin-Huxley modelを計算する関数。<br>
 * 4次のルンゲクッタ法を用いてHodgikin-Huxley modelの1ステップ
 * (dt)を計算する。計算は4元連立微分方程式になるので、ルンゲクッタ法
 * の計算には
 * Nistk::Temp::Runge4Siを用いる。また、内部での計算では、
 * 静止膜電位からの相対電位を使って計算する。
 *
 * @param dt 時間刻み幅
 * @param *hhv Hodgikin-Huxley modelの変数クラスのポインタ
 * @param *hhp Hodgikin-Huxley modelのパラメータクラスのポインタ
 */
void Nistk::Neuron::HHNeuron::calc(double dt, Nistk::Neuron::HHVariable *hhv, 
				   Nistk::Neuron::HHParameter *hhp)
{
  double val[4];     // ルンゲクッタ法用に変数V,m,h,nを格納する変数
  // ルンゲクッタ用関数ポインタの配列
  double (*func[4])(double dt, double *val, 
		    Nistk::Neuron::HHVariable *hhv, 
		    Nistk::Neuron::HHParameter *hhp) = {
                                &Nistk::Neuron::HHNeuron::calc_v,
				&Nistk::Neuron::HHNeuron::calc_m_si,
				&Nistk::Neuron::HHNeuron::calc_h_si, 
				&Nistk::Neuron::HHNeuron::calc_n_si};
  // HHVariableの変数V,m,h,nを配列valに格納
  val[0] = hhv->V - hhp->V_rest;  // 静止膜電位からの相対電位
  val[1] = hhv->m;                // イオンチャネルの開口状態(開口確率)を表す成分m
  val[2] = hhv->h;                // イオンチャネルの開口状態(開口確率)を表す成分h
  val[3] = hhv->n;                // イオンチャネルの開口状態(開口確率)を表す成分n
  // ルンゲクッタ法を用いて計算(2番目の引数dtはダミー)
  Nistk::Temp::Runge4Si<Nistk::Neuron::HHVariable*, 
    Nistk::Neuron::HHParameter*>(func, dt, val, hhv, hhp, dt, 4);
  // HHVariableの変数の値を格納
  hhv->V = val[0] + hhp->V_rest;  // 相対電位を元に戻す
  hhv->m = val[1];
  hhv->h = val[2];
  hhv->n = val[3];
  hhv->I_Na = hhp->G_Na * pow(hhv->m,3) * hhv->h * (hhv->V - hhp->E_Na);
  hhv->I_K = hhp->G_K * pow(hhv->n,4) * (hhv->V - hhp->E_K);
  hhv->I_leak = hhp->G_m * (hhv->V - hhp->V_L);

  return;
}

/** 膜電位Vの微分値計算用関数
 *
 * 膜電位Vの微分値計算用関数。<br>
 * 膜電位Vの微分値
 \f[
      \frac{dV}{dt} = (G_{Na}m^3h((E_{Na}-V_{rest}) -V) 
                       + G_Kn^4((E_K-V_{rest}) -V) 
                       + G_m((V_L-V_{rest}) -V) + I_{inj}(t)) / C_m
 \f]
 * を計算する。膜電位は静止膜電位からの相対電位であるので注意すること。
 * よって、\f$E_{Na}\f$,\f$E_K\f$,\f$V_L\f$は、\f$V_{rest}\f$からの
 * 相対電位になっている。
 * この関数は、膜電位Vの微分値計算を
 * Nistk::Neuron::HHNeuron::calc等で利用できるようにしたものである。
 * 2番目の引数*valは<br>
 * <br>
 * val[0]:V - V_rest (静止膜電位からの相対電位)<br>
 * val[1]:m          (イオンチャネルの開口状態(開口確率)を表す成分m)<br>
 * val[2]:h          (イオンチャネルの開口状態(開口確率)を表す成分h)<br>
 * val[3]:n          (イオンチャネルの開口状態(開口確率)を表す成分n)<br>
 * <br>
 * となっているので注意すること。
 * また、膜電位は、膜電位単体を
 * Nistk::Temp::Runge4で利用できるように
 * しても、m,h,nが必要となるので、それ用の関数は作っていない。
 *
 * @param dt ダミー(使用しない) 
 * @param *val 変数V,m,h,nを格納する変数のポインタ
 * @param *hhv Hodgikin-Huxley modelの変数クラスのポインタ
 * @param *hhp Hodgikin-Huxley modelのパラメータクラスのポインタ
 * @return 膜電位の微分値
 */
double Nistk::Neuron::HHNeuron::calc_v(double dt, double *val, 
				       Nistk::Neuron::HHVariable *hhv, 
				       Nistk::Neuron::HHParameter *hhp)
{
  double diff;       // 微分値用

  diff = (hhp->G_Na * pow(val[1],3) * val[2] * ((hhp->E_Na - hhp->V_rest) - val[0])
	  + hhp->G_K * pow(val[3],4) * ((hhp->E_K - hhp->V_rest) - val[0])
	  + hhp->G_m * ((hhp->V_L - hhp->V_rest) - val[0])
	  + hhv->I_inj) / hhp->C_m;

  return diff;
}


/** イオンチャネルの開口状態(開口確率)を表す成分mの微分値Si用関数
 *
 * イオンチャネルの開口状態(開口確率)を表す成分mの微分値Si用関数。<br>
 * イオンチャネルの開口状態(開口確率)を表す成分mの微分値
 \f[
      \frac{dm}{dt} = \alpha _m(V)(1-m) - \beta _m(V)m
 \f]
 * を計算する。膜電位は静止膜電位からの相対電位であるので注意すること。
 * また、この関数は、イオンチャネルの開口状態(開口確率)を表す成分mを
 * Nistk::Neuron::HHNeuron::calc等で利用できるようにしたものである。
 * 2番目の引数*valは<br>
 * <br>
 * val[0]:V - V_rest (静止膜電位からの相対電位)<br>
 * val[1]:m          (イオンチャネルの開口状態(開口確率)を表す成分m)<br>
 * val[2]:h          (イオンチャネルの開口状態(開口確率)を表す成分h)<br>
 * val[3]:n          (イオンチャネルの開口状態(開口確率)を表す成分n)<br>
 * <br>
 * となっているので注意すること。関数内では、
 * Nistk::Neuron::HHNeuron::calc_mを呼んでいるだけである。
 *
 * @param dt ダミー(使用しない) 
 * @param *val 変数V,m,h,nを格納する変数のポインタ
 * @param *hhv Hodgikin-Huxley modelの変数クラスのポインタ(ダミー、使用しない)
 * @param *hhp Hodgikin-Huxley modelのパラメータクラスのポインタ
 * @return イオンチャネルの開口状態(開口確率)を表す成分mの微分値
 */
double Nistk::Neuron::HHNeuron::calc_m_si(double dt, double *val, 
					  Nistk::Neuron::HHVariable *hhv, 
					  Nistk::Neuron::HHParameter *hhp)
{
  return Nistk::Neuron::HHNeuron::calc_m(dt, val[1], val[0], hhp);
}

/** イオンチャネルの開口状態(開口確率)を表す成分hの微分値Si用関数
 *
 * イオンチャネルの開口状態(開口確率)を表す成分hの微分値Si用関数。<br>
 * イオンチャネルの開口状態(開口確率)を表す成分hの微分値
 \f[
      \frac{dh}{dt} = \alpha _h(V)(1-h) - \beta _h(V)h
 \f]
 * を計算する。膜電位は静止膜電位からの相対電位であるので注意すること。
 * また、この関数は、イオンチャネルの開口状態(開口確率)を表す成分hを
 * Nistk::Neuron::HHNeuron::calc等で利用できるようにしたものである。
 * 2番目の引数*valは<br>
 * <br>
 * val[0]:V - V_rest (静止膜電位からの相対電位)<br>
 * val[1]:m          (イオンチャネルの開口状態(開口確率)を表す成分m)<br>
 * val[2]:h          (イオンチャネルの開口状態(開口確率)を表す成分h)<br>
 * val[3]:n          (イオンチャネルの開口状態(開口確率)を表す成分n)<br>
 * <br>
 * となっているので注意すること。関数内では、
 * Nistk::Neuron::HHNeuron::calc_hを呼んでいるだけである。
 *
 * @param dt ダミー(使用しない) 
 * @param *val 変数V,m,h,nを格納する変数のポインタ
 * @param *hhv Hodgikin-Huxley modelの変数クラスのポインタ(ダミー、使用しない)
 * @param *hhp Hodgikin-Huxley modelのパラメータクラスのポインタ
 * @return イオンチャネルの開口状態(開口確率)を表す成分hの微分値
 */
double Nistk::Neuron::HHNeuron::calc_h_si(double dt, double *val, 
					  Nistk::Neuron::HHVariable *hhv, 
					  Nistk::Neuron::HHParameter *hhp)
{
  return Nistk::Neuron::HHNeuron::calc_h(dt, val[2], val[0], hhp);
}

/** イオンチャネルの開口状態(開口確率)を表す成分nの微分値Si用関数
 *
 * イオンチャネルの開口状態(開口確率)を表す成分nの微分値Si用関数。<br>
 * イオンチャネルの開口状態(開口確率)を表す成分nの微分値
 \f[
      \frac{dn}{dt} = \alpha _n(V)(1-n) - \beta _n(V)n
 \f]
 * を計算する。膜電位は静止膜電位からの相対電位であるので注意すること。
 * また、この関数は、イオンチャネルの開口状態(開口確率)を表す成分nを
 * Nistk::Neuron::HHNeuron::calc等で利用できるようにしたものである。
 * 2番目の引数*valは<br>
 * <br>
 * val[0]:V - V_rest (静止膜電位からの相対電位)<br>
 * val[1]:m          (イオンチャネルの開口状態(開口確率)を表す成分m)<br>
 * val[2]:h          (イオンチャネルの開口状態(開口確率)を表す成分h)<br>
 * val[3]:n          (イオンチャネルの開口状態(開口確率)を表す成分n)<br>
 * <br>
 * となっているので注意すること。関数内では、
 * Nistk::Neuron::HHNeuron::calc_nを呼んでいるだけである。
 *
 * @param dt ダミー(使用しない) 
 * @param *val 変数V,m,h,nを格納する変数のポインタ
 * @param *hhv Hodgikin-Huxley modelの変数クラスのポインタ(ダミー、使用しない)
 * @param *hhp Hodgikin-Huxley modelのパラメータクラスのポインタ
 * @return イオンチャネルの開口状態(開口確率)を表す成分nの微分値
 */
double Nistk::Neuron::HHNeuron::calc_n_si(double dt, double *val, 
					  Nistk::Neuron::HHVariable *hhv, 
					  Nistk::Neuron::HHParameter *hhp)
{
  return Nistk::Neuron::HHNeuron::calc_n(dt, val[3], val[0], hhp);
}

/** イオンチャネルの開口状態(開口確率)を表す成分mの微分値計算用関数
 *
 * イオンチャネルの開口状態(開口確率)を表す成分mの微分値計算用関数。<br>
 * イオンチャネルの開口状態(開口確率)を表す成分mの微分値
 \f[
      \frac{dm}{dt} = \alpha _m(V)(1-m) - \beta _m(V)m
 \f]
 * を計算する。膜電位は静止膜電位からの相対電位であるので注意すること。
 * また、この関数は、イオンチャネルの開口状態(開口確率)を表す成分m単体を
 * Nistk::Temp::Runge4で利用できるようにしたものである。
 *
 * @param dt ダミー(使用しない) 
 * @param m イオンチャネルの開口状態(開口確率)を表す成分mの現在値
 * @param v 膜電位(静止膜電位からの相対電位)
 * @param *hhp Hodgikin-Huxley modelのパラメータクラスのポインタ
 * @return イオンチャネルの開口状態(開口確率)を表す成分mの微分値
 */
double Nistk::Neuron::HHNeuron::calc_m(double dt, double m, double v, 
				       Nistk::Neuron::HHParameter *hhp)
{
  double diff;       // 微分値用
  double alpha_m;    // 電圧依存係数α用
  double beta_m;     // 電圧依存係数β用

  // 微分値の計算 α,βは温度による係数T_scaleをかける必要がある
  alpha_m = Nistk::Neuron::HHNeuron::calc_alpha_m(v, hhp) * hhp->T_scale;
  beta_m = Nistk::Neuron::HHNeuron::calc_beta_m(v, hhp) * hhp->T_scale;
  diff = (alpha_m * (1 - m) - beta_m * m) * hhp->mhn_scale; 

  return diff;
}

/** イオンチャネルの開口状態(開口確率)を表す成分hの微分値計算用関数
 *
 * イオンチャネルの開口状態(開口確率)を表す成分hの微分値計算用関数。<br>
 * イオンチャネルの開口状態(開口確率)を表す成分hの微分値
 \f[
      \frac{dh}{dt} = \alpha _h(V)(1-h) - \beta _h(V)h
 \f]
 * を計算する。膜電位は静止膜電位からの相対電位であるので注意すること。
 * また、この関数は、イオンチャネルの開口状態(開口確率)を表す成分h単体を
 * Nistk::Temp::Runge4で利用できるようにしたものである。
 *
 * @param dt ダミー(使用しない) 
 * @param h イオンチャネルの開口状態(開口確率)を表す成分hの現在値
 * @param v 膜電位(静止膜電位からの相対電位)
 * @param *hhp Hodgikin-Huxley modelのパラメータクラスのポインタ
 * @return イオンチャネルの開口状態(開口確率)を表す成分hの微分値
 */
double Nistk::Neuron::HHNeuron::calc_h(double dt, double h, double v, 
				       Nistk::Neuron::HHParameter *hhp)
{
  double diff;       // 微分値用
  double alpha_h;    // 電圧依存係数α用
  double beta_h;     // 電圧依存係数β用

  // 微分値の計算 α,βは温度による係数T_scaleをかける必要がある
  alpha_h = Nistk::Neuron::HHNeuron::calc_alpha_h(v, hhp) * hhp->T_scale;
  beta_h = Nistk::Neuron::HHNeuron::calc_beta_h(v, hhp) * hhp->T_scale;
  diff = (alpha_h * (1 - h) - beta_h * h) * hhp->mhn_scale; 

  return diff;
}

/** イオンチャネルの開口状態(開口確率)を表す成分nの微分値計算用関数
 *
 * イオンチャネルの開口状態(開口確率)を表す成分nの微分値計算用関数。<br>
 * イオンチャネルの開口状態(開口確率)を表す成分nの微分値
 \f[
      \frac{dn}{dt} = \alpha _n(V)(1-n) - \beta _n(V)n
 \f]
 * を計算する。膜電位は静止膜電位からの相対電位であるので注意すること。
 * また、この関数は、イオンチャネルの開口状態(開口確率)を表す成分n単体を
 * Nistk::Temp::Runge4で利用できるようにしたものである。
 *
 * @param dt ダミー(使用しない) 
 * @param n イオンチャネルの開口状態(開口確率)を表す成分nの現在値
 * @param v 膜電位(静止膜電位からの相対電位)
 * @param *hhp Hodgikin-Huxley modelのパラメータクラスのポインタ
 * @return イオンチャネルの開口状態(開口確率)を表す成分nの微分値
 */
double Nistk::Neuron::HHNeuron::calc_n(double dt, double n, double v, 
				       Nistk::Neuron::HHParameter *hhp)
{
  double diff;       // 微分値用
  double alpha_n;    // 電圧依存係数α用
  double beta_n;     // 電圧依存係数β用

  // 微分値の計算 α,βは温度による係数T_scaleをかける必要がある
  alpha_n = Nistk::Neuron::HHNeuron::calc_alpha_n(v, hhp) * hhp->T_scale;
  beta_n = Nistk::Neuron::HHNeuron::calc_beta_n(v, hhp) * hhp->T_scale;
  diff = (alpha_n * (1 - n) - beta_n * n) * hhp->mhn_scale; 

  return diff;
}

/** イオンチャネルの電圧依存係数α_mを計算する関数
 *
 * イオンチャネルの電圧依存係数α_mを計算する関数。<br>
 * イオンチャネルの電圧依存係数α_m
 \f[
   \alpha _m(V) = \frac{am_1-V}{am_2(e^{(am_3-V)/am_4}-am_5)}
 \f]
 * を計算する。係数am_iは
 * Nistk::Neuron::HHParameterの値を使用する。また、膜電位は
 * 静止膜電位からの相対電位であるので注意すること。この関数は、よく見ると
 * 分母が0となる場合がある。そこで、苦肉の策であるが、分母が0となった際は、
 * 関数値を\f$v\pm 10^{-12}\f$で計算して、その平均値を間数値としている。
 *
 * @param v 膜電位(静止膜電位からの相対電位)
 * @param *hhp Hodgikin-Huxley modelのパラメータクラスのポインタ
 * @return イオンチャネルの電圧依存係数α_mの値
 */
double Nistk::Neuron::HHNeuron::calc_alpha_m(double v, 
					     Nistk::Neuron::HHParameter *hhp)
{
  double f1;     // 分子 hhp->am_1 - v
  double f2;     // 分母 hhp->am_2 * (exp((hhp->am_3 - v) / hhp->am_4) - hhp->am_5)
  double ret;    // 戻り値
  double f1_1,f1_2; // 分子バッファ
  double f2_1,f2_2; // 分母バッファ
  double v_buf;
  
  f1 = hhp->am_1 - v;
  f2 = hhp->am_2 * (exp((hhp->am_3 - v) / hhp->am_4) - hhp->am_5);

  if (f2 == 0.0){
    v_buf = v - pow(10.0, -12);
    f1_1 = hhp->am_1 - v_buf;
    f2_1 = hhp->am_2 * (exp((hhp->am_3 - v_buf) / hhp->am_4) - hhp->am_5);
    v_buf = v + pow(10.0, -12);
    f1_2 = hhp->am_1 - v_buf;
    f2_2 = hhp->am_2 * (exp((hhp->am_3 - v_buf) / hhp->am_4) - hhp->am_5);
    ret = ((f1_1 / f2_1) + (f1_2 / f2_2)) / 2.0;
  }
  else ret = f1 / f2;
  return ret;
}

/** イオンチャネルの電圧依存係数β_mを計算する関数
 *
 * イオンチャネルの電圧依存係数β_mを計算する関数。<br>
 * イオンチャネルの電圧依存係数β_m
 \f[
      \beta _m(V) = bm_1e^{-V/bm_2}
 \f]
 * を計算する。係数bm_iは
 * Nistk::Neuron::HHParameterの値を使用する。また、膜電位は
 * 静止膜電位からの相対電位であるので注意すること。
 *
 * @param v 膜電位(静止膜電位からの相対電位)
 * @param *hhp Hodgikin-Huxley modelのパラメータクラスのポインタ
 * @return イオンチャネルの電圧依存係数β_mの値
 */
double Nistk::Neuron::HHNeuron::calc_beta_m(double v, 
					    Nistk::Neuron::HHParameter *hhp)
{
  return hhp->bm_1 * exp((-1.0 * v) / hhp->bm_2);
}

/** イオンチャネルの電圧依存係数α_hを計算する関数
 *
 * イオンチャネルの電圧依存係数α_hを計算する関数。<br>
 * イオンチャネルの電圧依存係数α_h
 \f[
      \alpha _h(V) = ah_1e^{-V/ah_2}
 \f]
 * を計算する。係数ah_iは
 * Nistk::Neuron::HHParameterの値を使用する。また、膜電位は
 * 静止膜電位からの相対電位であるので注意すること。
 *
 * @param v 膜電位(静止膜電位からの相対電位)
 * @param hhp Hodgikin-Huxley modelのパラメータクラスのポインタ
 * @return イオンチャネルの電圧依存係数α_hの値
 */
double Nistk::Neuron::HHNeuron::calc_alpha_h(double v, 
					     Nistk::Neuron::HHParameter *hhp)
{
  return hhp->ah_1 * exp((-1.0 * v) / hhp->ah_2);
}

/** イオンチャネルの電圧依存係数β_hを計算する関数
 *
 * イオンチャネルの電圧依存係数β_hを計算する関数。<br>
 * イオンチャネルの電圧依存係数β_h
 \f[
      \beta _h(V) = \frac{bh_1}{e^{(bh_2-V)/bh_3}+bh_4}
 \f]
 * を計算する。係数bh_iは
 * Nistk::Neuron::HHParameterの値を使用する。また、膜電位は
 * 静止膜電位からの相対電位であるので注意すること。
 *
 * @param v 膜電位(静止膜電位からの相対電位)
 * @param *hhp Hodgikin-Huxley modelのパラメータクラスのポインタ
 * @return イオンチャネルの電圧依存係数β_hの値
 */
double Nistk::Neuron::HHNeuron::calc_beta_h(double v, 
					    Nistk::Neuron::HHParameter *hhp)
{
  return hhp->bh_1 / (exp((hhp->bh_2 - v) / hhp->bh_3) + hhp->bh_4);
}

/** イオンチャネルの電圧依存係数α_nを計算する関数
 *
 * イオンチャネルの電圧依存係数α_nを計算する関数。<br>
 * イオンチャネルの電圧依存係数α_n
 \f[
      \alpha _n(V) = \frac{an_1-V}{an_2(e^{(an_3-V)/an_4}-an_5)}
 \f]
 * を計算する。係数an_iは
 * Nistk::Neuron::HHParameterの値を使用する。また、膜電位は
 * 静止膜電位からの相対電位であるので注意すること。この関数は、よく見ると
 * 分母が0となる場合がある。そこで、苦肉の策であるが、分母が0となった際は、
 * 関数値を\f$v\pm 10^{-12}\f$で計算して、その平均値を間数値としている。
 *
 * @param v 膜電位(静止膜電位からの相対電位)
 * @param *hhp Hodgikin-Huxley modelのパラメータクラスのポインタ
 * @return イオンチャネルの電圧依存係数α_nの値
 */
double Nistk::Neuron::HHNeuron::calc_alpha_n(double v, 
					     Nistk::Neuron::HHParameter *hhp)
{
  double f1;     // 分子 hhp->an_1 - v
  double f2;     // 分母 hhp->an_2 * (exp((hhp->an_3 - v) / hhp->an_4) - hhp->an_5)
  double ret;    // 戻り値
  double f1_1,f1_2; // 分子バッファ
  double f2_1,f2_2; // 分母バッファ
  double v_buf;

  f1 = hhp->an_1 - v;
  f2 = hhp->an_2 * (exp((hhp->an_3 - v) / hhp->an_4) - hhp->an_5);

  if (f2 == 0.0){
    v_buf = v - pow(10.0, -12);
    f1_1 = hhp->an_1 - v_buf;
    f2_1 = hhp->an_2 * (exp((hhp->an_3 - v_buf) / hhp->an_4) - hhp->an_5);
    v_buf = v + pow(10.0, -12);
    f1_2 = hhp->an_1 - v_buf;
    f2_2 = hhp->an_2 * (exp((hhp->an_3 - v_buf) / hhp->an_4) - hhp->an_5);
    ret = ((f1_1 / f2_1) + (f1_2 / f2_2)) / 2.0;
  }
  else ret = f1 / f2;
  return ret;
}

/** イオンチャネルの電圧依存係数β_nを計算する関数
 *
 * イオンチャネルの電圧依存係数β_nを計算する関数。<br>
 * イオンチャネルの電圧依存係数β_n
 \f[
      \beta _n(V) = bn_1e^{-V/bn_2}
 \f]
 * を計算する。係数bn_iは
 * Nistk::Neuron::HHParameterの値を使用する。また、膜電位は
 * 静止膜電位からの相対電位であるので注意すること。
 *
 * @param v 膜電位(静止膜電位からの相対電位)
 * @param *hhp Hodgikin-Huxley modelのパラメータクラスのポインタ
 * @return イオンチャネルの電圧依存係数β_nの値
 */
double Nistk::Neuron::HHNeuron::calc_beta_n(double v, Nistk::Neuron::HHParameter *hhp)
{
  return hhp->bn_1 * exp((-1.0 * v) / hhp->bn_2);
}



