/**
 * @file  ivparameter.cpp
 * @brief class for Parameter of Izhikevich's Simple Spiking Neuron(実装)
 *
 * @author Masakazu Komori
 * @date 2011-08-05
 * @version $Id: ivparameter.cpp,v.20110805 $
 *
 * Copyright (C) 2011 Masakazu Komori
 */

#include<nistk/ivparameter.h>

/** コンストラクタ
 */
Nistk::Neuron::IVParameter::IVParameter()
{
  a = 0.02;        // Simple Spiking Neuron modelのパラメータa (初期値:0.02)
  b = 0.2;         // Simple Spiking Neuron modelのパラメータb (初期値:0.2)
  c = -65;         // Simple Spiking Neuron modelのパラメータc (初期値:-65[mV])
  d = 2.0;         // Simple Spiking Neuron modelのパラメータd (初期値:2)
  v_rest = 30.0;   // 膜電位リセットがかかる電位v_rest (初期値:30[mV])
  vp_1 = 0.04;     // 膜電位vに関する式のパラメータvp_1 (初期値:0.04)
  vp_2 = 5.0;      // 膜電位vに関する式のパラメータvp_2 (初期値:5)
  vp_3 = 140.0;    // 膜電位vに関する式のパラメータvp_3 (初期値:140)
  vp_4 = 1.0;      // 膜電位vに関する式のパラメータvp_4 (初期値:1.0)
  vp_5 = 0.0;      // 膜電位vに関する式のパラメータvp_5 (初期値:0.0)
  vp_6 = 1.0;      // 膜電位vに関する式のパラメータvp_6 (初期値:1.0)
  vp_7 = 0.0;      // 膜電位vに関する式のパラメータvp_7 (初期値:0.0)
  up_1 = 0.0;      // リカバリ変数に関する式のパラメータup_1 (初期値:0.0)
  up_2 = 1.0;      // リカバリ変数に関する式のパラメータup_2 (初期値:1.0)
  up_3 = 1.0;      // リカバリ変数に関する式のパラメータup_3 (初期値:1.0)
}

/** デストラクタ
 */
Nistk::Neuron::IVParameter::~IVParameter()
{
}
