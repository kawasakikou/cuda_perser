/**
 * @file  mathtools2.cpp
 * @brief class for math tools2(実装)
 *
 * @author Masakazu Komori
 * @date 2013-04-01
 * @version $Id: mathtools2.cpp,v.20130401 $
 *
 * Copyright (C) 2011-2013 Masakazu Komori
 */
#include<cmath>
#include<cstring>
#include<nistk/simdata.h>
#include<nistk/mathtools.h>

/** 楕円形ガウス関数(3D)
 *
 * 楕円形のガウス関数(グラフを書くと3D)である以下の式を計算して返す関数。
 * ゲインであるaを指定できるようにしてある。<br>
 \f[
    f(x)=a\, exp^{-\frac{[(x-x_c)cos(\phi)-(y-y_c)sin(\phi)]^2}{\sigma_1^2}
                  -\frac{[(x-x_c)sin(\phi)+(y-y_c)cos(\phi)]^2}{\sigma_2^2}}
 \f] 
 * 注意：上式をよく見ればわかるが、計算は、計算点を回転させて、角度が0の時の
 * 楕円に対して計算しているので、元の計算点から見ると、時計回りに回転させた
 * 事になるので注意が必要である。
 *
 * @param x 入力(x軸)
 * @param y 入力(y軸)
 * @param x_c ガウス関数の中心位置(x軸)
 * @param y_c ガウス関数の中心位置(y軸)
 * @param gain ゲイン(a)
 * @param sigma1 幅(\f$\sigma_1 \f$)
 * @param sigma2 幅(\f$\sigma_2 \f$)
 * @param angle 角度(単位：ラジアン　\f$0 \leq \phi <\pi \f$)
 * @return 関数値
 */
double Nistk::MathTools::e_gaussian(double x, double y, 
		  double x_c, double y_c, double gain, double sigma1,
		  double sigma2, double angle)
{
  double f1, f2, ret;

  f1 = ((x - x_c) * cos(angle)) - ((y - y_c) * sin(angle));
  f1 = (f1 * f1) / (sigma1 * sigma1);
  f2 = ((x - x_c) * sin(angle)) + ((y - y_c) * cos(angle));
  f2 = (f2 * f2) / (sigma2 * sigma2);
  ret = gain * exp(-1.0 * (f1 + f2));

  return ret;
}


/** 楕円形Gaussianを計算しSimData型に格納する関数(3D)
 *
 * 楕円形のガウス関数(グラフを書くと3D)である
 * 以下の式を計算してSimData型に格納する関数。ゲインであるaを
 * 指定できるようにしてある。原点は,SimData型の二次元配列の中心になる。
 * 単純に縦と横を2で割る(整数演算)だけなので,偶数のときは原点が
 * ずれることになるので注意が必要である。(例えば,7だと3,10だと5となる。
 * 配列は0からなので,奇数は中心,偶数は+1ずれる。)x_step,y_stepを1以外
 * の値にすると,計算では要素間の距離が1以外になる。(配列の添字は整数値で
 * 扱うことになるが。。）通常は,1で良いかと思う。計算上は,そのまま式を計算
 * するが配列の範囲外の値は無視される。
 * 
 * <br>
 \f[
    f(x)=a\, exp^{-\frac{[(x-x_c)cos(\phi)-(y-y_c)sin(\phi)]^2}{\sigma_1^2}
                  -\frac{[(x-x_c)sin(\phi)+(y-y_c)cos(\phi)]^2}{\sigma_2^2}}
 \f]
 * 注意：上式をよく見ればわかるが、計算は、計算点を回転させて、角度が0の時の
 * 楕円に対して計算しているので、元の計算点から見ると、時計回りに回転させた
 * 事になるので注意が必要である。
 * <br>
 * 計算モードopは<br>
 * "none": 計算値をSimDataにそのまま格納<br> 
 * "max" : 計算値とSimDataの値を比較した時の最大値を格納<br>
 * "min" : 計算値とSimDataの値を比較した時の最小値を格納<br>
 * "+"   : 計算値とSimDataの値を加算したものを格納<br>
 * "-"   : 計算値からSimDataの値を減算したものを格納<br>
 * "*"   : 計算値とSimDataの値を乗算したものを格納<br>
 * "/"   : 計算値をSimDataの値で除算したものを格納<br>
 *
 * @param i_data データを格納するNistk::SimData型のポインタ
 * @param x_c ガウス関数の中心位置(x軸)
 * @param y_c ガウス関数の中心位置(y軸)
 * @param x_step 刻み幅(x軸)
 * @param y_step 刻み幅(y軸)
 * @param gain ゲイン(a)
 * @param sigma1 幅(\f$\sigma 1 \f$)
 * @param sigma2 幅(\f$\sigma 2 \f$)
 * @param angle 角度(単位：ラジアン　\f$0 \leq \phi <\pi \f$)
 * @param op 計算モード
 */
void Nistk::MathTools::simdata_from_e_gaussian(Nistk::SimData *i_data,
			     double x_c, double y_c,
			     double x_step, double y_step,
			     double gain, double sigma1, double sigma2, 
			     double angle, const char *op)
{
  int col,row;             // 縦横のカウンタ
  int x_origin,y_origin;   // SimData型上での原点
  int width,height;        // SimData型上の幅と高さ
  double xd_start,yd_start;  // SimData型配列起点のでのx,y値
  double x_val,y_val;      // 計算上のx,yの値
  double val;              // Gaussian2の値
  double s_val;            // SimDataの値
  double v_sin;            // sinの値
  double v_cos;            // cosの値
  double f1, f2;

  int mode;
  
  if(strcmp(op, "none") == 0) mode = 0;
  else if(strcmp(op,"max") == 0) mode = 1;
  else if(strcmp(op,"min") == 0) mode = 2;
  else if(strcmp(op,"+") == 0) mode = 3;
  else if(strcmp(op,"-") == 0) mode = 4;
  else if(strcmp(op,"*") == 0) mode = 5;
  else if(strcmp(op,"/") == 0) mode = 6;

  // sin,cosの計算
  v_sin = sin(angle);
  v_cos = cos(angle);
  // SimData型の幅と高さを取得
  width = i_data->get_width();
  height = i_data->get_height();
  // SimData型の2次元配列上での原点を計算
  x_origin = width/2;
  y_origin = height/2;
  // 原点から見た左上角のx,y値
  xd_start = -(double)x_origin * x_step;
  yd_start = (double)y_origin * y_step;

  // DoGを計算してSimData型に格納
  x_val = xd_start; // 計算開始点のセット
  y_val = yd_start;
  for(row=0; row < height; row++){ // 縦
    for(col=0; col < width; col++){ // 横
      // gaussianを計算
      /*
      val = e_gaussian(x_val, y_val, x_c, y_c, gain, 
		       sigma1, sigma2, angle);
      */
      f1 = ((x_val - x_c) * v_cos) - ((y_val - y_c) * v_sin);
      f1 = (f1 * f1) / (sigma1 * sigma1);
      f2 = ((x_val - x_c) * v_sin) + ((y_val - y_c) * v_cos);
      f2 = (f2 * f2) / (sigma2 * sigma2);
      val = gain * exp(-1.0 * (f1 + f2));
      // モードをみて格納
      if(mode == 0){  
	i_data->put_data(col, row, val);
      } else if(mode == 1){
	s_val = i_data->get_data(col, row);
	if(s_val >= val) i_data->put_data(col, row, s_val);
	else i_data->put_data(col, row, val);
      } else if(mode == 2){
	s_val = i_data->get_data(col, row);
	if(s_val <= val) i_data->put_data(col, row, s_val);
	else i_data->put_data(col, row, val);
      } else if(mode == 3){
	s_val = i_data->get_data(col, row);
	i_data->put_data(col, row, val + s_val);
      } else if(mode == 4){
	s_val = i_data->get_data(col, row);
	i_data->put_data(col, row, val - s_val);
      } else if(mode == 5){
	s_val = i_data->get_data(col, row);
	i_data->put_data(col, row, val * s_val);
      } else if(mode == 6){
	s_val = i_data->get_data(col, row);
	i_data->put_data(col, row, val / s_val);
      }
      x_val += x_step;
    }
    x_val = xd_start; // 計算点のセット
    y_val -= y_step;
  }

  return;
}

/** SimDataに格納されている値を初期化する関数
 *
 * SimDataに格納されている値を指定された値で初期化する。
 * SimDataで確保されている配列はそのままに、指定された
 * 値で配列すべてを初期化する。
 *
 * @param i_data データを格納するNistk::SimData型のポインタ
 * @param i_val 初期化の値
 */
void Nistk::MathTools::init_simdata(Nistk::SimData *i_data, double i_val)
{
  int col,row;             // 縦横のカウンタ
  int width,height;        // SimData型上の幅と高さ

  // SimData型の幅と高さを取得
  width = i_data->get_width();
  height = i_data->get_height();
  // 初期化
  for(row = 0; row < height; row++){ // 縦
    for(col = 0; col < width; col++){ // 横
	i_data->put_data(col, row, i_val);
    }
  }

  return;
}

/** SimDataに格納されている値を正規化する関数
 *
 * SimDataに格納されている値の総和を求め、(値)/(総和)という形で、
 * SimDataに格納されている値を正規化する。
 * <br>
 * 計算モードはおまけで付けたもので,計算における情報落ちをある程度
 * 補正するものである。ここで使う計算ではたいした効果はないと思われる。
 * sを和,rを積み残し,\f$a_i\f$を加えたい数とすると,以下の様な計算をしている
 * だけである。<br>
 * r += \f$a_i\f$ 積み残し+加えたい数 <br>
 * t = s    前回までの和をtへ代入<br>
 * s += r   和を更新 <br>
 * t -= s   実際に加算された数の符号を変えたもの <br>
 * r += t   積み残し
 *
 * @param i_data データを格納するNistk::SimData型のポインタ
 * @param mode 計算モード
 */
void Nistk::MathTools::normalize_simdata(Nistk::SimData *i_data, bool mode)
{
  int col,row;             // 縦横のカウンタ
  int width,height;        // SimData型上の幅と高さ
  double val;              // SimDataの値
  double total;            // 総和
  double diff;             // 積み残し
  double buf;              // 一時変数


  // SimData型の幅と高さを取得
  width = i_data->get_width();
  height = i_data->get_height();
  // 総和の計算
  total = 0.0;
  diff = 0.0;
  for(row = 0; row < height; row++){ // 縦
    for(col = 0; col < width; col++){ // 横
	val = i_data->get_data(col, row);
	if(mode == true){    // 情報落ちを補正する場合
	  diff += val;         // 積み残し+加えたい数
	  buf = total;         // 前回までの和をbufへ代入
	  total += diff;       // 和を更新
	  buf -= total;        // 実際に加算された数の符号を変えたもの
	  diff += buf;         // 積み残し
	}
	else total += val;
    }
  }
  // 正規化
  for(row = 0; row < height; row++){ // 縦
    for(col = 0; col < width; col++){ // 横
	val = i_data->get_data(col, row);
	i_data->put_data(col, row, val / total);
    }
  }

  return;
}

/** SimData型のデータ同士を演算してSimData型に格納する関数
 * 
 * SimData型に格納されているデータ同士を演算してSimData型
 * に格納する。計算の範囲は引数として渡されている3つのSimData型
 * の縦と横それぞれの一番小さな値となる。基本、同じ大きさ同士
 * を想定しているので、そのようにしている。よって開始位置は
 * (0,0)となる。クラスSimDataに組み込まなかったのは、クラスが
 * 太るのが嫌だったので。
 * <br>
 * 計算モードopは<br>
 * "max" : 計算値とSimDataの値を比較した時の最大値を格納<br>
 * "min" : 計算値とSimDataの値を比較した時の最小値を格納<br>
 * "+"   : 計算値とSimDataの値を加算したものを格納<br>
 * "-"   : 計算値からSimDataの値を減算したものを格納<br>
 * "*"   : 計算値とSimDataの値を乗算したものを格納<br>
 * "/"   : 計算値をSimDataの値で除算したものを格納<br>
 *
 * @param o_data データを格納するNistk::SimData型のポインタ
 * @param i_data1 オペランドとなるNistk::SimData型のポインタ1
 * @param i_data2 オペランドとなるNistk::SimData型のポインタ2
 * @param op 計算モード
 */
void Nistk::MathTools::calc_simdata(Nistk::SimData *o_data, 
				    Nistk::SimData *i_data1,
				    Nistk::SimData *i_data2, const char *op)
{
  int col,row;             // 縦横のカウンタ
  int width,height;        // 格納対象のSimData型上の幅と高さ
  int width1,height1;      // 計算対象のSimData型上の幅と高さ
  int width2,height2;      // 計算対象のSimData型上の幅と高さ
  double s_val1;           // 格納するSimDataの値1
  double s_val2;           // 格納するSimDataの値2
  int mode;
  
  if(strcmp(op,"max") == 0) mode = 1;
  else if(strcmp(op,"min") == 0) mode = 2;
  else if(strcmp(op,"+") == 0) mode = 3;
  else if(strcmp(op,"-") == 0) mode = 4;
  else if(strcmp(op,"*") == 0) mode = 5;
  else if(strcmp(op,"/") == 0) mode = 6;

  // SimData型の幅と高さを取得
  width1 = i_data1->get_width();
  height1 = i_data1->get_height();
  width2 = i_data2->get_width();
  height2 = i_data2->get_height();
  width = o_data->get_width();
  height = o_data->get_height();
  // 計算対象の範囲を決定
  if(width1 < width) width = width1;
  if(width2 < width) width = width2;
  if(height1 < height) height = height1;
  if(height2 < height) height = height2;

  for(row = 0; row < height; row++){ // 縦
    for(col = 0; col < width; col++){ // 横
      s_val1 = i_data1->get_data(col, row);
      s_val2 = i_data2->get_data(col, row);
      // モードをみて格納
      if(mode == 1){
	if(s_val1 >= s_val2) o_data->put_data(col, row, s_val1);
	else o_data->put_data(col, row, s_val2);
      } else if(mode == 2){
	if(s_val1 <= s_val2) o_data->put_data(col, row, s_val1);
	else o_data->put_data(col, row, s_val2);
      } else if(mode == 3){
	o_data->put_data(col, row, s_val1 + s_val2);
      } else if(mode == 4){
	o_data->put_data(col, row, s_val1 - s_val2);
      } else if(mode == 5){
	o_data->put_data(col, row, s_val1 * s_val2);
      } else if(mode == 6){
	o_data->put_data(col, row, s_val1 / s_val2);
      }
    }
  }

  return;
}

/** 半径rのDoG(Differential of Gaussian)を計算しSimData型に格納する関数(3D)
 *
 * 2変数のDoG(Differntial of Gaussian)(グラフを書くと3D)である
 * 以下の式を計算してSimData型に格納する関数。ゲインであるa1,a2を
 * 指定できるようにしてある。原点は,SimData型の二次元配列の中心になる。
 * 単純に縦と横を2で割る(整数演算)だけなので,偶数のときは原点が
 * ずれることになるので注意が必要である。(例えば,7だと3,10だと5となる。
 * 配列は0からなので,奇数は中心,偶数は+1ずれる。)x_step,y_stepを1以外
 * の値にすると,計算では要素間の距離が1以外になる。(配列の添字は整数値で
 * 扱うことになるが。。）通常は,1で良いかと思う。計算式は次のようになっているが、
 * 配列の範囲外の値は計算されないようになっている。また、半径rを超えると0.0が
 * 格納される。
 * 
 * <br>
 \f[
    f(x)=a_1\, \frac{exp^{-\frac{(x-x_c)^2+(y-y_c)^2}{\sigma_1 ^2}}}{\sum _{v,u \in r} exp^{-\frac{(u-x_c)^2+(v-y_c)^2}{\sigma_1 ^2}}} -a_2\, \frac{exp^{-\frac{(x-x_c)^2+(y-y_c)^2}{\sigma_2 ^2}}}{\sum _{v,u \in r} exp^{-\frac{(u-x_c)^2+(v-y_c)^2}{\sigma_2 ^2}}}
 \f] 
 * ただし、u(n+1)=u(n)+x_step,v(n+1)=v(n)+y_step <br>
 * <br>
 * 計算モードはおまけで付けたもので,計算における情報落ちをある程度
 * 補正するものである。ここで使う計算ではたいした効果はないと思われる。
 * sを和,rを積み残し,\f$a_i\f$を加えたい数とすると,以下の様な計算をしている
 * だけである。<br>
 * r += \f$a_i\f$ 積み残し+加えたい数 <br>
 * t = s    前回までの和をtへ代入<br>
 * s += r   和を更新 <br>
 * t -= s   実際に加算された数の符号を変えたもの <br>
 * r += t   積み残し
 *
 * @param i_data データを格納するNistk::SimData型のポインタ
 * @param x_c ガウス関数の中心位置(x軸)
 * @param y_c ガウス関数の中心位置(y軸)
 * @param x_step 刻み幅(x軸)
 * @param y_step 刻み幅(y軸)
 * @param gain1 ゲイン(a1)
 * @param gain2 ゲイン(a2)
 * @param sigma1 幅(\f$\sigma 1 \f$)
 * @param sigma2 幅(\f$\sigma 2 \f$)
 * @param r 計算半径
 * @param mode 計算モード
 */
void Nistk::MathTools::simdata_from_DoG_r(Nistk::SimData *i_data, 
					  double x_c, double y_c, 
					  double x_step, double y_step,
					  double gain1, double gain2, 
					  double sigma1, double sigma2, 
					  double r, bool mode)
{
  int col,row;             // 縦横のカウンタ
  int x_origin,y_origin;   // SimData型上での原点
  int width,height;        // SimData型上の幅と高さ
  double xd_start,yd_start;  // SimData型配列起点のでのx,y値
  double x_val,y_val;      // 計算上のx,yの値
  double total1,total2;    // ガウス関数の和
  double DoG_val;          // DoGの値
  double r_tmp;

  // SimData型の幅と高さを取得
  width = i_data->get_width();
  height = i_data->get_height();
  // SimData型の2次元配列上での原点を計算
  x_origin = width/2;
  y_origin = height/2;
  // 原点から見た左上角のx,y値
  xd_start = -(double)x_origin*x_step;
  yd_start = -(double)y_origin*y_step;
  
 
  // ２つのガウス関数の和を計算(SimDataのサイズを考慮して計算)
  total1 = gaussian_3D_sum_r(x_c, y_c, xd_start, xd_start+((width-1)*x_step), 
			   yd_start, yd_start+((height-1)*y_step),
			   x_step, y_step, sigma1, r, mode);
  total2 = gaussian_3D_sum_r(x_c, y_c, xd_start, xd_start+((width-1)*x_step), 
			   yd_start, yd_start+((height-1)*y_step),
			   x_step, y_step, sigma2, r, mode);

  // DoGを計算してSimData型に格納
  x_val = xd_start; // 計算開始点のセット
  y_val = yd_start;
  for(row = 0; row < height; row++){ // 縦
    for(col = 0; col < width; col++){ // 横
      r_tmp = sqrt((x_c - x_val) * (x_c - x_val) 
		   + (y_c - y_val) * (y_c - y_val));
      if(r_tmp <= r){
	DoG_val = Nistk::MathTools::diff_of_gaussian_3D(x_val, y_val, 
							x_c, y_c, gain1/total1, 
				      gain2/total2, sigma1, sigma2);
	i_data->put_data(col, row, DoG_val);
      } else i_data->put_data(col, row, 0.0);
      x_val += x_step;
    }
    x_val = xd_start; // 計算点のセット
    y_val += y_step;
  }

  return;
}

/** 半径rのガウス関数の和を計算する関数(3D)
 *
 * 以下の様な2変数のガウス関数(グラフを書くと3D)の和を計算して返す関数。
 * 和はx_start,y_startからx_end,y_endまでをそれぞれx_step,y_step刻みで
 * ガウス関数を計算した値となる。中心位置は必要ないが一応付けておく。
 * この和(total)計算し、gaussian_3Dのゲインに1/totalを指定することによって,
 * 正規化されたガウス関数の値を得ることができる。on中心,off中心の受容野
 * 計算に必要となる。<br>
 \f[
    total=\sum _{v,u \in r} exp^{-\frac{(u-x_c)^2+(v-y_c)^2}{\sigma ^2}}
 \f] 
 * ただし、u(n+1)=u(n)+x_step,v(n+1)=v(n)+y_step <br>
 * <br>
 * 計算モードはおまけで付けたもので,計算における情報落ちをある程度
 * 補正するものである。ここで使う計算ではたいした効果はないと思われる。
 * sを和,rを積み残し,\f$a_i\f$を加えたい数とすると,以下の様な計算をしている
 * だけである。<br>
 * r += \f$a_i\f$ 積み残し+加えたい数 <br>
 * t = s    前回までの和をtへ代入<br>
 * s += r   和を更新 <br>
 * t -= s   実際に加算された数の符号を変えたもの <br>
 * r += t   積み残し
 *
 * @param x_c ガウス関数の中心位置(x軸)
 * @param y_c ガウス関数の中心位置(y軸)
 * @param x_start 計算の開始位置(x軸)
 * @param x_end 計算終了位置(x軸)
 * @param y_start 計算の開始位置(y軸)
 * @param y_end 計算終了位置(y軸)
 * @param x_step 刻み幅(x軸)
 * @param y_step 刻み幅(y軸)
 * @param sigma 幅(\f$\sigma \f$)
 * @param r 計算半径
 * @param mode 計算モード
 * @return 合計値
 */
double Nistk::MathTools::gaussian_3D_sum_r(double x_c, double y_c,
					   double x_start, double x_end, 
					   double y_start, double y_end,
					   double x_step, double y_step, 
					   double sigma, double r,
					   bool mode)
{
  double total;   // ガウス関数の和
  double x_counter; // ループカウンタ(x軸）
  double y_counter; // ループカウンタ(y軸)
  double val;     // ガウス関数の計算値
  double diff;    // 積み残し
  double buf;     // 一時変数
  double r_tmp;

  total = 0.0;
  diff = 0.0;
  for(y_counter = y_start; y_counter <= y_end; y_counter += y_step){
    for(x_counter = x_start; x_counter <= x_end; x_counter += x_step){
      r_tmp = sqrt((x_c - x_counter) * (x_c - x_counter)
		   + (y_c - y_counter) * (y_c - y_counter));
      if(r_tmp <= r){
	val = Nistk::MathTools::gaussian_3D(x_counter, y_counter, 
					    x_c, y_c, 1.0, sigma);
	if(mode == true){    // 情報落ちを補正する場合
	  diff += val;         // 積み残し+加えたい数
	  buf = total;         // 前回までの和をbufへ代入
	  total += diff;       // 和を更新
	  buf -= total;        // 実際に加算された数の符号を変えたもの
	  diff += buf;         // 積み残し
	}
	else total += val;
      }
    }
  }

  return total;
}
