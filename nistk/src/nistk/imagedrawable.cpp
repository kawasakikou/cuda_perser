/**
 * @file  imagedrawable.cpp
 * @brief class for imagedrawable used window(実装)
 *
 * @author Masakazu Komori
 * @date 2011-05-21
 * @version $Id: imagedrawable.cpp,v.20110521 $
 *
 * Copyright (C) 2011 Masakazu Komori
 */

#include<gtkmm.h>
#include<imagedrawable.h>

/** コンストラクタ 引数無し
 */
Nistk::ImageDrawable::ImageDrawable()
{
  show_flag = false;
}

/** コンストラクタ 生成と同時にpixbufポインタをセットする
 *
 * pixbufポインタをセットするとしているが、実際には
 * コピーする。
 *
 * @param i_pixbuf コピーするpixbufのGlib::RefPtr<Gdk::Pixbuf>型ポインタ
 */
Nistk::ImageDrawable::ImageDrawable(Glib::RefPtr<Gdk::Pixbuf> i_pixbuf)
{
  m_pixbuf=i_pixbuf->copy();
}

/** コピーコンストラクタ
 */
Nistk::ImageDrawable::ImageDrawable(const Nistk::ImageDrawable &obj)
{
  m_pixbuf=obj.m_pixbuf->copy();
}

/** デストラクタ
 */
Nistk::ImageDrawable::~ImageDrawable()
{
}

/** 代入演算子=のオーバーロード
 */
Nistk::ImageDrawable &Nistk::ImageDrawable::operator=(Nistk::ImageDrawable &obj)
{
  this->m_pixbuf=obj.m_pixbuf->copy();

  return *this;
}

/** pixbufポインタをセットする関数
 *
 * pixbufポインタをセットするとしているが、実際には
 * コピーする。
 *
 * @param i_pixbuf コピーするpixbufのGlib::RefPtr<Gdk::Pixbuf>型ポインタ
 */
void Nistk::ImageDrawable::set_pixbuf(Glib::RefPtr<Gdk::Pixbuf> i_pixbuf)
{
  m_pixbuf=i_pixbuf->copy();
 }

/** Drawableのpixbufをコピーして他のオブジェクトに渡す関数
 *
 * @param i_pixbuf コピーするpixbufのGlib::RefPtr<Gdk::Pixbuf>型ポインタ
 * @return コピーされたGlib::RefPtr<Gdk::Pixbuf>型ポインタ
 */
Glib::RefPtr<Gdk::Pixbuf> Nistk::ImageDrawable::get_pixbuf(
					  Glib::RefPtr<Gdk::Pixbuf> i_pixbuf)
{
  i_pixbuf=m_pixbuf->copy();

  return i_pixbuf;
 }

/** 自動生成の関数でユーザーが使うことはない
 */
void Nistk::ImageDrawable::on_realize()
{
  Gtk::DrawingArea::on_realize();
  m_gc=Gdk::GC::create(get_window());
}

/** 自動描画の関数でユーザーが使うことはない
 */
bool Nistk::ImageDrawable::on_expose_event(GdkEventExpose *event)
{
  Glib::RefPtr<Gdk::Window> window;  // ウィンドウポインタ

  window = get_window();
  if(window){
    show_flag = true; 
    // 画像の幅と高さをpixbufより取得
    int width=m_pixbuf->get_width();
    int height=m_pixbuf->get_height();
  
    // 画像の幅と高さをDrawable領域にセットして描画する。
    // 幅と高さをセットしなくてもできるが,これをしないと
    // スクロールウィンドウなどで使えなくなる
    set_size_request(width,height);
    get_window()->draw_pixbuf(m_gc,m_pixbuf,0,0,0,0,
			      width,height,Gdk::RGB_DITHER_NONE,0,0);
 }
  return true;
}

/** showフラグの値を取得する関数
 *
 * showフラグの値を取得する関数
 *
 * @return 画面が表示されているとtrue
 */
bool Nistk::ImageDrawable::is_show()
{
  return show_flag;
}

/** showフラグをセットする関数
 *
 * showフラグをセットする関数
 *
 * @param sflag showフラグの値
 */
void Nistk::ImageDrawable::set_show_flag(bool sflag)
{
  show_flag = sflag;
  
  return;
}


/** 再描画の関数
 * 画像を強制的に再描画する関数。スレッドプログラミングで使う際に
 * これをしないと画像が書き変らない。
 */
void Nistk::ImageDrawable::redraw()
{
  // windowの取得
  Glib::RefPtr<Gdk::Window> win = get_window();
  if(win){
    // Rectangleで領域を取得
    Gdk::Rectangle r(0, 0, get_allocation().get_width(),
		     get_allocation().get_height());
    // invalidateでwindowに通知
    win->invalidate_rect(r, false);
  }
  
  return;
}
