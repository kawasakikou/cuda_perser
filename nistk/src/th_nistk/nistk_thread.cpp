/**
 * @file  nistk_thread.cpp
 * @brief class for make thread-based class(実装)
 *
 * @author Masakazu Komori
 * @date 2010-08-12
 * @version $Id: nistk_thread.cpp,v.20100812 $
 *
 * Copyright (C) 2010 Masakazu Komori
 */

#include<nistk_thread.h>

/** コンストラクタ
 */
Nistk::NistkThread::NistkThread()
{
  in_thread = false;
  wait_join = false;
  in_main = false;
  nistk_thread_id = 0;
}

/** デストラクタ
 */
Nistk::NistkThread::~NistkThread()
{
  if(thread->joinable()) thread->join();
}

// 排他的処理用mutex(static) mutexの初期化

Glib::StaticMutex Nistk::NistkThread::th_mutex = GLIBMM_STATIC_MUTEX_INIT;

/** threadを生成実行する関数
 * threadが終了していないときはendシグナルを送り、
 * スレッドを終了してから新しくスレッドを生成実行する。
 */
void Nistk::NistkThread::thread_run()
{
  // スレッドが終了していないとき終了のシグナルを送り終了
  if(in_thread == true){
    send_th_end();
  }
  // スレッドの生成実行
  if(in_thread == false){
    sleep(NISTK_THREAD_WAIT);
    in_thread = true;
    thread = Glib::Thread::create(
		   sigc::mem_fun(*this,&Nistk::NistkThread::slot_thread),true);
  }
}

/** threadのjoinをする関数
 */
void Nistk::NistkThread::thread_join()
{
  // threadのjoin待ちのフラグをたてる
  th_mutex.lock();
  wait_join = true;
  th_mutex.unlock();
  // threadのjoin
  thread->join();
  in_thread = false;
  wait_join = false;
}

/** threadの状態を返す関数
 *
 * @return スレッドが走っているかどうか
 */
bool Nistk::NistkThread::is_thread_run()
{
  return in_thread;
}

/** threadのjoin待ちかを返す関数
 *
 * @return threadのjoin待ちかどうか
 */
bool Nistk::NistkThread::is_thread_wait_join()
{
  return wait_join;
}

/** threadのスロット用main関数の状態を返す関数
 *
 * @return threadのスロット用main関数が実行ちゅうかどうか
 */
bool Nistk::NistkThread::is_main_run()
{
  return in_main;
}

/** threadのスロット関数
 *
 * threadのスロット関数。この関数から実際のthread用の
 * main関数を呼出す。thread用main関数を終了しても
 * threadは終了せずにjoinの呼出しまで待つので注意すること。
 */
void Nistk::NistkThread::slot_thread()
{
  // フラグのセット
  th_mutex.lock();
  in_main = true;
  th_mutex.unlock();

  // 実際にthreadで処理をするmain関数の呼び出し
  nistk_slot_main();

  // フラグのアンセット
  th_mutex.lock();
  in_main = false;
  th_mutex.unlock();

  // スレッドを終了せずにjoinフラグがセットされるまで待つ
  for(;;){
    if(wait_join == true) break;
    sleep(NISTK_THREAD_WAIT);
  }
}

/** threadのidをセットする関数
 *
 * @param i セットするthreadのid
 */
void Nistk::NistkThread::set_thread_id(int i)
{
  nistk_thread_id = i;
}

/** threadのidを取得する関数
 *
 * @return セットされたthreadのid
 */
int Nistk::NistkThread::get_thread_id()
{
  return nistk_thread_id;
}







