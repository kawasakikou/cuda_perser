/**
 * @file imagefiles.h
 * @brief Setting file for Nistk
 *
 * @author Masakazu Komori
 * @date 2011-06-25
 * @version $Id: imagefiles.h,v.20110625 $
 *
 * Copyright (C) 2011 Masakazu Komori
 */

/** ツールキットで使用する画像ファイルのパス設定
 *
 * 以下のdefineの記述のPATHの部分をnistkが展開されている
 * デイレクトリのパスに書き換える。
 * パスはそのディレクトリに行き、pwdコマンドを使うなどをすればわかる。
 * 
 */

#ifndef IMAGEFILES_H
#define IMAGEFILES_H

#define NOIMAGE "/nistk/image/no_image.png"


#endif  //IMAGEFILES_H
