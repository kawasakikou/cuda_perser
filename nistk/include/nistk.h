/**
 * @file  nistk.h
 * @brief NISTK (Neural Image Simulation Tool Kit) 
 *
 * @author Masakazu Komori
 * @date 2013-03-15
 * @version $Id: nistk.h,v.20130315 $
 *
 * Copyright (C) 2008-2013 Masakazu Komori
 */

/** \mainpage
 *
 * NISTKはニューラルネットワークシミュレーションのための
 * クラスライブラリである。画像を使ったシミュレーションが
 * 可能となるように,いくつかのクラスを準備している。
 * ImageData,MathTools,Toolsクラスは,分類のためにクラスとしており,
 * オブジェクトとして使う必要はない。
 * 画像処理とウィンドウ関係はGTK+のpixbuf構造体を使っており、
 * 実際には gtkmmを使って組んである。画像表示関係は
 * 全てpixbufを使うことになるが,実際には
 * pixbufの扱いは全てGlib::RefPtr<Gdk::Pixbuf>型
 * スマートポインタを介して行うことになる。
 * 詳しくはgtkmmのHPを参照のこと。<br>
 *
 * ライブラリ一覧:
 *   - メインウィンドウ関係
 *     - Nistk::NistkMainWin : 表示系をひとまとめで扱うメインウィンドウ
 *     - Nistk::ImageViewButton : メインウィンドウのためのImageView用ボタン
 *     - Nistk::ImageArrayButton : メインウィンドウのためのImageArray用ボタン
 *     - Nistk::PlotViewButton : メインウィンドウのためのPlotView用ボタン
 *   - 画像関係
 *     - Nistk::GrayData : グレイデータを扱うクラス
 *     - Nistk::ImageData : 画像処理関係の関数のクラス
 *     - Nistk::ImageDrawable : 画像の描画領域を扱うクラス
 *     - Nistk::ImageLoader : 複数のシミュレーション用画像を読み込むクラス
 *     - Nistk::ImageView : 描画領域をウィンドウ表示するクラス
 *     - Nistk::ImageArray : 複数の画像を並べて表示するためのクラス
 *     - Nistk::PlotDrawable : PlotDataクラスよりグラフのDrawableを生成するクラス
 *     - Nistk::PlotView : データを二次元のグラフとして表示するクラス
 *     - Nistk::SaveArray : 複数の画像を並べて一つのpixbufに格納して画像ファイルとして保存するためのクラス
 *   - 計算用関数関係
 *     - Nistk::MathTools :  よく使いそうな数学関係の関数等
 *     - Nistk::RandGene : ランダムジェネレータ(stdlib版)
 *     - Nistk::RandGene2 : 確率分布に従うランダムジェネレータ(stdlib版)
 *     - Nistk::Calc : シミュレーションでよく使いそうな計算を関数化しまとめたクラス
 *     - Nistk::Temp::Runge4 : 4次のルンゲクッタ法計算用関数テンプレート
 *     - Nistk::Temp::Runge4Fs : 4次のルンゲクッタ法計算用関数テンプレート(FSocket)
 *     - Nistk::Temp::Runge4Si : 4次のルンゲクッタ法で連立微分方程式の計算用関数テンプレート
 *   - ニューロン関係
 *     - Nistk::Neuron::HHNeuron : Hodgikin-Huxley modelの計算用関数を集めたクラス 
 *     - Nistk::Neuron::HHVariable : Hodgikin-Huxley modelの変数を集めたクラス
 *     - Nistk::Neuron::HHParameter : Hodgikin-Huxley modelのためのパラメータ用クラス
 *     - Nistk::Neuron::IVNeuron : IzhikevichのSimple Spiking Neuron modelの計算用関数を集めたクラス 
 *     - Nistk::Neuron::IVVariable : IzhikevichのSimple Spiking Neuron modelの変数を集めたクラス
 *     - Nistk::Neuron::IVParameter : IzhikevichのSimple Spiking Neuron modelのためのパラメータ用クラス
 *   - シミュレーション関係
 *     - Nistk::SimData : シミュレーション用データのためのクラス 
 *     - Nistk::SimDataArray : SimDataクラスを配列として扱うためのクラス
 *     - Nistk::DataFileOut : データをファイルに書き出すためのクラス
 *     - Nistk::PlotData : PlotDrawableクラスのためのデータを管理するクラス
 *     - Nistk::Input : シミュレーション用の入力を作成する関数を集めたクラス
 *     - Nistk::DataIO : シミュレーションにおけるデータの入出力に関するクラス
 *   - その他
 *     - Nistk::Tools : シミュレーションには直接関係ないtool的な関数を集めたクラス
 *     - Nistk::Temp::FSocket1 関数自体(引数1)をクラスもしくは関数に渡すためのソケットクラス
 *     - Nistk::Temp::FSocket2 関数自体(引数2)をクラスもしくは関数に渡すためのソケットクラス
 *     - Nistk::Temp::FSocket3 関数自体(引数3)をクラスもしくは関数に渡すためのソケットクラス
 *     - Nistk::Temp::FSocket4 関数自体(引数4)をクラスもしくは関数に渡すためのソケットクラス
 *   - スレッド版クラス（β版）
 *     - Nistk::NistkThread : Thread対応のクラスを作るためのクラス（β版）
 *     - Nistk::ThWindowBase : Thread対応のクラス用ウィンドウクラスを作るためのクラス（β版）
 *
 * <br>
 * Copyright (C) 2008-2013 Masakazu Komori
 */

/**
 * @namespace Nistk
 * NISTK (Neural Image Simulation Tool Kit)
 * で定義されているクラスの名前空間
 */

/**
 * @namespace Nistk::Temp
 * Nistkのテンプレートクラスおよび関数の名前空間。
 * 通常のクラスライブラリとして実装できないテンプレートクラス
 * および関数のための名前空間。 区別するためにNistkの名前空間
 * のなかの名前空間としている。ただし、ある一定の区分分けができる
 * テンプレートクラスおよび関数の場合は、別の名前空間を作ることにする。
 * テンポラリとは違うので注意すること。スコープはNistk::Tempとなる。
 */

/**
 * @namespace Nistk::Neuron
 * Nistkで準備しているニューロンに関するクラスのための
 * 名前空間。状況に応じて増やしていく予定である。
 */

#include<imagefiles.h>

#include<nistk/imagearray_button.h>
#include<nistk/imageview_button.h>
#include<nistk/plotview_button.h>
#include<nistk/nistk_main_win.h>

#include<nistk/graydata.h>
#include<nistk/imagedata.h>
#include<nistk/imagedrawable.h>
#include<nistk/imageloader.h>
#include<nistk/imageview.h>
#include<nistk/imagearray.h>
#include<nistk/plotdata.h>
#include<nistk/plotdrawable.h>
#include<nistk/plotview.h>
#include<nistk/savearray.h>

#include<nistk/mathtools.h>
#include<nistk/randgene.h>
#include<nistk/randgene2.h>
#include<nistk/calc.h>
#include<nistk/tools.h>

#include<nistk/simdata.h>
#include<nistk/simdataarray.h>
#include<nistk/datafileout.h>

#include<th_nistk/nistk_thread.h>
#include<th_nistk/th_window_base.h>

#include<nistk/fsocket.h>
#include<nistk/runge.h>

#include<nistk/hhneuron.h>
#include<nistk/hhvariable.h>
#include<nistk/hhparameter.h>
#include<nistk/ivneuron.h>
#include<nistk/ivvariable.h>
#include<nistk/ivparameter.h>

#include<nistk/input.h>
#include<nistk/data_io.h>
