/**
 * @file  ivvariable.h
 * @brief Class for Variable of Izhikevich's Simple Spiking Neuron
 *
 * @author Masakazu Komori
 * @date 2011-08-05
 * @version $Id: ivvariable.h,v.20110805 $
 *
 * Copyright (C) 2011 Masakazu Komori
 */

#ifndef IVVARIABLE_H
#define IVVARIABLE_H

namespace Nistk{
namespace Neuron{
/** IzhikevichのSimple Spiking Neuron modelのための変数クラス
 *
 * IzhikevichのSimple Spiking Neuron modelのための変数クラスで、
 * モデルを次のようなパラメータで表した際のメンバをもっている。
 \f[
        \frac{dv}{dt} = (vp\_1 * v^2 + vp\_2 * v + vp\_3 
                            - vp\_4 * u + vp\_5 + I) / vp\_6
 \f]
 \f[
        \frac{du}{dt} = a * (b * (v + up\_1) - up\_2 * u)
 \f]
 \f[
  if\,\,\, v \geq v\_rest[mV] \,\,\,then \,\,\,\left\{
                                  \begin{array}{ll}
                                     v & \leftarrow vp\_7 * v + c\\
                                     u & \leftarrow up\_3 * u + d
                                  \end{array}
                                 \right.
 \f]
 * このクラスのメンバ変数は、手法としては良くないが操作の煩雑さを
 * 減らすために全てpublicとする。つまり、アクセッサなどを使わない。詳しいことは、
 * Nistk::Neuron::IVNeuronを参照のこと。
 *
 * Copyright (C) 2011 Masakazu Komori
 */
class IVVariable
{
 public:
  double v;   ///< Simple Spiking Neuron modelにおける膜電位
  double u;   ///< Simple Spiking Neuron modelにおけるリカバリ変数
  double I;   ///< 入力
  IVVariable();   ///< コンストラクタ
  ~IVVariable();   ///< デストラクタ
};

}
}

#endif //IVVARIABLE_H
