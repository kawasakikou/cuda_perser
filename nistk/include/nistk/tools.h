/**
 * @file  tools.h
 * @brief class for tools
 *
 * @author Masakazu Komori
 * @date 2011-04-29
 * @version $Id: tools.h,v.20110429 $
 *
 * Copyright (C) 2011 Masakazu Komori
 */

#ifndef NISTK_TOOLS_H
#define NISTK_TOOLS_H
#include<cmath>

namespace Nistk{
/** シミュレーションには直接関係ないtool的な関数を集めたクラス
 *
 * シミュレーションには直接関係ないtool的な関数を集めたクラス。このクラスは便宜上
 * クラスにしているだけで、オブジェクトを生成する必要はないクラス
 * である。また、このクラスで準備される関数は必要に応じて増やしていく
 * つもりなので,最終的にどうなるかは考えていない。
 *
 * Copyright (C) 2011 Masakazu Komori
 */
class Tools
{
 public:
  /// データの最大次数を求める関数
  static int calc_order(double data);
  /// orderで指定した次数からdataの下位の桁を0にする関数
  static double round_off_data(double data, int order);
  /// dataの値を文字列に変換する関数
  static void num_to_str(double data, char *o_str, int max_order, 
			 int min_order, int length, bool mode);
};

}
#endif // NISTK_TOOLS_H
