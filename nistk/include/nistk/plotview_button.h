/**
 * @file  plotview_button.h
 * @brief class for plotview button used by nistk main window
 *
 * @author Masakazu Komori
 * @date 2011-08-09
 * @version $Id: plotview_button.h,v.20110809 $
 *
 * Copyright (C) 2011 Masakazu Komori
 */

#ifndef PLOTVIEW_BUTTON_H
#define PLOTVIEW_BUTTON_H

#include<gtkmm/window.h>
#include<gtkmm/button.h>
#include<nistk/plotview.h>
#include<nistk/plotdata.h>

namespace Nistk{
/** メインウィンドウのためのPlotView用ボタン
 *
 * NistkMainWinでPlotViewのボタンを配置し、ボタンがクリックされた際に
 * PlotViewのウィンドウが表示されるようにするためのクラス。
 * なので、ユーザが使用することはない。
 * 再度ボタンをクリックすると消えるようになっている。<br>
 *
 * Copyright (C) 2011 Masakazu Komori
 */

class PlotViewButton :public Gtk::Button  // Gtk::Buttonクラスを継承
{
 protected:
  Nistk::PlotView window;        ///< PlotViewウィジット
  virtual void on_clicked();     ///< クリックされた時の処理(オーバーライド)

 public:
  PlotViewButton();              ///< コンストラクタ
  ~PlotViewButton();             ///< デストラクタ
  void set_name(const char *name);  ///< ボタンとウィンドウの名前をセットする関数
  Nistk::PlotView* get_plotview_ptr();   ///< PlotViewのポインタを取得する関数
};

}

#endif // PLOTVIEW_BUTTON_H

