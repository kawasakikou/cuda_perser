/**
 * @file  simdataarray.h
 * @brief class for SimData array
 *
 * @author Masakazu Komori
 * @date 2011-08-15
 * @version $Id: simdataarray.h,v.20110815 $
 *
 * Copyright (C) 2009-2011 Masakazu Komori
 */

#ifndef NISTK_SIMDATAARRAY_H
#define NISTK_SIMDATAARRAY_H

#include<simdata.h>

namespace Nistk{
/** シミュレーション用の二次元配列データの配列のためのクラス
 *
 * 2次元のデータを二次元配列で格納できるクラス。
 * その他,SimData型であればいろいろ使える。高さを1とすれば
 * 1次元データのベクターとしても扱うことが可能である。
 * 実際は1次元のSimDataクラスの配列をデータとして持っているだけ。
 * ちなみに作成時はデータの初期化はしていない。
 * 画像入力に対するニューロンの2次元の重みの配列として使うときは、
 * 縦横は奇数にすること。相対位置を使って入力の荷重和をとる際に
 * その方が都合がよい。配列の要素,つまりSimData型オブジェクトの
 * メンバ関数を使えばそれぞれの配列の大きさはバラバラにすることも
 * 可能である。このクラスのオブジェクトは,基本的に
 * ポインタを使ってやりとりすること。データサイズがおおきいと
 * 処理が遅くなるので。
 *
 * Copyright (C) 2009-2011 Masakazu Komori
 */
class SimDataArray
{
 protected:
  int height;                     ///< 二次元配列データの配列の高さ
  int width;                      ///< 二次元配列データの配列の幅
  int length;                     ///< 一次元での長さ
  /// SimData型の配列を二次元配列として扱うためのポインタ
  Nistk::SimData **w_array2d;


 public:
  SimDataArray();                 ///< コンストラクタ 引数無し

  /// コンストラクタ 配列の確保もする SimData配列の大きさは皆同じになる
  SimDataArray(int w,int h, int w_w, int w_h);

  /// コピーコンストラクタ 基本使わないこと! 処理が遅くなる
  SimDataArray(const Nistk::SimDataArray &obj);  

  /// 代入演算子=のオーバーロード 基本使わないこと!使うと処理が遅くなる
  Nistk::SimDataArray &operator=(Nistk::SimDataArray &obj);
  ~SimDataArray();                ///< デストラクタ
  void delete_array();            ///< SimDataArrayにあるデータを消去する関数

  /// 配列の確保をする関数  SimData配列の大きさは皆同じになる
  void create_data_size(int w,int h, int w_w, int w_h); 

  /// 配列の大きさを変更する関数 データはきえてしまい,大きさは皆同じになる
  void recreate_data_size(int w,int h, int w_w, int w_h);

  int get_width();               ///< SimData型配列の幅を取得する関数
  int get_height();              ///< SimData型配列の高さを取得する関数
  int get_length();              ///< SimData型配列の長さを取得する関数

  /// 位置x,yのSimData型オブジェクトの幅を取得する関数
  int get_array_width(int x, int y); 

  /// 位置x,yのSimData型オブジェクトの高さを取得する関数
  int get_array_height(int x, int y);

  /// 位置x,yのSimData型オブジェクト内の位置w_x,w_yの値を取得する関数
  double get_data(int x, int y, int w_x, int w_y);

  /// 位置x,yのSimData型オブジェクト内の位置w_x,w_yにデータを格納する関数
  void put_data(int x, int y, int w_x, int w_y, double i_data);

  /// 位置x,yのSimData型オブジェクトのポインタを取得する関数
  Nistk::SimData* get_simdata_ptr(int x, int y);

  /// 位置x,yのSimData型オブジェクトのデータを消去する関数
  void delete_simdata(int x, int y);

  /// 位置x,yのSimData型オブジェクトのデータをセットする関数  0で初期化
  void set_simdata(int x, int y, int w_w, int w_h); 

  /// 位置x,yのSimData型オブジェクトのデータをセットする関数  i_SimDataで初期化
  void set_simdata(Nistk::SimData *i_SimData, int x, int y);

  /// 位置x,yのSimData型オブジェクトのデータをコピーする関数
  void get_simdata(int x, int y, Nistk::SimData *i_SimData);

  /// 一次元配列のとした時の位置xのSimData型オブジェクトの幅を取得する関数
  int get_array_width1d(int x); 

  /// 一次元配列のとした時の位置xのSimData型オブジェクトの高さを取得する関数
  int get_array_height1d(int x);

  /// 一次元配列のとした時の位置xのSimData型オブジェクト内の位置w_x,w_yの値を取得する関数
  double get_data1d(int x, int w_x, int w_y);

  /// 一次元配列のとした時の位置xのSimData型オブジェクト内の位置w_x,w_yにデータを格納する関数
  void put_data1d(int x, int w_x, int w_y, double i_data);

  /// 一次元配列のとした時の位置xのSimData型オブジェクトのポインタを取得する関数
  Nistk::SimData* get_simdata_ptr1d(int x);

  /// 一次元配列のとした時の位置xのSimData型オブジェクトのデータを消去する関数
  void delete_simdata1d(int x);

  /// 一次元配列のとした時の位置xのSimData型オブジェクトのデータをセットする関数  0で初期化
  void set_simdata1d(int x, int w_w, int w_h); 

  /// 一次元配列のとした時の位置xのSimData型オブジェクトのデータをセットする関数  i_SimDataで初期化
  void set_simdata1d(Nistk::SimData *i_SimData, int x);

  /// 一次元配列のとした時の位置xのSimData型オブジェクトのデータをコピーする関数
  void get_simdata1d(int x, Nistk::SimData *i_SimData);

};

}

#endif // NISTK_SIMDATAARRAY_H
