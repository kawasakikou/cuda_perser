/**
 * @file  hhvariable.h
 * @brief Class for variable of Hodgikin-Huxley model
 *
 * @author Masakazu Komori
 * @date 2011-08-01
 * @version $Id: hhvariable.h,v.20110801 $
 *
 * Copyright (C) 2011 Masakazu Komori
 */

#ifndef HHVARIABLE_H
#define HHVARIABLE_H

namespace Nistk{
namespace Neuron{
/** Hodgikin-Huxley modelの変数を集めたクラス
 *
 * Hodgikin-Huxley modelのための変数用クラス。
 * Hodgikin-Huxley modelにおける変数を集めたクラス。
 * このクラスのメンバ変数は、手法としては良くないが操作の煩雑さを
 * 減らすために全てpublicとする。つまり、アクセッサなどを使わない。
 * 初期化は全て0.0で行うので、膜電位Vは使用の際は必ず各自で初期化
 * すること。変数の詳しい説明はクラス
 * Nistk::Neuron::HHNeuronを参照のこと。<br>
 *   クラスでは、各イオン電流等を確認できるように以下の式
 \f[
      C_m\frac{dV}{dt} = G_{Na}m^3h(E_{Na} - V) 
                         + G_Kn^4(E_K - V) 
                         + G_m(V_L - V) + I_{inj}(t) 
 \f]
 * を次のようにも考えてメンバを確保している。
 \f[
      C_m\frac{dV}{dt} + I_{Na} + I_K + I_{leak} = I_{inj}(t) 
 \f]
 \f[
      I_{Na} = G_{Na}m^3h(V - E_{Na}) 
 \f]
 \f[
      I_K = G_Kn^4(V - E_K)
 \f]
 \f[
      I_{leak} = G_m(V - V_L)
 \f]
 * 各係数は次のようにして決定される。
 \f[
      \frac{dm}{dt} = \alpha _m(V)(1-m) - \beta _m(V)m
 \f]
 \f[
      \frac{dh}{dt} = \alpha _h(V)(1-h) - \beta _h(V)h
 \f]
 \f[
      \frac{dn}{dt} = \alpha _n(V)(1-n) - \beta _n(V)n
 \f]
 * 各パラメータのなかには初期化が必要なものがあるので使用の前には必ず
 * Nistk::Neuron::HHNeuronクラスのinit関数で初期化を行うこと。
 *
 * Copyright (C) 2011 Masakazu Komori
 */
class HHVariable
{
 public:
  double V;                   ///< 膜電位
  double I_Na;                ///< Naイオン電流
  double I_K;                 ///< Kイオン電流
  double I_leak;              ///< leak電流
  double I_inj;               ///< 入力電流
  double m;                   ///< イオンチャネルの開口状態m
  double h;                   ///< イオンチャネルの開口状態h
  double n;                   ///< イオンチャネルの開口状態n
  HHVariable();               ///< コンストラクタ
  ~HHVariable();              ///< デストラクタ
};

}
}

#endif //HHVARIABLE_H
