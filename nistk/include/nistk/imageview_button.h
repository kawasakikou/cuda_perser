/**
 * @file  imageview_button.h
 * @brief class for imageview button used by nistk main window
 *
 * @author Masakazu Komori
 * @date 2011-08-09
 * @version $Id: imageview_button.h,v.20110809 $
 *
 * Copyright (C) 2010-2011 Masakazu Komori
 */

#ifndef IMAGEVIEW_BUTTON_H
#define IMAGEVIEW_BUTTON_H

#include<gtkmm/window.h>
#include<gtkmm/button.h>
#include<imageview.h>

namespace Nistk{
/** メインウィンドウのためのImageView用ボタン
 *
  * NistkMainWinでImageViewのボタンを配置し、ボタンがクリックされた際に
 * ImageViewのウィンドウが表示されるようにするためのクラス。
 * なので、ユーザが使用することはない。
 * 再度ボタンがクリックすると消えるようになっている。<br>
 *
 * Copyright (C) 2010-2011 Masakazu Komori
 */

class ImageViewButton :public Gtk::Button  // Gtk::Buttonクラスを継承
{
 protected:
  Nistk::ImageView window;          ///< ImageViewウィジット
  virtual void on_clicked();        ///< クリックされた時の処理(オーバーライド)

 public:
  ImageViewButton();                ///< コンストラクタ
  ~ImageViewButton();               ///< デストラクタ
  void set_name(const char *name);  ///< ボタンとウィンドウの名前をセットする関数
  Nistk::ImageView* get_imageview_ptr();   ///< ImageViewのポインタを取得する関数
};

}

#endif // IMAGEVIEW_BUTTON_H

