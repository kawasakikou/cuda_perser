/**
 * @file  imageview.h
 * @brief class for image drawable
 *
 * @author Masakazu Komori
 * @date 2011-05-21
 * @version $Id: imageview.h,v.20110521 $
 *
 * Copyright (C) 2008-2011 Masakazu Komori
 */

#ifndef NISTK_IMAGEVIEW_H
#define NISTK_IMAGEVIEW_H
#include<gtkmm.h>
#include<imagedrawable.h>
#include<string>

namespace Nistk{
/** dorawable(描画領域)を読み込みウィンドウを生成するクラス
 *
 * Nistk::ImageDrawableとGtk :: Dialogを使ってDrawable
 * にかかれた画像をウィンドウに表示するクラス。
 * ウィンドウに名前を付けることもでき、この名前は表示を
 * セーブするときのファイル名にもなる。
 * <br>
 *
 * Copyright (C) 2008-2011 Masakazu Komori
 */
class ImageView : public Gtk::Dialog
{
 public:
  ImageView();                      ///< コンストラクタ 引数無し
  /// コンストラクタ 引数(Glib::RefPtr<Gdk::Pixbuf>) 描画までしてしまう
  ImageView(Glib::RefPtr<Gdk::Pixbuf> i_pixbuf); 
  virtual ~ImageView();            ///< デストラクタ
  void set_name(const char *i_name); ///< 名前をセットする関数
  /// 画像をセットする関数
  void set_image(Glib::RefPtr<Gdk::Pixbuf> i_pixbuf);
  /// ボタンが押されるとウィンドウを閉じる関数 ユーザは使う必要はない
  virtual void on_button_close();
  /// ボタンが押されると現在描画しているイメージをファイルにセーブする関数
  virtual void on_button_save();
  /// コンストラクタで呼ばれる関数 ユーザは使う必要はない
  void set_up_window();
  bool is_window_show();           ///< windowの表示フラグの取得関数
  /// windowのshowフラグをセットする関数
  void set_show_window(bool sflag);
  /// 再描画関数 スレッドプログラミングの際に必要になる
  void redraw_window();

 protected: 
  Gtk::ScrolledWindow m_ScrolledWindow;
  Gtk::Button m_Button_Close,m_Button_Save;
  Gtk::HBox m_Hbox;
  Nistk::ImageDrawable m_draw;  
  std::string m_name;
  bool on_delete_event(GdkEventAny*);  ///< xボタンが押されたときの処理関数
};

}  

#endif // NISTK_IMAGEVIEW_H
