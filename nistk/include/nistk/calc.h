/**
 * @file  calc.h
 * @brief class for calculation tool
 *
 * @author Masakazu Komori
 * @date 2013-03-15
 * @version $Id: calc.h,v.20130315 $
 *
 * Copyright (C) 2010-2013 Masakazu Komori
 */

#ifndef CALC_H
#define CALC_H

#include<simdata.h>
#include<simdataarray.h>

namespace Nistk{
/** シミュレーションでよく使いそうな計算を関数化しまとめたクラス
 *
 * ニューラルネットワークのシミュレーションでプログラムを組む際に
 * よく使いそうな計算処理を集めたクラス。このクラスは便宜上
 * クラスにしているだけで、オブジェクトを生成する必要はないクラス
 * である。また、このクラスで準備される関数は必要に応じて増やしていく
 * つもりなので,最終的にどうなるかは考えていない。
 *
 * Copyright (C) 2010-2013 Masakazu Komori
 */
class Calc
{
 public:
  /// SimData型の入力データの位置x,yを中心として荷重和を取る関数
  static double weighted_sum(Nistk::SimData *in, Nistk::SimData *weight,
   		                                              int x, int y);
  /// SimData型の入力データの位置x,yを中心として荷重和を取る関数2
  static double weighted_sum2(Nistk::SimData *in, Nistk::SimData *weight,
			      int x, int y, int wx_start, int wx_end, 
			                           int wy_stat, int wy_end);
  /// SimData型の入力データに対して同一の荷重で荷重和を計算する関数
  static void input_single_weight(Nistk::SimData *in, Nistk::SimData *weight,
   	 	      Nistk::SimData *out, int start_x, int start_y, int step);
  /// SimData型の入力データに対してSimDataArray型の荷重で荷重和を計算する関数
  static void input_array_weight(Nistk::SimData *in, 
		      Nistk::SimDataArray *weight, Nistk::SimData *out, 
				       int start_x, int start_y, int step);
};

}

#endif // CALC_H
