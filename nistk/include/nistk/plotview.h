/**
 * @file  plotview.h
 * @brief class for plot drawable
 *
 * @author Masakazu Komori
 * @date 2011-08-09
 * @version $Id: plotview.h,v.20110809 $
 *
 * Copyright (C) 2011 Masakazu Komori
 */

#ifndef NISTK_PLOTVIEW_H
#define NISTK_PLOTVIEW_H
#include<gtkmm.h>
#include<plotdrawable.h>
#include<plotdata.h>
#include<string>


namespace Nistk{
/** データを二次元のグラフとして表示するクラス
 *
 * Nistk::PlotDrawableとGtk :: Dialogを使って
 * 二次元データのグラフを表示するクラス。
 * ウィンドウに名前を付けることもでき、この名前は表示を
 * セーブするときのファイル名にもなる。
 * グラフはアレイ状に複数表示が可能となっており，一つのグラフに
 * 複数のプロットをすることも可能となっている。この機能は
 * PlotDataクラスをつかっておこなっており，表示するデータは
 * メンバ関数pd_ptrを使ってPlotDataのポインタを取得する形で、
 * データの格納、セーブ、参照等が可能となっている。
 * PlotDataクラスのところに書いたが利用できるメモリを考慮
 * しているわけではないので注意が必要である。
 * <br>
 *
 * Copyright (C) 2011 Masakazu Komori
 */

class PlotView : public Gtk::Dialog
{
  protected: 
   Gtk::ScrolledWindow m_ScrolledWindow; ///< PlotDrawable格納用ScrolledWindow
   Gtk::Button m_Button_Close;           ///< Close用ボタン
   Gtk::Button m_Button_Save;            ///< Save用ボタン
   Gtk::HBox m_Hbox;                     ///< Close,Saveボタン格納用Hbox
   Nistk::PlotDrawable m_draw;           ///< Plot用Drawable領域
   std::string m_name;                   ///< ウィンドウ名用文字列
  /// ボタンが押されるとウィンドウを閉じる関数 ユーザは使う必要はない
  virtual void on_button_close();
  /// ボタンが押されると現在描画しているイメージをファイルにセーブする関数
  virtual void on_button_save();
  /// コンストラクタで呼ばれる関数 ユーザは使う必要はない
  void set_up_window();
  bool on_delete_event(GdkEventAny*);  ///< xボタンが押されたときの処理関数

 public:
  PlotView();                      ///< コンストラクタ 引数無し
  ~PlotView();                     ///< デストラクタ
  /// グラフ領域を確保する関数
  void set_graph_area(int x_num, int y_num, int x_range, int y_range, 
		      int x_margin=80, int y_margin=40);
  /// 位置x,yのグラフのPlotDataのポインタを取得する関数
  Nistk::PlotData* pd_ptr(int x, int y);
  void set_name(const char *i_name);     ///< 名前をセットする関数
  bool is_window_show();           ///< windowの表示フラグの取得関数
  /// windowのshowフラグをセットする関数
  void set_show_window(bool sflag);
  void redraw_window();            ///< 表示の再描画用関数
};

}  

#endif // NISTK_PLOTVIEW_H
