/**
 * @file  nistk_main_win.h
 * @brief class for main window class
 *
 * @author Masakazu Komori
 * @date 2011-08-09
 * @version $Id: nistk_main_win.h,v.20110809 $
 *
 * Copyright (C) 2011 Masakazu Komori
 */

#ifndef NISTK_MAIN_WIN_H
#define NISTK_MAIN_WIN_H

#include<gtkmm.h>
#include<imageview.h>
#include<imagearray.h>
#include<plotview.h>
#include<plotdata.h>
#include<imagearray_button.h>
#include<imageview_button.h>
#include<plotview_button.h>

namespace Nistk{
/** 表示系をひとまとめで扱うメインウィンドウ
 * 
 * ImageView,ImageArray,PlotViewに対応するボタンを収容してメインウィンドウの形
 * にしたもの。このようにすることによって、シミュレーション結果を事前に
 * セットしておけば、まとめて確認することが可能となる。ボタンの個数、
 * つまり、ImageView,ImageArray,PlotViewの個数は
 * create_imageview,create_imagearray,create_plotview
 * 関数によって設定することがかのうとなっている。また、それぞれのボタンに
 * 対して、set_imageview_name,set_imagearray_name,set_plotview_name関数を使うことによって、
 * 名前を付けることが可能となっている。<br>
 *
 * Copyright (C) 2010-2011 Masakazu Komori
 */

class NistkMainWin :public Gtk::Dialog  // Gtk::Dialogクラスを継承
{
 protected:
  Gtk::Button m_button_close;             ///< closeボタンウィジット
  Gtk::VBox m_vbox;                       ///< Frame収納用ボックスウィジット

  Gtk::Frame image_view_frame;            ///< ImageView用フレーム
  Gtk::ScrolledWindow image_view_scrowin; ///< ImageView用スクロールドウィンドウ
  Gtk::Table image_view_table;            ///< ImageView用テーブル

  Gtk::Frame image_array_frame;           ///< ImageArray用フレーム
  Gtk::ScrolledWindow image_array_scrowin;///< ImageArray用スクロールドウィンドウ
  Gtk::Table image_array_table;           ///< ImageArray用テーブル

  Gtk::Frame plot_view_frame;            ///< PlotView用フレーム
  Gtk::ScrolledWindow plot_view_scrowin; ///< PlotView用スクロールドウィンドウ
  Gtk::Table plot_view_table;            ///< PlotView用テーブル

  Nistk::ImageViewButton *button_image_view;    ///< ImageViewボタンウィジット
  Nistk::ImageArrayButton *button_image_array;  ///< ImageArrayボタンウィジット
  Nistk::PlotViewButton *button_plot_view;      ///< PlotViewボタンウィジット

  int image_view_num;                         ///< 作成したImageViewの数
  int image_array_num;                        ///< 作成したImageArrayの数
  int plot_view_num;                          ///< 作成したPlotViewの数

  bool is_imageview_packed;                ///< ImageViewがフレームに格納されているか
  bool is_imagearray_packed;               ///< Imagearrayがフレームに格納されているか
  bool is_plotview_packed;                 ///< Plotviewがフレームに格納されているか

  void on_button_close();                 ///< closeボタンが押されたときの処理関数
  

 public:
  NistkMainWin();                                ///< コンストラクタ
  ~NistkMainWin();                               ///< デストラクタ

  void create_imageview(int num, int col_num=5); ///< ImageViewを作成する関数
  /// ImageViewとボタンに名前をセットする関数
  void set_imageview_name(int num, const char *name);
  /// ImageViewのポインタを取得する関数
  Nistk::ImageView* get_imageview_ptr(int num); 
  /// NistkMainWinからImageViewのフレーム取り除く関数
  void remove_imageview();
  /// NistkMainWinにImageViewのフレーム追加する関数
  void add_imageview();
  int get_imageview_num();                       ///< ImageViewの数を取得する関数

  void create_imagearray(int num, int col_num=5); ///< ImageArrayを作成する関数
  /// ImageArrayとボタンに名前をセットする関数
  void set_imagearray_name(int num, const char *name);
  /// ImageArrayのポインタを取得する関数
  Nistk::ImageArray* get_imagearray_ptr(int num); 
  /// NistkMainWinからImageArrayのフレーム取り除く関数
  void remove_imagearray();
  /// NistkMainWinにImageArrayのフレーム追加する関数
  void add_imagearray();
  int get_imagearray_num();                     ///< ImageArrayの数を取得する関数

  void create_plotview(int num, int col_num=5); ///< PlotViewを作成する関数
  /// PlotViewとボタンに名前をセットする関数
  void set_plotview_name(int num, const char *name);
  /// PlotViewのポインタを取得する関数
  Nistk::PlotView* get_plotview_ptr(int num); 
  /// num番目のPlotViewの位置x,yのグラフのPlotDataのポインタを取得する関数
  Nistk::PlotData* pd_ptr(int num, int x, int y); 
  /// NistkMainWinからPlotViewのフレーム取り除く関数
  void remove_plotview();
  /// NistkMainWinにPlotViewのフレーム追加する関数
  void add_plotview();
  int get_plotview_num();                       ///< PlotViewの数を取得する関数
};

}

#endif // NISTK_MAIN_WIN_H

