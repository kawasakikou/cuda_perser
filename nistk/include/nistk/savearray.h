/**
 * @file  savearray.h
 * @brief class for save SimDataArray image file
 *
 * @author Masakazu Komori
 * @date 2013-03-15
 * @version $Id: savearray.h,v.20130315 $
 *
 * Copyright (C) 2013 Masakazu Komori
 */

#ifndef NISTK_SAVEARRAY_H
#define NISTK_SAVEARRAY_H
#include<string>
#include<cmath>
#include<gtkmm.h>
#include<simdataarray.h>
#include<imageview.h>
#include<imageloader.h>

namespace Nistk{
/** 複数の画像を並べて一つのpixbufに格納して画像ファイルとして保存するためのクラス
 *
 * 複数のイメージを並べて一つのpixbufに格納して画像ファイル
 * として保存するためのクラス。Nistk::ImageArrayクラスを改変したものである。
 * ImageLoader,SimDataArrayといった
 * 複数のイメージを一括して扱えるものからと,個別のSimData
 * を並べることもできる。ただし、個別のSimData,画像を並べる場合,
 * 幅と高さが揃っていないといけない。
 * 画像ファイルとして保存の際は、引数のファイル名は拡張子を付けなくても
 * 良いようになっている。
 * 使えるファイル形式はgtkmmのpixbufが対応しているファイル形式になる。
 * 例えば,png,jpeg,bmpなど。また、シミュレーションでの途中経過を
 * 保存したい場合にも使いやすいように、ファイル名の前もしくは後に
 * 数値を付けれるようにしてある。(例：test0023.45.png,0010test.png) 
 * ただし、汎用性を考えて引数はdouble型としている。実数を文字に変換
 * する際にたまに値がずれるので注意すること。(例:23.45→23.449)
 * 整数と少数の桁数指定も必要となっている。モードの指定は以下の
 * とおりである。<br><br>
 *
 * mode: 0:数字を付けない 
 *       1:ファイル名の後に数字をつける 
 *       2:ファイル名の前に数字をつける 
 *
 * Copyright (C) 2013 Masakazu Komori
 */
class SaveArray 
{
 public:
  SaveArray();                           ///< コンストラクタ 引数無し
  ~SaveArray();                          ///< デストラクタ
  /// arrayのためのpixbuf領域を確保する関数
  void create_array_area(int im_width, int im_height, int im_margin,
			                             int w_num, int h_num);
  /// 1つのpixbufをarrayの位置x_num,y_numに書き込む関数
  void set_pixbuf_to_array(Glib::RefPtr<Gdk::Pixbuf> i_pixbuf, 
			                             int x_num, int y_num);
  /// 1つのSimDataをarrayの位置x_num,y_numに書き込む関数
  void set_simdata_to_array(Nistk::SimData *i_simdata, 
			                             int x_num, int y_num);
  /// Nistk::SimDataArrayから画像のアレイを作る関数
  void array_from_simdataarray(Nistk::SimDataArray *i_array, int i_margin, 
		                        double i_scale=1, int num_width=5);
  /// Nistk::ImageLoaderから画像のアレイを作る関数
  void array_from_file(Nistk::ImageLoader i_loader, int i_margin, 
		                        double i_scale=1, int num_width=5);
  /// 格納された画像を画像ファイルとして保存する関数
  void save_image(const char *i_file_name="out-fig",
		  const char *i_file_type="png",
		  int mode=0, double num=0, 
		  int major=0, int minor=0);
  /// arrayのpixbufを取得する関数
  Glib::RefPtr<Gdk::Pixbuf> get_pixbuf(Glib::RefPtr<Gdk::Pixbuf> i_pixbuf);
  int get_array_width();                 ///< 画像アレイの幅を取得する関数
  int get_array_height();                ///< 画像アレイの高さを取得する関数
  int get_array_w_num();                 ///< 横の画像の数を取得する関数
  int get_array_h_num();                 ///< 縦の画像のを取得する関数
  int get_image_height();                ///< 1つの画像の高さを取得する関数
  int get_image_width();                 ///< 1つの画像の幅を取得する関数
  int get_image_margin();                ///< 画像間の幅を取得する関数
 protected: 
  Glib::RefPtr<Gdk::Pixbuf> array_pixbuf;  ///< 画像アレイの収納用
  int array_width;                         ///< 画像アレイの幅
  int array_height;                        ///< 画像アレイの高さ
  int array_w_num;                         ///< 横の画像の数
  int array_h_num;                         ///< 縦の画像の数
  int image_width;                         ///< 1つの画像の幅
  int image_height;                        ///< 1つの画像の高さ
  int image_margin;                        ///< 画像間のマージン
  bool is_alloc;                           ///< pixbufを確保しているか

  guint8 *array_p_data;                    ///< RGBデータの開始位置
  int array_channels;                      ///< 1pixel当たりのデータ数
  int array_stride;                        ///< 1行当たりのデータ数
};

}

#endif // NISTK_SAVEARRAY_H
