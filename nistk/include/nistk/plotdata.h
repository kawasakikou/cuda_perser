/**
 * @file plotdata.h
 * @brief class for manage plotdata
 *
 * @author Masakazu Komori
 * @date 2011-08-10
 * @version $Id: plotdata.h,v.20110810 $
 *
 * Copyright (C) 2011 Masakazu Komori
 */

#ifndef NISTK_POLT_DATA
#define NISTK_POLT_DATA

#include<vector>


namespace Nistk{
/** PlotDrawableクラスのためのデータを管理するクラス
 *
 * PlotDrawableクラスのためのデータを管理するクラス。PlotDrawableクラスは
 * PlotViewクラスで利用されるので、最終的にはグラフのためのデータを管理する
 * ためのクラスであるといえる。このクラス単体でも使用可能ではあるが使い方次第である。
 * このクラスで管理するデータはグラフ1つ分であるが、グラフで複数のプロットが可能となる
 * ように、複数のデータ領域を持つことが可能となっている。ただし、全体で使用することが
 * できる実メモリ(この場合、プログラム全体で使えるメモリ量)を考慮していないので、
 * その辺は注意が必要である。
 * <br>
 *
 * Copyright (C) 2011 Masakazu Komori
 */

class PlotData
{
 protected:
  std::vector<double> *x_data;      ///< x軸のデータ用ベクタクラスのポインタ
  std::vector<double> *y_data;      ///< y軸のデータ用ベクタクラスのポインタ
  int data_num;                     ///< プロット用データの数
  int increment_capacity;           ///< メモリ領域の増分
  double x_data_max;                ///< x軸のデータの最大値
  double x_data_min;                ///< x軸のデータの最小値
  double y_data_max;                ///< y軸のデータの最大値
  double y_data_min;                ///< y軸のデータの最小値
  bool auto_range;                  ///< オートレンジのフラグ
  bool *raster;                      ///< ラスター図のフラグのポインタ
  ///< ラスター図のファイル出力時の点の大きさのポインタ(scaleの初期値は0.005)
  double *raster_scale;

 public:
  PlotData();                               ///< コンストラクタ
  ~PlotData();                              ///< デストラクタ
  void create_data_region(int num);         ///< プロット用データ領域の作成関数
  void delete_data_region();                ///< プロット用データ領域の削除関数
  int get_data_num();                ///< プロット用データ領域の数を取得する関数
  /// x,y軸の最大値、最小値をセットする関数
  void set_data_range(double x_min, double x_max, double y_min, double y_max);
  /// プロット領域のメモリ領域の増分をセットする関数
  void set_increment_capacity(int size);
  /// オートレンジをセットする関数
  void set_auto_range();
  /// num番目のデータ領域の末尾にデータをセットする関数
  void push_back_data(int num, double x, double y);
  /// num番目のデータ領域のindex番目のxのデータを取得する関数
  double get_x(int num, int index);
  /// num番目のデータ領域のindex番目のyのデータを取得する関数
  double get_y(int num, int index);
  /// num番目のデータ領域のindex番目のxにデータをセットする関数
  void set_x(int num, int index, double data);
  /// num番目のデータ領域のindex番目のyにデータをセットする関数
  void set_y(int num, int index, double data);
  /// num番目のデータ領域のサイズを取得する関数
  int get_size(int num);
  /// num番目のデータ領域のデータを消去する関数
  void clear(int num);
  double get_x_data_max();                  ///< x_data_maxを取得する関数
  double get_x_data_min();                  ///< x_data_minを取得する関数
  double get_y_data_max();                  ///< y_data_maxを取得する関数
  double get_y_data_min();                  ///< y_data_minを取得する関数
  /// num番目のデータ領域のデータをファイルに出力する関数
  void save_data(int num, const char *file_name);
  /// num番目のデータ領域のデータをラスター図とするかどうかのフラグをセットする関数
  void set_raster(int num, bool flag);
  /// num番目のデータ領域のデータをラスター図とするかどうかのフラグを取得する関数
  bool get_raster(int num);
  /// num番目のデータ領域のデータのraster_scaleをセットする関数
  void set_raster_scale(int num, double val);
  /// num番目のデータ領域のデータのraster_scaleをセットする関数
  double get_raster_scale(int num);
};

}

#endif //NISTK_POLT_DATA
