/**
 * @file imagedrawable.h
 * @brief class for imagedrawable used window
 *
 * @author Masakazu Komori
 * @date 2011-05-21
 * @version $Id: imagedrawable.h,v.20110521 $
 *
 * Copyright (C) 2011 Masakazu Komori
 */

#ifndef NISTK_IMAGEDRAWABLE_H
#define NISTK_IMAGEDRAWABLE_H
#include<gtkmm.h>

namespace Nistk{
/** Gtk::DrawingAreaを使って描画領域にレンダリングするクラス
 *
 * Gtk::DrawingAreaを使ってpixbufに取り込んだ画像を
 * 描画領域にレンダリングする。ImageViewクラスと分けたのは
 * 一見無駄なようだが使いまわしができるようにするため。
 *
 * Copyright (C) 2010 Masakazu Komori
 */
class ImageDrawable : public Gtk::DrawingArea
{
 protected:
  Glib::RefPtr<Gdk::GC> m_gc;         ///< グラフィックコンテキスト
  Glib::RefPtr<Gdk::Pixbuf> m_pixbuf; ///< pixbufのポインタ 
  bool show_flag;                     ///< 画面が表示されているかのフラグ
 public:
  ImageDrawable();  ///< コンストラクタ 引数なし
  /// コンストラクタ 生成と同時にpixbufポインタをセットする
  ImageDrawable(Glib::RefPtr<Gdk::Pixbuf> i_pixbuf);
  ImageDrawable(const Nistk::ImageDrawable &obj); ///< コピーコンストラクタ
  virtual ~ImageDrawable();                       ///< デストラクタ 
  /// 代入演算子=のオーバーロード
  Nistk::ImageDrawable &operator=(Nistk::ImageDrawable &obj);
  /// pixbufポインタをセットする関数
  virtual void set_pixbuf(Glib::RefPtr<Gdk::Pixbuf> i_pixbuf);
  /// Drawableのpixbufをコピーして他のオブジェクトに渡す関数
  virtual Glib::RefPtr<Gdk::Pixbuf> get_pixbuf(Glib::RefPtr<Gdk::Pixbuf> 
					                        i_pixbuf);
  ///自動生成の関数でユーザーが使うことはない
  virtual void on_realize();
  ///自動描画の関数でユーザーが使うことはない
  virtual bool on_expose_event(GdkEventExpose *event);
  bool is_show();                     ///< showフラグの値を取得する関数
  void set_show_flag(bool sflag);     ///< showフラグをセットする関数
  ///再描画の関数
  virtual void redraw();
};

}  

#endif // NISTK_IMAGEDRAWABLE_H
