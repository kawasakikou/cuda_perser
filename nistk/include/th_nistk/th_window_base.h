/**
 * @file  th_window_base.h
 * @brief class for make thread-based window class
 *
 * @author Masakazu Komori
 * @date 2011-05-21
 * @version $Id: th_window_base.h,v.20110521 $
 *
 * Copyright (C) 2010-2011 Masakazu Komori
 */


#ifndef NISTK_TH_WINDOW_BASE_H
#define NISTK_TH_WINDOW_BASE_H

#include<gtkmm.h>

#define NISTK_THREAD_TIMEOUT 50


namespace Nistk{
/** Thread対応のクラス用ウィンドウクラスを作るためのクラス（β版）
 * 
 * Thread対応のクラス用ウィンドウクラスを作るためのクラス。
 * このクラスを継承することによりNistkThreadクラスを継承した
 * thread対応のクラスで使用可能となるウィンドウ関連の処理をする
 * クラスを作ることができる。ウィンドウの終了に関しては終了フラグと
 * チェック用タイムアウト関数を使っている。ただし、タイムアウト用関数である
 * slot_window_funcはウィンドウ終了チェックの後に純粋仮想関数time_out_win_func
 * を呼出している。これは、ウィンドウ終了のみならずこれを使って他の処理が
 * 出来るようにするためである。純粋仮想関数が３つあるので注意すること。<br>
 * thread関連のクラスはバグがあるのか動作が安定しないのでこのクラス
 * はβ版としている。<br><br>
 *
 * time_out_win_func(純粋仮想関数)について<br><br>
 *
 * タイムアウト用スロット関数slot_window_funcから呼出される。
 * ウィンドウ終了処理以外に処理をさせたいときにこの関数に
 * その処理を書く。通常は、フラグを用意してそれの条件分岐が望ましい。
 * 何も必要がないときには空の関数を書けばよい。<br><br>
 * 
 * on_delete_event(純粋仮想関数)について<br><br>
 *
 * スレッドが実行中にxボタンが押されても終了しないようにする。
 * そうしないと。threadプログラムが不安定になる。次のように
 * 記述すればよい。ただし、元のクラスが関数で処理している場合は必要ない<br>
 * bool クラス名::on_delete_event(GdkEventAny*) <br>
 * { <br>
 *  return true; <br>
 * } 
 *
 * close_window(純粋仮想関数)について<br><br>
 *
 * 実際にウィンドウを終了させる関数。次のように
 * 記述すればよい。<br>
 * void クラス名::close_window() <br>
 * {<br>
 *  hide();<br>
 * }<br>
 *
 * Copyright (C) 2010 Masakazu Komori
 */

class ThWindowBase
{
 protected:
  bool is_window_close;             ///< ウィンドウ終了フラグ
  sigc::connection conn_close;      ///< 終了チェック用タイムアウトコネクタ 
  /// deleteイベント(xボタン)に関する処理の関数(純粋仮想関数)
  virtual bool on_delete_event(GdkEventAny*)=0;

 public:
  ThWindowBase();                     ///< コンストラクタ
  ~ThWindowBase();                    ///< デストラクタ
  bool slot_window_func();            ///< タイムアウト用スロット関数
  virtual void time_out_win_func()=0; ///< タイムアウト用関数(純粋仮想関数)
  void set_window_close();            ///< ウィンドウ終了フラグセット用関数
  virtual void close_window()=0;      ///< ウィンド終了処理用関数(純粋仮想関数)
};

}

#endif // NISTK_TH_WINDOW_BASE_H


