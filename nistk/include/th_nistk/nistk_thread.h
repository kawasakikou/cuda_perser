/**
 * @file  nistk_thread.h
 * @brief class for make thread-based class
 *
 * @author Masakazu Komori
 * @date 2010-08-12
 * @version $Id: nistk_thread.h,v.20100812 $
 *
 * Copyright (C) 2010 Masakazu Komori
 */

#ifndef NISTK_THREAD_H
#define NISTK_THREAD_H

#include<gtkmm.h>

#define NISTK_THREAD_WAIT 1


namespace Nistk{
/** Thread対応のクラスを作るためのクラス（β版）
 * 
 * Thread対応のクラスを作るためのクラス。このクラスを継承することにより
 * Thread対応のクラスにすることができる。ウィンドウ関係のものをThread化
 * するためには、th_window_baseクラスを継承した新しいクラスをつくり、
 * そのクラスをこのクラスを継承したクラスのメンバとする必要がある。
 * nistk_slot_main関数とsend_th_end関数は純粋仮想関数であるので、
 * 注意すること。Threadは、nistk_slot_main関数が終了してもjoinするまで
 * 終了しないようになっているので注意すること。また、threadを扱う前には、
 * 必ずスレッドシステムの初期化が必要である。gtkmmの初期化も含めると
 * 次の様な記述となる。<br>
 *  Glib::thread_init();<br>
 *  Gtk::Main kit(argc,argv);<br>
 * バグがあるのか動作が安定しないのでこのクラスはβ版としている。
 * このクラスを継承しているクラスも同様である。 <br> 
 * <br>
 * nistk_slot_main(純粋仮想関数)について <br><br>
 *
 * threadで実行されるメイン関数。純粋仮想関数であるので、
 * 継承したクラスで実行する必要がある。ウィンドウ関連の
 * クラスであれば<br>
 * Gtk::Main::run(オブジェクト)<br>
 * を実行することになる。
 * <br><br>
 * send_th_end(純粋仮想関数)について<br><br>
 *
 * threadがウィンドウ関連の処理であれば、th_window_baseクラス
 * を継承したクラスのオブジェクトを使っているはずであるので、継承
 * したクラスで次のような記述を使えばよい。<br>
 *  sleep(1);<br>
 *  th_mutex.lock();<br>
 *  if(in_main == true){<br>
 *   オブジェクト.set_window_close();<br>
 *  }<br>
 *  th_mutex.unlock();<br>
 * ウィンドウ関連でないのなら、空の関数で良い。
 *
 * Copyright (C) 2010 Masakazu Komori
 */

// Glib::StaticMutex th_mutex = GLIBMM_STATIC_MUTEX_INIT;  // 排他的処理用mutex
// Glib::Mutex th_mutex;    // 排他的処理用mutex

class NistkThread
{
 protected:
  Glib::Thread* thread;      ///< thread用ポインタ
  bool in_thread;            ///< threadが走っているかどうかのフラグ
  bool wait_join;            ///< join待ちかどうかのフラグ。threadは終了していない
  bool in_main;              ///< スロット関数nistk_slot_main実行中かどうか
  int nistk_thread_id;       ///< threadのid用（動作がおかしい）
  // Glib::Mutex th_mutex;            // 排他的処理用mutex
  // static Glib::Mutex th_mutex;     // 排他的処理用mutex
  static Glib::StaticMutex th_mutex;  ///< 排他的処理用mutex(static)
  int data;

 public:
  NistkThread();                     ///< コンストラクタ
  ~NistkThread();                    ///< デストラクタ
  void thread_run();                ///< threadを生成実行する関数
  void thread_join();               ///< threadのjoinをする関数
  bool is_thread_run();             ///< threadの状態を返す関数
  bool is_thread_wait_join();       ///< threadのjoin待ちかを返す関数
  bool is_main_run();              ///< threadのスロット用main関数の状態を返す関数
  void slot_thread();               ///< threadのスロット関数
  virtual void nistk_slot_main()=0;///< threadのスロット用メイン関数(純粋仮想関数)
  virtual void send_th_end()=0;   ///< ウィンドウウィジット終了用関数(純粋仮想関数)
  void set_thread_id(int i);        ///< threadのidをセットする関数
  int get_thread_id();              ///< threadのidを取得する関数
};

}

#endif // NISTK_THREAD_H








