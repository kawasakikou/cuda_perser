#include<iostream>
#include<nistk.h>

#define PLOT_VIEW_NUM 5   // PlotViewの数
#define PLOT_WIDTH  300   // グラフの幅
#define PLOT_HEIGHT 100   // グラフの幅
#define LOOP_NUM 1000     // 乱数発生の繰り返し回数
#define HIST_NUM 20       // 度数分布の数(実数乱数は整数化してカウント)
#define ALPHA 1           // 指数分布の期待値の逆数
#define POISSON_M 1       // ポアソン分布の期待値
#define ERLANG_LAMBDA 1.0 // アーラン分布のパラメータ
#define ERLANG_N 8        // アーラン分布のパラメータ
#define GAUSSIAN_EX 10.0  // 正規分布の期待値
#define GAUSSIAN_VX 2.0   // 正規分布の分散

int main(int argc, char **argv)
{
 // gtkmmを使う際には必ず必要。まず、最初にやる。
  Gtk::Main kit(argc,argv);
  // オブジェクトの宣言
  Nistk::NistkMainWin main_window;
  char plot_name[PLOT_VIEW_NUM][15] = {"exponential","poisson",
				       "erlang","gaussian","histgram"};
  // 度数分布による乱数生成のための配列等の設定
  // これだけはここに書くしかない。
  int h_num = 11;
  double x[11] = {0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0};
  double p[11] = {0.0, 0.02, 0.36, 0.34, 0.13, 0.07, 
		  0.03, 0.02, 0.01, 0.01, 0.01};
  //
  Nistk::RandGene2 rand_gene;
  int hist[HIST_NUM];           // 度数分布用配列
  int rand_num;
  int i;

 /*** 
   * プロット用ウィンドウの設定
  ***/
  main_window.set_size_request(700,150);
  // PlotViewの生成と名前のセット
  main_window.create_plotview(PLOT_VIEW_NUM);
  for(i = 0; i < PLOT_VIEW_NUM; i++) 
    main_window.set_plotview_name(i,plot_name[i]);

 // グラフを横2個、縦1個として縦横の大きさとマージンを設定
  for(i = 0; i < PLOT_VIEW_NUM; i++){
    main_window.get_plotview_ptr(i)->set_graph_area(1, 1, PLOT_WIDTH, 
						    PLOT_HEIGHT, 30, 30);
    main_window.pd_ptr(i,0,0)->create_data_region(1);
    main_window.pd_ptr(i,0,0)->set_data_range(-1.0, HIST_NUM, 0.0, LOOP_NUM);
  }
  std::cout << "pass 1 \n";

  // 指数分布
  // 度数分布用配列の初期化
  for(i = 0; i < HIST_NUM; i++) hist[i] = 0;
  // 乱数の発生と度数のカウント
  for(i = 0; i < LOOP_NUM; i++){
    rand_num = (int)rand_gene.get_rand_exp(ALPHA);
    if(rand_num < HIST_NUM) hist[rand_num] = hist[rand_num] + 1; 
  }
  // 度数分布のプロットへのセット
  for(i = 0; i < HIST_NUM; i++){
    main_window.pd_ptr(0,0,0)->push_back_data(0, (double)i, 0.0);
    main_window.pd_ptr(0,0,0)->push_back_data(0, (double)i, (double) hist[i]);
    main_window.pd_ptr(0,0,0)->push_back_data(0, (double)i, 0.0);
  }
  std::cout << "pass 2 \n";

  // ポアソン分布
  // 度数分布用配列の初期化
  for(i = 0; i < HIST_NUM; i++) hist[i] = 0;
  // 乱数の発生と度数のカウント
  for(i = 0; i < LOOP_NUM; i++){
    rand_num = rand_gene.get_rand_poisson(POISSON_M);
    if(rand_num < HIST_NUM) hist[rand_num] = hist[rand_num] + 1; 
  }
  // 度数分布のプロットへのセット
  for(i = 0; i < HIST_NUM; i++){
    main_window.pd_ptr(1,0,0)->push_back_data(0, (double)i, 0.0);
    main_window.pd_ptr(1,0,0)->push_back_data(0, (double)i, (double) hist[i]);
    main_window.pd_ptr(1,0,0)->push_back_data(0, (double)i, 0.0);
  }
  std::cout << "pass 3 \n";

  // アーラン分布
  // 度数分布用配列の初期化
  for(i = 0; i < HIST_NUM; i++) hist[i] = 0;
  // 乱数の発生と度数のカウント
  for(i = 0; i < LOOP_NUM; i++){
    rand_num = rand_gene.get_rand_erlang(ERLANG_LAMBDA, ERLANG_N);
    if(rand_num < HIST_NUM) hist[rand_num] = hist[rand_num] + 1; 
  }
  // 度数分布のプロットへのセット
  for(i = 0; i < HIST_NUM; i++){
    main_window.pd_ptr(2,0,0)->push_back_data(0, (double)i, 0.0);
    main_window.pd_ptr(2,0,0)->push_back_data(0, (double)i, (double) hist[i]);
    main_window.pd_ptr(2,0,0)->push_back_data(0, (double)i, 0.0);
  }
  std::cout << "pass 4 \n";

  // 正規分布
  // 度数分布用配列の初期化
  for(i = 0; i < HIST_NUM; i++) hist[i] = 0;
  // 乱数の発生と度数のカウント
  for(i = 0; i < LOOP_NUM; i++){
    rand_num = (int)rand_gene.get_rand_gaussian(GAUSSIAN_EX, GAUSSIAN_VX);
    if((rand_num > 0) && (rand_num < HIST_NUM)) 
                              hist[rand_num] = hist[rand_num] + 1; 
  }
  // 度数分布のプロットへのセット
  for(i = 0; i < HIST_NUM; i++){
    main_window.pd_ptr(3,0,0)->push_back_data(0, (double)i, 0.0);
    main_window.pd_ptr(3,0,0)->push_back_data(0, (double)i, (double) hist[i]);
    main_window.pd_ptr(3,0,0)->push_back_data(0, (double)i, 0.0);
  }
  std::cout << "pass 5 \n";

  // 度数分布
  // 度数分布用配列の初期化
  for(i = 0; i < HIST_NUM; i++) hist[i] = 0;
  // 乱数の発生と度数のカウント
  for(i = 0; i < LOOP_NUM; i++){
    rand_num = (int)(rand_gene.get_rand_hist(x, p, h_num) * 10.0);
    if((rand_num > 0) && (rand_num < HIST_NUM)) 
                              hist[rand_num] = hist[rand_num] + 1; 
  }
  // 度数分布のプロットへのセット
  for(i = 0; i < HIST_NUM; i++){
    main_window.pd_ptr(4,0,0)->push_back_data(0, (double)i, 0.0);
    main_window.pd_ptr(4,0,0)->push_back_data(0, (double)i, (double) hist[i]);
    main_window.pd_ptr(4,0,0)->push_back_data(0, (double)i, 0.0);
  }
  std::cout << "pass 5 \n";

  // メインウィンドウを起動して表示
  Gtk::Main::run(main_window);


  return 0;
}

