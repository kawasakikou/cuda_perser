#include<iostream>
#include<cmath>
#include<nistk.h>

#define PLOT_VIEW_NUM 1
#define FROM_T 0.0
#define END_T 100.0
#define NODE_NUM 100.0
#define THRESHOLD 0.9

int main(int argc, char **argv)
{
 // gtkmmを使う際には必ず必要。まず、最初にやる。
  Gtk::Main kit(argc,argv);
  // オブジェクトの宣言
  Nistk::NistkMainWin main_window;
  Nistk::RandGene m_rand;
  // PlotView用名前の文字列の配列
  char plot_name[PLOT_VIEW_NUM][15] = {"raster_test"};
  char file_name[15] = "test1.dat";

  double t,node,val;
  int i;

  main_window.set_size_request(200,150);
  // PlotViewの生成と名前のセット
  main_window.create_plotview(PLOT_VIEW_NUM);
  for(i = 0; i < PLOT_VIEW_NUM; i++) 
    main_window.set_plotview_name(i,plot_name[i]);

 // グラフを横2個、縦2個として縦横の大きさとマージンを設定
  for(i = 0; i < PLOT_VIEW_NUM; i++){
    main_window.get_plotview_ptr(i)->set_graph_area(1,1,400,200,80,30);
    main_window.pd_ptr(i,0,0)->create_data_region(1);
  }

  // 
  main_window.pd_ptr(0,0,0)->set_raster(0, true);
  for(t = FROM_T; t <= END_T; t += 1.0){
    for(node = 0.0; node <= NODE_NUM; node += 1.0){
      val = m_rand.get_rand_d();
      if(val >= THRESHOLD) 
	main_window.pd_ptr(0,0,0)->push_back_data(0, node, t);
    }
  }
  // データを保存
  main_window.pd_ptr(0,0,0)->save_data(0,file_name);

 // メインウィンドウを起動して表示
  Gtk::Main::run(main_window);

  return 0;
}
