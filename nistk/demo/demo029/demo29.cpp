#include<iostream>
#include<gtkmm.h>
#include<nistk.h>

#define IMAGE_VIEW_NUM 1
#define I_WIDTH  101       // 荷重の幅
#define I_HEIGHT 101       // 荷重の高さ
#define STEP 0.1           // 計算の刻み幅
#define PAI 3.141592

int main(int argc,char *argv[])
{
  // gtkmmを使う際には必ず必要。まず、最初にやる。
  Gtk::Main kit(argc,argv);
  // オブジェクトの宣言
  Nistk::NistkMainWin main_window;
  Nistk::DataFileOut d_file;
  // PlotView用名前の文字列の配列
  char image_name[IMAGE_VIEW_NUM][15] = {"input_test"};
  Glib::RefPtr<Gdk::Pixbuf> in_image;   // 画像用pixbuf
  Nistk::SimData in_data;               // simdat
  Nistk::SimData in_data2;               // simdat2

  double x, y;
  double x_start, y_start;
  int x_end, y_end;
  double buf;
  int i,j;

  main_window.set_size_request(200,150);
  // ImageViewの生成と名前のセット
  main_window.create_imageview(IMAGE_VIEW_NUM);
  for(i = 0; i < IMAGE_VIEW_NUM; i++) 
    main_window.set_imageview_name(i,image_name[i]);
  in_data.create_data_size(I_WIDTH,I_HEIGHT);
  in_data2.create_data_size(I_WIDTH,I_HEIGHT);

  in_image = Nistk::ImageData::create_pixbuf_new(in_data.get_width(),
						  in_data.get_height());


  /***
    * simdata_from_DoG_rを利用してin_dataに格納してそれを表示
  ***/
  Nistk::MathTools::simdata_from_DoG_r(&in_data, 0.0, 0.0, 1.0, 1.0,
                   1.0, 1.0, 10, 15, 50, true);
  // 合計が0となっているか確認
  buf = 0.0;
  x_end = in_data.get_width();
  y_end = in_data.get_height();
  y = y_start;
  for(i = 0; i < y_end; i++){
    for(j = 0; j < x_end; j++){
      buf += in_data.get_data(j, i);
    }
  } 
  std::cout << "total1 =" << buf << '\n';
  Nistk::ImageData::pixbuf_from_simdata(&in_data,in_image);
  main_window.get_imageview_ptr(0)->set_image(in_image);
  Gtk::Main::run(main_window);
  std::cout << "pass 1\n";

  Nistk::MathTools::simdata_from_DoG_r(&in_data, 0.0, 0.0, 1.0, 1.0,
                   1.0, 1.0, 55, 10, 50, true);
  // 合計が0となっているか確認
  buf = 0.0;
  x_end = in_data.get_width();
  y_end = in_data.get_height();
  y = y_start;
  for(i = 0; i < y_end; i++){
    for(j = 0; j < x_end; j++){
      buf += in_data.get_data(j, i);
    }
  } 
  std::cout << "total2 =" << buf << '\n';
  Nistk::ImageData::pixbuf_from_simdata(&in_data,in_image);
  main_window.get_imageview_ptr(0)->set_image(in_image);
  Gtk::Main::run(main_window);
  std::cout << "pass 2\n";


  return 0;
}

