/* 二元連立微分方程式の4次のルンゲクッタ法によるシミュレーション
 *
 * 連立微分方程式
 *  dx/dt = y
 *  dy/dt = x
 * 連立微分方程式の解
 *  x = 1/2 * (exp(t) - exp(-t))
 *  x = 1/2 * (exp(t) + exp(-t))
 */
#include<iostream>
#include<cmath>
#include<nistk.h>

#define PLOT_VIEW_NUM 1
#define DT 0.01
#define FROM_T 0.0
#define END_T 2.0

// テスト用関数のプロトタイプ宣言
// 原関数
double funcx(double t);
double funcy(double t);
// 微分方程式
double dxdt(double t, double *x, double p1, double p2);
double dydt(double t, double *y, double p1, double p2);

int main(int argc, char **argv)
{
 // gtkmmを使う際には必ず必要。まず、最初にやる。
  Gtk::Main kit(argc,argv);
  // オブジェクトの宣言
  Nistk::NistkMainWin main_window;
  // PlotView用名前の文字列の配列
  char plot_name[PLOT_VIEW_NUM][15] = {"runge test"};
  // 関数ポインタの配列
  double (*func[2])(double fx, double *fy, 
		    double para1, double para2) = {dxdt, dydt};
  double x[2] = {0.0, 1.0};     // x:x[0] y:x[1] 初期値も設定
  char file_name[][15] = {"test1.dat", "test2.dat", "test3.dat", "test4.dat"};
  double t,y,dt;
  double buf_t,buf_x,diff;
  int i;

  main_window.set_size_request(200,150);
  // PlotViewの生成と名前のセット
  main_window.create_plotview(PLOT_VIEW_NUM);
  for(i = 0; i < PLOT_VIEW_NUM; i++) 
    main_window.set_plotview_name(i,plot_name[i]);

 // グラフを横2個、縦3個として縦横の大きさとマージンを設定
  for(i = 0; i < PLOT_VIEW_NUM; i++){
    main_window.get_plotview_ptr(i)->set_graph_area(2,3,200,100,80,30);
    main_window.pd_ptr(i,0,0)->create_data_region(1);
    main_window.pd_ptr(i,1,0)->create_data_region(1);
    main_window.pd_ptr(i,0,1)->create_data_region(1);
    main_window.pd_ptr(i,1,1)->create_data_region(1);
    main_window.pd_ptr(i,0,2)->create_data_region(1);
    main_window.pd_ptr(i,1,2)->create_data_region(1);
  }

  // dtの設定
  dt = DT;
  // 原関数の計算をして格納
  for(t = FROM_T; t <= END_T; t += dt){
    y = funcx(t);
    main_window.pd_ptr(0,0,0)->push_back_data(0, t, y);
    y = funcy(t);
    main_window.pd_ptr(0,1,0)->push_back_data(0, t, y);
  }
  // 4次のルンゲクッタで計算して格納
  for(t = FROM_T; t <= END_T; t += dt){
    main_window.pd_ptr(0,0,1)->push_back_data(0, t, x[0]);
    main_window.pd_ptr(0,1,1)->push_back_data(0, t, x[1]);
    Nistk::Temp::Runge4Si<double,double>(func, t, x, 0.0, 0.0, dt, 2);
  }  

  // 元関数の値とルンゲクッタでの計算値の差を求めて格納
  for(i = 0; i < main_window.pd_ptr(0,0,0)->get_size(0); i++){
    buf_t = main_window.pd_ptr(0,0,0)->get_x(0,i);
    // x
    buf_x = main_window.pd_ptr(0,0,0)->get_y(0,i);
    diff = buf_x - main_window.pd_ptr(0,0,1)->get_y(0,i);
    main_window.pd_ptr(0,0,2)->push_back_data(0, buf_t, diff);
    // y
    buf_x = main_window.pd_ptr(0,1,0)->get_y(0,i);
    diff = buf_x - main_window.pd_ptr(0,1,1)->get_y(0,i);
    main_window.pd_ptr(0,1,2)->push_back_data(0, buf_t, diff);
  }

  // データを保存
  main_window.pd_ptr(0,0,0)->save_data(0,file_name[0]);
  main_window.pd_ptr(0,1,0)->save_data(0,file_name[1]);
  main_window.pd_ptr(0,0,1)->save_data(0,file_name[2]);
  main_window.pd_ptr(0,1,1)->save_data(0,file_name[3]);

 // メインウィンドウを起動して表示
  Gtk::Main::run(main_window);

  return 0;
}

// テスト用関数の定義
double funcx(double x)
{
  double ret;

  ret = 0.5 * (exp(x) - exp(-1.0 * x));
  
  return ret;
}

double funcy(double y)
{
  double ret;

  ret = 0.5 * (exp(y) + exp(-1.0 * y));
  
  return ret;
}

double dxdt(double t, double *x, double p1, double p2)
{
  double ret;

  ret = x[1];
  
  return ret;
}

double dydt(double t, double *y, double p1, double p2)
{
  double ret;

  ret = y[0];
  
  return ret;
}
