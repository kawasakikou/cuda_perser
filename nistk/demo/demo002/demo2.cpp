#include<iostream>
#include<gtkmm.h>
// #include<imageview.h>
// #include<imagedata.h>
// #include<simdata.h>
#include<nistk.h>

int main(int argc,char *argv[])
{
  // gtkmmを使う際には必ず必要。まず、最初にやる。
  Gtk::Main kit(argc,argv);
  // オブジェクトの宣言
  Glib::RefPtr<Gdk::Pixbuf> m_pixbuf,m_pixbuf2;
  Nistk::ImageLoader i_loader;
  Nistk::GrayData s_data;
  Nistk::ImageView window;
  // 変数の宣言
  char *if_name;
  char w_name[][15] = {"test0", "gray-image", };
  char file_path[] = "../image/i_file.txt";
  int f_num;




  // 画像リストファイルを読み込む
  // そして、ファイル数をf_numにセット
  i_loader.set_input_file(file_path);
  f_num=i_loader.get_input_file_num();
  // リストファイル名を取得して表示
  if_name = i_loader.get_list_file_name(); 
  std::cout << "list file=" << if_name << '\n';

  // ローダを利用して画像ファイルを2周分
  // 読み込んで表示
  for(int i=0; i<2*f_num; i++){
    m_pixbuf = i_loader.load_image_next();
    if_name = i_loader.get_input_file_name();
    window.set_name(w_name[0]);
    window.set_image(m_pixbuf);
    Gtk::Main::run(window);
    std::cout << "pass 1 file=" << i << ' ' << if_name << "\n";
  }

  // pixbufよりグレイデータを生成し、高さと幅を表示
  // まずは、画像のサイズよりグレイデータクラスの
  // サイズを設定する必要がある。
  s_data.create_data_size(m_pixbuf->get_width(),m_pixbuf->get_height());
  // 生成したデータクラスの大きさの表示
  std::cout << "pass 2 width=" << m_pixbuf->get_width() << " height=" 
	    <<m_pixbuf->get_height() << '\n';
  // pixbufからグレイデータを生成
  Nistk::ImageData::graydata_from_pixbuf(&s_data,m_pixbuf,2);
  std::cout << "pass 3 \n";

  // 新しく空のpixbufを大きさを指定して生成
  // ここでは、グレイデータの大きさに設定
  m_pixbuf2 = Nistk::ImageData::create_pixbuf_new(s_data.get_width(),
						  s_data.get_height());
  // 大きさを表示
  std::cout << "pass 4 width=" << m_pixbuf2->get_width() << " height=" 
	    << m_pixbuf2->get_height() << '\n';

  // グレイデータをpixbufに張り付け
  Nistk::ImageData::pixbuf_from_graydata(&s_data,m_pixbuf2);
  std::cout << "pass 4-2 \n";

  // 大きさを1/4に変更
  m_pixbuf2=Nistk::ImageData::resize_pixbuf(m_pixbuf2, 
	     m_pixbuf2->get_width()/4, m_pixbuf2->get_height()/4);
  std::cout << "pass 5 "<< m_pixbuf2->get_width() << " height=" 
	    << m_pixbuf2->get_height() << '\n';

  // ウィンドウに新しいpixbufをセットして、イベント処理を開始
  window.set_name(w_name[1]);
  window.set_image(m_pixbuf2);
  Gtk::Main::run(window);
  std::cout << "pass 6 \n";
  // pixbufをファイル名を指定してセーブ
  char o_file[] = "test-out";
  char o_type[] = "png";
  Nistk::ImageData::save_pixbuf(m_pixbuf2,o_file,o_type);

  return 0;
}
