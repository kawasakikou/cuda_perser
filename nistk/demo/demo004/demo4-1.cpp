#include<iostream>
#include<gtkmm.h>
#include<nistk.h>

int main(int argc,char *argv[])
{
// gtkmmを使う際には必ず必要。まず、最初にやる。
  Gtk::Main kit(argc,argv);
  // オブジェクトの宣言
  Glib::RefPtr<Gdk::Pixbuf> m_pixbuf;
  Nistk::ImageView window;
  Nistk::DataFileOut d_file;
  Nistk::SimData s_data;
  // 変数の宣言
  double x,y,val,total1,total2,sum,xd,yd;
  char file_name[7][15] = {"test1.dat", "test2.dat", "test3.dat",
			  "test4.dat", "test5.dat", "test6.dat",
			  "test7.dat"}; 
  char func_name[7][25] = {"Step function", "Sigmoid function",
		      "Gaussian function(2D)", "DoG(2D)",
		      "Gaussian function(3D)", "DoG(3D)",
		      "DoG(3D) SimData"};
  char win_name[] = "DoG-image";

  // ステップ関数を計算してファイルに書きだし
  d_file.open(file_name[0],false);
  d_file.insert_comment(func_name[0]);
  for(x=-1.0; x<=1.0; x+=0.01){
    val = Nistk::MathTools::step_func(x);
    d_file.write(x,val);
  }
  d_file.close();

  // シグモイド関数を計算してファイルに書きだし
  d_file.open(file_name[1],false);
  d_file.insert_comment(func_name[1]);
  for(x=-1.0; x<=1.0; x+=0.01){
    val = Nistk::MathTools::sigmoid_func(x,5.0);
    d_file.write(x,val);
  }
  d_file.close();

  // ガウス関数2Dを計算してファイルに書きだし
  d_file.open(file_name[2],false);
  d_file.insert_comment(func_name[2]);
  total1 = Nistk::MathTools::gaussian_2D_sum(3.0, -10.0, 10.0, 0.1, 2.0, true);
  sum = 0.0;
  for(x=-10.0; x<=10.0; x+=0.1){
    val = Nistk::MathTools::gaussian_2D(x, 3.0, 1/total1, 2.0);
    d_file.write(x,val);
    sum += val;
  }
  d_file.close();
  std::cout << "Gaussian function(2D) sum=" << sum << '\n';

  // DoG 2Dを計算してファイルに書きだし
  d_file.open(file_name[3],false);
  d_file.insert_comment(func_name[3]);
  total1 = Nistk::MathTools::gaussian_2D_sum(3.0, -10.0, 10.0, 0.1, 2.0, true);
  total2 = Nistk::MathTools::gaussian_2D_sum(3.0, -10.0, 10.0, 0.1, 3.0, true);
  sum = 0.0;
  for(x=-10.0; x<=10.0; x+=0.1){
    val = Nistk::MathTools::diff_of_gaussian_2D(x, 3.0, 1/total1, 1/total2, 
						                    2.0, 3.0);
    d_file.write(x,val);
    sum += val;
  }
  d_file.close();
  std::cout << "DoG(2D) sum=" << sum << '\n';

  // ガウス関数3Dを計算してファイルに書きだし
  d_file.open(file_name[4],true);
  d_file.insert_comment(func_name[4]);
  total1 = Nistk::MathTools::gaussian_3D_sum(3.0, 0.0, -10.0, 10.0, 
				       -10.0, 10.0, 0.3, 0.3, 2.0, true);
  sum = 0.0;
  for(y=-10.0; y<=10.0; y+=0.3){
    for(x=-10.0; x<=10.0; x+=0.3){
      val = Nistk::MathTools::gaussian_3D(x, y, 3.0, 0.0, 1/total1, 2.0);
      d_file.write(x, y, val);
      sum += val;
    }
  }
  d_file.close();
  std::cout << "Gaussian function(3D) sum=" << sum << '\n';

  // DoG 3Dを計算してファイルに書きだし
  d_file.open(file_name[5],true);
  d_file.insert_comment(func_name[5]);
  total1 = Nistk::MathTools::gaussian_3D_sum(3.0, 0.0, -10.0, 10.0, 
					 -10.0, 10.0, 0.3, 0.3, 2.0, true);
  total2 = Nistk::MathTools::gaussian_3D_sum(3.0, 0.0, -10.0, 10.0, 
					 -10.0, 10.0, 0.3, 0.3, 3.0, true);
  sum = 0.0;
  for(y=-10.0; y<=10.0; y+=0.3){
    for(x=-10.0; x<=10.0; x+=0.3){
      val = Nistk::MathTools::diff_of_gaussian_3D(x, y, 3.0, 0.0, 1/total1, 
					               1/total2, 2.0, 3.0);
      d_file.write(x, y, val);
      sum += val;
    }
  }
  d_file.close();
  std::cout << "DoG(3D) sum=" << sum << '\n';

   // DoG 3DをSimData型に格納してしてファイルに書きだし
  d_file.open(file_name[6],true);
  d_file.insert_comment(func_name[6]);
  s_data.create_data_size(81,81); // わざと大きくしてある
  total1 = Nistk::MathTools::gaussian_3D_sum(3.0, 0.0, -10.0, 10.0, 
					 -10.0, 10.0, 0.5, 0.5, 2.0, true);
  total2 = Nistk::MathTools::gaussian_3D_sum(3.0, 0.0, -10.0, 10.0, 
					 -10.0, 10.0, 0.5, 0.5, 3.0, true);
  Nistk::MathTools::simdata_from_DoG(&s_data, 3.0, 0.0, 0.5, 0.5, 
				                 1.0, 1.0, 2.0, 3.0, true);
  // SimData型の配列の左上角の座標の計算
  xd = -((double)(s_data.get_width()/2))*0.5; 
  yd = -((double)(s_data.get_height()/2))*0.5;
  // ファイルに書き込み
  d_file.data_from_simdata(&s_data, xd, 0.5, yd, 0.5);
  d_file.close();
  // SimDataからpixbufを生成して表示
  m_pixbuf = Nistk::ImageData::create_pixbuf_new(s_data.get_width(),
						      s_data.get_height());
  Nistk::ImageData::pixbuf_from_simdata(&s_data, m_pixbuf);
  window.set_name(win_name);
  window.set_image(m_pixbuf);
  Gtk::Main::run(window);
 
  return 0;
}
