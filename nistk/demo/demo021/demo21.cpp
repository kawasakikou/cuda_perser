#include<iostream>
#include<cmath>
#include<nistk.h>

#define PLOT_VIEW_NUM 7   // PlotViewの数
#define DT 0.01           // 時間の刻み幅(単位:msec)
#define FROM_T 0.0        // 開始時刻(単位:msec)
#define END_T 20.0        // 終了時刻(単位:msec)
#define FROM_INJ 1.0      // 電流注入開始時刻(単位:msec)
#define END_INJ 1.5       // 電流注入終了時刻(単位:msec)
#define INJ_VAL 0.0004    // 電流値(単位:uA)

int main(int argc, char **argv)
{
 // gtkmmを使う際には必ず必要。まず、最初にやる。
  Gtk::Main kit(argc,argv);
  // オブジェクトの宣言
  Nistk::NistkMainWin main_window;
  Nistk::Neuron::HHVariable hhv;
  Nistk::Neuron::HHParameter hhp;
  // PlotView用名前の文字列の配列
  char plot_name[PLOT_VIEW_NUM][25] = {"HHNeuron-V_rest=0[mV]",
				       "HHNeuron-V_rest=-60[mV]",
				       "para-mnh","alpha_m-n",
				       "HHNeuron-MKS","para-mnh2",
				       "alpha_m-n2"};
  char file_name[][15] = {"test1.dat", "test2.dat", "test3.dat",
			  "test4.dat", "test5.dat"};
  double t,dt;
  double v,av,bv,mv,hv,nv;
  double scale;
  int i;

  main_window.set_size_request(700,300);
  // PlotViewの生成と名前のセット
  main_window.create_plotview(PLOT_VIEW_NUM);
  for(i = 0; i < PLOT_VIEW_NUM; i++) 
    main_window.set_plotview_name(i,plot_name[i]);

 // グラフを横2個、縦2個として縦横の大きさとマージンを設定
  for(i = 0; i < PLOT_VIEW_NUM; i++){
    main_window.get_plotview_ptr(i)->set_graph_area(2,2,200,100,80,30);
    main_window.pd_ptr(i,0,0)->create_data_region(1);
    main_window.pd_ptr(i,1,0)->create_data_region(1);
    main_window.pd_ptr(i,0,1)->create_data_region(3);
    main_window.pd_ptr(i,1,1)->create_data_region(2);
  }

  // dtの設定
  dt = DT;
  // 静止膜電位 0mV
  Nistk::Neuron::HHNeuron::init(&hhv, &hhp);  // 初期化
  std::cout << "V_rest = " << hhp.V_rest << "[mV] \n";
  std::cout << "T_scale = " << hhp.T_scale << '\n';
  std::cout << "C_m = " << hhp.C_m << '\n';
  std::cout << "G_m = " << hhp.G_m << '\n';
  std::cout << "G_Na = " << hhp.G_Na << '\n';
  std::cout << "G_K = " << hhp.G_K << '\n';
  std::cout << "V_L = " << hhp.V_L << '\n';
  std::cout << "m(0) = " << hhv.m << '\n';
  std::cout << "h(0) = " << hhv.h << '\n';
  std::cout << "n(0) = " << hhv.n << "\n \n";
  // 計算をして格納 
  for(t = FROM_T; t <= END_T; t += dt){
    if( (t >= FROM_INJ) && (t <= END_INJ)) hhv.I_inj = INJ_VAL;
    else hhv.I_inj = 0.0;
    // 値を格納
    // 入力電流
    main_window.pd_ptr(0,0,0)->push_back_data(0, t, hhv.I_inj);
    // 膜電位
    main_window.pd_ptr(0,1,0)->push_back_data(0, t, hhv.V);
    // 係数m,h,n
    main_window.pd_ptr(0,0,1)->push_back_data(0, t, hhv.m);
    main_window.pd_ptr(0,0,1)->push_back_data(1, t, hhv.h);
    main_window.pd_ptr(0,0,1)->push_back_data(2, t, hhv.n);
    // I_Na,I_K
    main_window.pd_ptr(0,1,1)->push_back_data(0, t, hhv.I_Na);
    main_window.pd_ptr(0,1,1)->push_back_data(1, t, hhv.I_K);
    
    // 計算
    Nistk::Neuron::HHNeuron::calc(dt, &hhv, &hhp);
  }

  // 静止膜電位 -60mV
  hhp.V_rest = -60;   // 静止膜電位を-60[mV]に設定
  hhp.E_Na += -60;    // 併せてE_Naも変更
  hhp.E_K += -60;     // 併せてE_Kも変更
  Nistk::Neuron::HHNeuron::init(&hhv, &hhp); // 初期化
  std::cout << "V_rest = " << hhp.V_rest << "[mV] \n";
  std::cout << "T_scale = " << hhp.T_scale << '\n';
  std::cout << "C_m = " << hhp.C_m << '\n';
  std::cout << "G_m = " << hhp.G_m << '\n';
  std::cout << "G_Na = " << hhp.G_Na << '\n';
  std::cout << "G_K = " << hhp.G_K << '\n';
  std::cout << "V_L = " << hhp.V_L << '\n';
  std::cout << "m(0) = " << hhv.m << '\n';
  std::cout << "h(0) = " << hhv.h << '\n';
  std::cout << "n(0) = " << hhv.n << "\n \n";
  // 計算をして格納 
  for(t = FROM_T; t <= END_T; t += dt){
    if( (t >= FROM_INJ) && (t <= END_INJ)) hhv.I_inj = INJ_VAL;
    else hhv.I_inj = 0.0;
    // 値を格納
    // 入力電流
    main_window.pd_ptr(1,0,0)->push_back_data(0, t, hhv.I_inj);
    // 膜電位
    main_window.pd_ptr(1,1,0)->push_back_data(0, t, hhv.V);
    // 係数m,h,n
    main_window.pd_ptr(1,0,1)->push_back_data(0, t, hhv.m);
    main_window.pd_ptr(1,0,1)->push_back_data(1, t, hhv.h);
    main_window.pd_ptr(1,0,1)->push_back_data(2, t, hhv.n);
    // I_Na,I_K
    main_window.pd_ptr(1,1,1)->push_back_data(0, t, hhv.I_Na);
    main_window.pd_ptr(1,1,1)->push_back_data(1, t, hhv.I_K);
    
    // 計算
    Nistk::Neuron::HHNeuron::calc(dt, &hhv, &hhp);
  }  
  
  // m,h,nの電圧依存性
  for(v=-40.0; v <= 140; v+=0.1){
    // m
    av = Nistk::Neuron::HHNeuron::calc_alpha_m(v, &hhp);
    bv = Nistk::Neuron::HHNeuron::calc_beta_m(v, &hhp);
    mv = av /(av + bv);
    // h
    av = Nistk::Neuron::HHNeuron::calc_alpha_h(v, &hhp);
    bv = Nistk::Neuron::HHNeuron::calc_beta_h(v, &hhp);
    hv = av /(av + bv);
    // n
    av = Nistk::Neuron::HHNeuron::calc_alpha_n(v, &hhp);
    bv = Nistk::Neuron::HHNeuron::calc_beta_n(v, &hhp);
    nv = av /(av + bv);
    // 値を格納
    main_window.pd_ptr(2,0,0)->push_back_data(0, v, mv);
    main_window.pd_ptr(2,1,0)->push_back_data(0, v, hv);
    main_window.pd_ptr(2,1,1)->push_back_data(0, v, nv);
    main_window.pd_ptr(2,0,1)->push_back_data(0, v, mv);
    main_window.pd_ptr(2,0,1)->push_back_data(1, v, hv);
    main_window.pd_ptr(2,0,1)->push_back_data(2, v, nv);
  }
  main_window.pd_ptr(2,0,0)->save_data(0,file_name[0]);
  main_window.pd_ptr(2,1,0)->save_data(0,file_name[1]);
  main_window.pd_ptr(2,1,1)->save_data(0,file_name[2]);

  // α_m,α_n
  for(v=-40.0; v <= 140; v+=1){
    // α_m
    mv = Nistk::Neuron::HHNeuron::calc_alpha_m(v, &hhp);
    // α_n
    nv = Nistk::Neuron::HHNeuron::calc_alpha_n(v, &hhp);
    main_window.pd_ptr(3,0,0)->push_back_data(0, v, mv);
    main_window.pd_ptr(3,1,0)->push_back_data(0, v, nv);
  }
  main_window.pd_ptr(3,0,0)->save_data(0,file_name[3]);
  main_window.pd_ptr(3,1,0)->save_data(0,file_name[4]);

  // 単位 V,A,S,F,sec
  // 静止膜電位 -60mV
  // パラメータの設定変更
  scale = pow(10,-3);
  hhp.V_rest = hhp.V_rest * scale;
  hhp.E_Na = hhp.E_Na * scale;
  hhp.E_K = hhp.E_K * scale;
  hhp.C_m_bar = 0.01 * hhp.C_m_bar;
  hhp.G_Na_bar = 10.0 * hhp.G_Na_bar;
  hhp.G_K_bar = 10.0 * hhp.G_K_bar;
  hhp.G_m_bar = 10.0 * hhp.G_m_bar;
  hhp.area_size = hhp.area_size * pow(10,-4);
  Nistk::Neuron::HHNeuron::convert_mhn_to_MSAV(&hhp);  // m,h,n,α,βの変換
  Nistk::Neuron::HHNeuron::init(&hhv, &hhp);           // 初期化
  std::cout << "V_rest = " << hhp.V_rest << "[V] \n";
  std::cout << "T_scale = " << hhp.T_scale << '\n';
  std::cout << "area_size = " << hhp.area_size << '\n';
  std::cout << "C_m = " << hhp.C_m << '\n';
  std::cout << "G_m = " << hhp.G_m << '\n';
  std::cout << "G_Na = " << hhp.G_Na << '\n';
  std::cout << "G_K = " << hhp.G_K << '\n';
  std::cout << "V_L = " << hhp.V_L << '\n';
  std::cout << "E_Na = " << hhp.E_Na << '\n';
  std::cout << "E_K = " << hhp.E_K << '\n';
  std::cout << "m(0) = " << hhv.m << '\n';
  std::cout << "h(0) = " << hhv.h << '\n';
  std::cout << "n(0) = " << hhv.n << "\n \n";
  // 計算をして格納
  dt = dt * scale;
  std::cout << "dt = " << dt << "\n \n";
  for(t = (FROM_T * scale); t <= (END_T * scale); t += dt){
    if((t >= (FROM_INJ * scale)) && (t <= (END_INJ * scale))) hhv.I_inj = 0.4 * pow(10,-9);
    else hhv.I_inj = 0.0;
    // 値を格納
    // 入力電流
    main_window.pd_ptr(4,0,0)->push_back_data(0, t, hhv.I_inj);
    // 膜電位
    main_window.pd_ptr(4,1,0)->push_back_data(0, t, hhv.V);
    // 係数m,h,n
    main_window.pd_ptr(4,0,1)->push_back_data(0, t, hhv.m);
    main_window.pd_ptr(4,0,1)->push_back_data(1, t, hhv.h);
    main_window.pd_ptr(4,0,1)->push_back_data(2, t, hhv.n);
    // I_Na,I_K
    main_window.pd_ptr(4,1,1)->push_back_data(0, t, hhv.I_Na);
    main_window.pd_ptr(4,1,1)->push_back_data(1, t, hhv.I_K);
    
    // 計算
    Nistk::Neuron::HHNeuron::calc(dt, &hhv, &hhp);
  }

  // m,h,nの電圧依存性
  for(v=-0.04; v <= 0.140; v+=0.0001){
    // m
    av = Nistk::Neuron::HHNeuron::calc_alpha_m(v, &hhp);
    bv = Nistk::Neuron::HHNeuron::calc_beta_m(v, &hhp);
    mv = av /(av + bv);
    // h
    av = Nistk::Neuron::HHNeuron::calc_alpha_h(v, &hhp);
    bv = Nistk::Neuron::HHNeuron::calc_beta_h(v, &hhp);
    hv = av /(av + bv);
    // n
    av = Nistk::Neuron::HHNeuron::calc_alpha_n(v, &hhp);
    bv = Nistk::Neuron::HHNeuron::calc_beta_n(v, &hhp);
    nv = av /(av + bv);
    // 値を格納
    main_window.pd_ptr(5,0,0)->push_back_data(0, v, mv);
    main_window.pd_ptr(5,1,0)->push_back_data(0, v, hv);
    main_window.pd_ptr(5,1,1)->push_back_data(0, v, nv);
    main_window.pd_ptr(5,0,1)->push_back_data(0, v, mv);
    main_window.pd_ptr(5,0,1)->push_back_data(1, v, hv);
    main_window.pd_ptr(5,0,1)->push_back_data(2, v, nv);
  }

  // α_m,α_n
  for(v=-0.04; v <= 0.140; v+=0.0001){
    // α_m
    mv = Nistk::Neuron::HHNeuron::calc_alpha_m(v, &hhp);
    // α_n
    nv = Nistk::Neuron::HHNeuron::calc_alpha_n(v, &hhp);
    main_window.pd_ptr(6,0,0)->push_back_data(0, v, mv);
    main_window.pd_ptr(6,1,0)->push_back_data(0, v, nv);
  }

 // メインウィンドウを起動して表示
  Gtk::Main::run(main_window);

  return 0;
}

