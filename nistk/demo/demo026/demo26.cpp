#include<iostream>
#include<nistk.h>
#include<cstdio>

#define X_NUM 2
#define Y_NUM 3

int main()
{
  Nistk::SimData data1,data2;
  Nistk::SimDataArray data3,data4;
  bool flag;
  char filename[] = "test.dat";
  char filename2[] = "test2.dat";
  double num,num2;
  int i,j,k,l;

  std::cout << "\n*** DataIO: SimData check \n";
  std::cout << "*DataIO: SimData check. save check \n";
  data1.create_data_size(X_NUM, Y_NUM);
  for(i = 0; i < Y_NUM; i++){
    for(j = 0; j < X_NUM; j++){
      std::cout << "data[" << i << "][" << j << "]=";
      std::cin >> num;
      data1.put_data(j, i, num);
      printf("num=%15.13lf \n",num);
    }
  }
  flag = Nistk::DataIO::save_simdata(&data1, filename);
  std::cout << "Output flag=" << flag << '\n';
  std::cout << "\n*DataIO: SimData check. Load check \n";
  flag = Nistk::DataIO::load_simdata(&data2, filename);
  for(i = 0; i < Y_NUM; i++){
    for(j = 0; j < X_NUM; j++){
      num = data2.get_data(j, i);
      std::cout << "data[" << i << "][" << j << "]=";
      std::cout << num;
      printf(" num=%15.13lf \n",num);
    }
  }
  std::cout << "Input flag=" << flag << "\n\n\n";

  std::cout << "***DataIO: SimDataArray check \n";
  std::cout << "*DataIO: SimDataArray check. save check \n";
  data3.create_data_size(X_NUM, Y_NUM, X_NUM, Y_NUM);
  num = 0.0;
  for(i = 0; i < Y_NUM; i++){
    for(j = 0; j < X_NUM; j++){
      std::cout << "position=" << i << ' ' <<j << '\n';
      // 各SimData(位置(j,i))の値を表示
      for(k = 0; k < Y_NUM; k++){
	for(l = 0; l < X_NUM; l++){
	  data3.put_data(j,i,l,k,num);
	  num2 = data3.get_data(j,i,l,k);
	  std::cout << num2 << ' ';
	  num += 0.0001;
	}
	std::cout << '\n';
      }
      std::cout << '\n';
    }
  }
  flag = Nistk::DataIO::save_array(&data3, filename2);
  std::cout << "Output flag=" << flag << '\n';

  std::cout << "\n*DataIO: SimData check. Load check \n";
  flag = Nistk::DataIO::load_array(&data4, filename2);
  for(i = 0; i < Y_NUM; i++){
    for(j = 0; j < X_NUM; j++){
      std::cout << "position=" << i << ' ' <<j << '\n';
      // 各SimData(位置(j,i))の値を表示
      for(k = 0; k < Y_NUM; k++){
	for(l = 0; l < X_NUM; l++){
	  num = data4.get_data(j,i,l,k);
	  std::cout << num << ' ';
	}
	std::cout << '\n';
      }
      std::cout << '\n';
    }
  }
  std::cout << "Input flag=" << flag << '\n';

  return 0;
}
