#include<iostream>
#include<cmath>
#include<nistk.h>

#define PLOT_VIEW_NUM 5   // PlotViewの数
#define DT 0.1            // 時間の刻み幅(単位:msec)
#define FROM_T 0.0        // 開始時刻(単位:msec)
#define END_T 250.0       // 終了時刻(単位:msec)
// 注入電流は最初膜電位とリカバリ変数の安定化のため時間をおいた方が良い
#define FROM_INJ 50.0     // 電流注入開始時刻(単位:msec)
#define END_INJ 250.0     // 電流注入終了時刻(単位:msec)
#define INJ_VAL 10        // 電流値(単位:uA)

int main(int argc, char **argv)
{
 // gtkmmを使う際には必ず必要。まず、最初にやる。
  Gtk::Main kit(argc,argv);
  // オブジェクトの宣言
  Nistk::NistkMainWin main_window;
  Nistk::Neuron::IVVariable ivv;
  Nistk::Neuron::IVParameter ivp;
  // PlotView用名前の文字列の配列
  char plot_name[PLOT_VIEW_NUM][15] = {"RS_neuron", "IB_neuron",
				       "CH_neuron", "FS_neuron",
				       "LTS_neuron"};
  char file_name[][15] = {"RS_v.dat", "RS_I.dat", "IB_v.dat", "IB_I.dat",
			  "CH_v.dat", "CH_I.dat", "FS_v.dat", "FS_I.dat",
			  "LTS_v.dat", "LTS_I.dat"};
  double t,dt;
  int i;

  main_window.set_size_request(700,150);
  // PlotViewの生成と名前のセット
  main_window.create_plotview(PLOT_VIEW_NUM);
  for(i = 0; i < PLOT_VIEW_NUM; i++) 
    main_window.set_plotview_name(i,plot_name[i]);

 // グラフを横2個、縦1個として縦横の大きさとマージンを設定
  for(i = 0; i < PLOT_VIEW_NUM; i++){
    main_window.get_plotview_ptr(i)->set_graph_area(2,1,300,100,80,30);
    main_window.pd_ptr(i,0,0)->create_data_region(2);
    main_window.pd_ptr(i,1,0)->create_data_region(1);
  }

  // dtの設定
  dt = DT;

  // 興奮性ニューロン
  // RS(Regular Spiking) neuron
  ivp.d = 8;
  Nistk::Neuron::IVNeuron::init(&ivv, &ivp);  // 初期化
  // 計算をして格納 
  for(t = FROM_T; t <= END_T; t += dt){
    if( (t >= FROM_INJ) && (t <= END_INJ)) ivv.I = INJ_VAL;
    else ivv.I = 0.0;
    // 値を格納
    // 膜電位
    main_window.pd_ptr(0,0,0)->push_back_data(0, t, ivv.v);
    // 入力電流
    main_window.pd_ptr(0,0,0)->push_back_data(1, t, ivv.I - 100.0);
    // リカバリ変数
    main_window.pd_ptr(0,1,0)->push_back_data(0, t, ivv.u);
    // 計算
    Nistk::Neuron::IVNeuron::calc(dt, &ivv, &ivp);
    // 活動電位の最大値をv_resetで揃える
    if(ivv.v >= ivp.v_rest) ivv.v = ivp.v_rest;
  }
  main_window.pd_ptr(0,0,0)->save_data(0,file_name[0]);
  main_window.pd_ptr(0,0,0)->save_data(1,file_name[1]);
  // パラメータをデフォルトに戻す
  ivp.d = 2;

  // IB(Intrinsically Bursting) neuron
  ivp.c = -55;
  ivp.d = 4;
  Nistk::Neuron::IVNeuron::init(&ivv, &ivp);  // 初期化
  // 計算をして格納 
  for(t = FROM_T; t <= END_T; t += dt){
    if( (t >= FROM_INJ) && (t <= END_INJ)) ivv.I = INJ_VAL;
    else ivv.I = 0.0;
    // 値を格納
    // 膜電位
    main_window.pd_ptr(1,0,0)->push_back_data(0, t, ivv.v);
    // 入力電流
    main_window.pd_ptr(1,0,0)->push_back_data(1, t, ivv.I - 100.0);
    // リカバリ変数
    main_window.pd_ptr(1,1,0)->push_back_data(0, t, ivv.u);
    // 計算
    Nistk::Neuron::IVNeuron::calc(dt, &ivv, &ivp);
    // 活動電位の最大値をv_resetで揃える
    if(ivv.v >= ivp.v_rest) ivv.v = ivp.v_rest;
  }
  main_window.pd_ptr(1,0,0)->save_data(0,file_name[2]);
  main_window.pd_ptr(1,0,0)->save_data(1,file_name[3]);
  // パラメータをデフォルトに戻す
  ivp.c = -65;
  ivp.d = 2;

  // CH(chattering) neuron
  ivp.c = -50;
  Nistk::Neuron::IVNeuron::init(&ivv, &ivp);  // 初期化
  // 計算をして格納 
  for(t = FROM_T; t <= END_T; t += dt){
    if( (t >= FROM_INJ) && (t <= END_INJ)) ivv.I = INJ_VAL;
    else ivv.I = 0.0;
    // 値を格納
    // 膜電位
    main_window.pd_ptr(2,0,0)->push_back_data(0, t, ivv.v);
    // 入力電流
    main_window.pd_ptr(2,0,0)->push_back_data(1, t, ivv.I - 100.0);
    // リカバリ変数
    main_window.pd_ptr(2,1,0)->push_back_data(0, t, ivv.u);
    // 計算
    Nistk::Neuron::IVNeuron::calc(dt, &ivv, &ivp);
    // 活動電位の最大値をv_restで揃える
    if(ivv.v >= ivp.v_rest) ivv.v = ivp.v_rest;
  }
  main_window.pd_ptr(2,0,0)->save_data(0,file_name[4]);
  main_window.pd_ptr(2,0,0)->save_data(1,file_name[5]);
  // パラメータをデフォルトに戻す
  ivp.c = -65;

  // 抑制性ニューロン
  // FS(Fast Spiking) neuron
  ivp.a = 0.1;
  Nistk::Neuron::IVNeuron::init(&ivv, &ivp);  // 初期化
  // 計算をして格納 
  for(t = FROM_T; t <= END_T; t += dt){
    if( (t >= FROM_INJ) && (t <= END_INJ)) ivv.I = INJ_VAL;
    else ivv.I = 0.0;
    // 値を格納
    // 膜電位
    main_window.pd_ptr(3,0,0)->push_back_data(0, t, ivv.v);
    // 入力電流
    main_window.pd_ptr(3,0,0)->push_back_data(1, t, ivv.I - 100.0);
    // リカバリ変数
    main_window.pd_ptr(3,1,0)->push_back_data(0, t, ivv.u);
    // 計算
    Nistk::Neuron::IVNeuron::calc(dt, &ivv, &ivp);
    // 活動電位の最大値をv_restで揃える
    if(ivv.v >= ivp.v_rest) ivv.v = ivp.v_rest;
  }
  main_window.pd_ptr(3,0,0)->save_data(0,file_name[6]);
  main_window.pd_ptr(3,0,0)->save_data(1,file_name[7]);
  // パラメータをデフォルトに戻す
  ivp.a = 0.02;

  // LTS(Low-Threshold Spiking) neuron
  ivp.b = 0.25;
  Nistk::Neuron::IVNeuron::init(&ivv, &ivp);  // 初期化
  // 計算をして格納 
  for(t = FROM_T; t <= END_T; t += dt){
    if( (t >= FROM_INJ) && (t <= END_INJ)) ivv.I = INJ_VAL;
    else ivv.I = 0.0;
    // 値を格納
    // 膜電位
    main_window.pd_ptr(4,0,0)->push_back_data(0, t, ivv.v);
    // 入力電流
    main_window.pd_ptr(4,0,0)->push_back_data(1, t, ivv.I - 100.0);
    // リカバリ変数
    main_window.pd_ptr(4,1,0)->push_back_data(0, t, ivv.u);
    // 計算
    Nistk::Neuron::IVNeuron::calc(dt, &ivv, &ivp);
    // 活動電位の最大値をv_restで揃える
    if(ivv.v >= ivp.v_rest) ivv.v = ivp.v_rest;
  }
  main_window.pd_ptr(4,0,0)->save_data(0,file_name[8]);
  main_window.pd_ptr(4,0,0)->save_data(1,file_name[9]);
  // パラメータをデフォルトに戻す
  ivp.b = 0.2;

  // メインウィンドウを起動して表示
  Gtk::Main::run(main_window);

  return 0;
}

