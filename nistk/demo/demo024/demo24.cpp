#include<iostream>
#include<gtkmm.h>
#include<nistk.h>

#define IMAGE_VIEW_NUM 1
#define I_WIDTH  101       // 荷重の幅
#define I_HEIGHT 101       // 荷重の高さ
#define STEP 0.1           // 計算の刻み幅
#define PAI 3.141592

int main(int argc,char *argv[])
{
  // gtkmmを使う際には必ず必要。まず、最初にやる。
  Gtk::Main kit(argc,argv);
  // オブジェクトの宣言
  Nistk::NistkMainWin main_window;
  Nistk::DataFileOut d_file;
  // PlotView用名前の文字列の配列
  char image_name[IMAGE_VIEW_NUM][15] = {"input_test"};
  Glib::RefPtr<Gdk::Pixbuf> in_image;   // 画像用pixbuf
  Nistk::SimData in_data;               // simdat
  Nistk::SimData in_data2;               // simdat2
  Nistk::SimData in_data3;               // simdat3
  double x, y;
  double x_start, y_start;
  int x_end, y_end;
  double buf;
  int i,j;

  main_window.set_size_request(200,150);
  // ImageViewの生成と名前のセット
  main_window.create_imageview(IMAGE_VIEW_NUM);
  for(i = 0; i < IMAGE_VIEW_NUM; i++) 
    main_window.set_imageview_name(i,image_name[i]);
  in_data.create_data_size(I_WIDTH,I_HEIGHT);
  in_data2.create_data_size(I_WIDTH,I_HEIGHT);
  in_data3.create_data_size(I_WIDTH,I_HEIGHT);

  /*** 
   * gaussian2_3Dを計算してin_dataに格納してそれを表示
  ***/
  // 開始位置と終了位置の計算
  x_start = -(I_WIDTH/2) * STEP; 
  y_start = (I_HEIGHT/2) * STEP;
  x_end = in_data.get_width();
  y_end = in_data.get_height();
  // 計算して格納
  y = y_start;
  for(i = 0; i < y_end; i++){
    x = x_start;
    for(j = 0; j < x_end; j++){
      buf = Nistk::MathTools::e_gaussian(x, y, -2.0, 1.0, 
					 1.0, 2.0, 0.5, (PAI/180.0) * 45);
      in_data.put_data(j, i, buf);
      x += STEP;
    }
    y -= STEP;
  }
  // ファイルに格納
  d_file.open("test1.dat",true);
  d_file.data_from_simdata(&in_data, x_start, STEP, y_start, -STEP);
  d_file.close();
  // simdataの内容を表示
  in_image = Nistk::ImageData::create_pixbuf_new(in_data.get_width(),
						  in_data.get_height());
  Nistk::ImageData::pixbuf_from_simdata(&in_data,in_image);
  main_window.get_imageview_ptr(0)->set_image(in_image);
  Gtk::Main::run(main_window);
  std::cout << "pass 1\n";

  /***
    * simdata_from_e_gaussianを利用してin_dataに格納してそれを表示
  ***/
  // 前のデータに計算モードmaxで重ねて表示
  Nistk::MathTools::simdata_from_e_gaussian(&in_data, 1.0, -1.0, 
					    STEP, STEP, 1.0, 2.0, 0.5, 
					    (PAI/180.0) * 135, "max");
  Nistk::ImageData::pixbuf_from_simdata(&in_data,in_image);
  main_window.get_imageview_ptr(0)->set_image(in_image);
  Gtk::Main::run(main_window);
  std::cout << "pass 2\n";
  // 0.0で初期化して、中央に表示
  Nistk::MathTools::init_simdata(&in_data, 0.0);
  Nistk::MathTools::simdata_from_e_gaussian(&in_data, 0.0, 0.0, 
					    STEP, STEP, 1.0, 2.0, 0.5, 
					    (PAI/180.0) * 135, "none");
  Nistk::ImageData::pixbuf_from_simdata(&in_data,in_image);
  main_window.get_imageview_ptr(0)->set_image(in_image);
  Gtk::Main::run(main_window);
  std::cout << "pass 3\n";
  // 違う大きさのもので引き算して表示(ゲインも0.5に変更)
  Nistk::MathTools::simdata_from_e_gaussian(&in_data, 0.0, 0.0, 
					    STEP, STEP, 0.5, 2.0, 1.0, 
					    (PAI/180.0) * 135, "-");
  Nistk::ImageData::pixbuf_from_simdata(&in_data,in_image);
  main_window.get_imageview_ptr(0)->set_image(in_image);
  Gtk::Main::run(main_window);
  std::cout << "pass 4\n";

  /***
    * simdata_from_e_gaussianを利用してin_dataに格納してそれを表示(その2)
  ***/
  // 初期化
  Nistk::MathTools::init_simdata(&in_data, 0.0);
  Nistk::MathTools::init_simdata(&in_data2, 0.0);
  Nistk::MathTools::init_simdata(&in_data3, 0.0);
  // 計算して正規化
  Nistk::MathTools::simdata_from_e_gaussian(&in_data, 0.0, 0.0, 
					    STEP, STEP, 1.0, 2.0, 0.5, 
					    (PAI/180.0) * 135, "none");
  Nistk::MathTools::simdata_from_e_gaussian(&in_data2, 0.0, 0.0, 
					    STEP, STEP, 1.0, 2.3, 1.0, 
					    (PAI/180.0) * 135, "none");
  Nistk::MathTools::normalize_simdata(&in_data);
  Nistk::MathTools::normalize_simdata(&in_data2);
  Nistk::ImageData::pixbuf_from_simdata(&in_data,in_image);
  main_window.get_imageview_ptr(0)->set_image(in_image);
  Gtk::Main::run(main_window);
  Nistk::ImageData::pixbuf_from_simdata(&in_data2,in_image);
  main_window.get_imageview_ptr(0)->set_image(in_image);
  Gtk::Main::run(main_window);
  // in_dataからin_data2を減算してin_data3に格納
  Nistk::MathTools::calc_simdata(&in_data3, &in_data, &in_data2, "-");
  // 合計が0となっているか確認
  buf = 0.0;
  x_end = in_data3.get_width();
  y_end = in_data3.get_height();
  y = y_start;
  for(i = 0; i < y_end; i++){
    for(j = 0; j < x_end; j++){
      buf += in_data3.get_data(j, i);
    }
  }
  std::cout << "total = " << buf << '\n';
  Nistk::ImageData::pixbuf_from_simdata(&in_data3,in_image);
  main_window.get_imageview_ptr(0)->set_image(in_image);
  Gtk::Main::run(main_window);
  std::cout << "pass 5\n";

  return 0;
}

