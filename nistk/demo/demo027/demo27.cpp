#include<iostream>
#include<gtkmm.h>
#include<nistk.h>

// 画像サイズは幅225,高さ233
#define W_WIDTH  51       // 荷重の幅
#define W_HEIGHT 51       // 荷重の高さ
#define OUT_WIDTH  87     // 出力の幅
#define OUT_HEIGHT 91     // 出力の高さ
#define CALC_STEP 2       // 荷重和計算の間隔
#define SIGMOID_THETA 2.0 // シグモイド関数の閾値
#define SIGMOID_GAIN  3.0 // シグモイド関数のゲイン

int main(int argc,char *argv[])
{
  // gtkmmを使う際には必ず必要。まず、最初にやる。
  Gtk::Main kit(argc,argv);
  // オブジェクトの宣言
  Nistk::NistkMainWin main_window;      // Nistkメインウィンドウ
  Glib::RefPtr<Gdk::Pixbuf> in_image;   // 入力画像用pixbuf
  Glib::RefPtr<Gdk::Pixbuf> image_buf;  // テンポラリ用pixbuf
  Nistk::SimData in_data;               // 入力画像用simdat
  Nistk::SimData weight_data;           // 荷重用simdata
  Nistk::SimData out_data;              // 計算結果用simdata
  int x,y;                              // 入力上の位置
  char win_name[][25] = {"input data", "weight data", "out data",
			 "out_data(limited calc)"};
  double tmp;
  int i,j;

  // メインウィンドウの設定
  main_window.create_imageview(4);
  main_window.set_imageview_name(0,win_name[0]);
  main_window.set_imageview_name(1,win_name[1]);
  main_window.set_imageview_name(2,win_name[2]);
  main_window.set_imageview_name(3,win_name[3]);

  /*** 
   * 画像を入力してin_dataに格納してそれを表示
  ***/
  // 入力画像をpixbufに格納
  in_image = Gdk::Pixbuf::create_from_file("../image/test-fig2.png");
  // pixbufから入力画像をsimdataに格納
  in_data.create_data_size(in_image->get_width(),in_image->get_height());
  Nistk::ImageData::simdata_from_pixbuf(&in_data,in_image,2);
  std::cout << "input data  ";
  std::cout << "width=" << in_data.get_width() << ' '
	    << "height=" <<in_data.get_height() << '\n';
  // simdataの内容を表示
  image_buf = Nistk::ImageData::create_pixbuf_new(in_data.get_width(),
						  in_data.get_height());
  Nistk::ImageData::pixbuf_from_simdata(&in_data,image_buf);
  main_window.get_imageview_ptr(0)->set_image(image_buf);
  std::cout << "pass 1 \n";

  /*** 
   * 荷重データを作成 ON-CENTER
  ***/
  // 領域確保
  weight_data.create_data_size(W_WIDTH,W_HEIGHT);
  // 荷重データ作成
  Nistk::MathTools::simdata_from_DoG(&weight_data, 0.0, 0.0,  // 中心
				     1.0, 1.0,                // x,yのステップ
				     1.0, 1.0,                // ゲイン
				     13.0, 18.0, true);         // シグマ
  // 荷重データを表示
  std::cout << "weight data  ";
  std::cout << "width=" << weight_data.get_width() << ' '
	    << "height=" <<weight_data.get_height() << '\n';
  image_buf = Nistk::ImageData::resize_pixbuf(image_buf, W_WIDTH,W_HEIGHT);
  Nistk::ImageData::pixbuf_from_simdata(&weight_data, image_buf);
  main_window.get_imageview_ptr(1)->set_image(image_buf); 
  std::cout << "pass 2 \n";

  /*** 
   * 荷重和を計算して格納
  ***/
  // 領域確保
  out_data.create_data_size(OUT_WIDTH,OUT_HEIGHT);
  // 荷重和の計算
  for(i = 0; i < out_data.get_height(); i++){  // y
    for(j = 0; j < out_data.get_width(); j++){  // x
      // 入力上での位置計算
      x = CALC_STEP * j + (int)(W_WIDTH/2);
      y = CALC_STEP * i + (int)(W_HEIGHT/2);
      // 荷重和を計算してシグモイド関数を通して格納
      tmp = Nistk::Calc::weighted_sum(&in_data, &weight_data, x, y);
      tmp = Nistk::MathTools::sigmoid_func(tmp-SIGMOID_THETA,SIGMOID_GAIN);
      out_data.put_data(j,i,tmp);
    }
  }
  // 計算結果を表示
  std::cout << "out data  ";
  std::cout << "width=" << out_data.get_width() << ' '
	    << "height=" << out_data.get_height() << '\n';
  image_buf = Nistk::ImageData::resize_pixbuf(image_buf, OUT_WIDTH,OUT_HEIGHT);
  Nistk::ImageData::pixbuf_from_simdata(&out_data, image_buf);
  main_window.get_imageview_ptr(2)->set_image(image_buf); 
  std::cout << "pass 3 \n";

  // 荷重和の範囲を限定(荷重のONの範囲に限定しているので白塗りになるはず)
  // 荷重和の計算
  for(i = 0; i < out_data.get_height(); i++){  // y
    for(j = 0; j < out_data.get_width(); j++){  // x
      // 入力上での位置計算
      x = CALC_STEP * j + (int)(W_WIDTH/2);
      y = CALC_STEP * i + (int)(W_HEIGHT/2);
      // 荷重和を計算してシグモイド関数を通して格納
      //tmp = weighted_sum2(&in_data, &weight_data, x, y,
      //		  -(W_WIDTH/2), W_WIDTH/2, -(W_HEIGHT/2), W_HEIGHT/2);
      tmp = Nistk::Calc::weighted_sum2(&in_data, &weight_data, x, y, 
				                        -13, 13, -13, 13);
      tmp = Nistk::MathTools::sigmoid_func(tmp-SIGMOID_THETA,SIGMOID_GAIN);
      out_data.put_data(j,i,tmp);
    }
  }
  // 計算結果を表示
  std::cout << "out data  ";
  std::cout << "width=" << out_data.get_width() << ' '
	    << "height=" << out_data.get_height() << '\n';
  image_buf = Nistk::ImageData::resize_pixbuf(image_buf, OUT_WIDTH,OUT_HEIGHT);
  Nistk::ImageData::pixbuf_from_simdata(&out_data, image_buf);
  main_window.get_imageview_ptr(3)->set_image(image_buf);
  std::cout << "pass 4 \n";
  Gtk::Main::run(main_window);


  return 0;
}
