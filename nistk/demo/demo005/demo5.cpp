#include<iostream>
#include<randgene.h>


int main(int argc,char *argv[])
{
  Nistk::RandGene m_rand;
  
  // ノーマルの乱数の発生(範囲はコンパイラ次第)
  for(int i=0; i<10; i++){
    std::cout << "get_rand=" << m_rand.get_rand() << '\n';
  }

  // startからendまでの範囲の乱数を発生させる
  for(int i=0; i<30 ; i++){
    std::cout << "get_rand_i=" << m_rand.get_rand_i(5,25,8) << '\n';
  }

  // 0から1までの実数型乱数の発生
  for(int i=0; i<10; i++){
    std::cout << "get_rand_d=" << m_rand.get_rand_d() << '\n';
    }

  return 0;
}
