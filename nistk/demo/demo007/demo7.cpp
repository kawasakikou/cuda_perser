// SimDataArrayクラスのテストデモ
#include<iostream>
#include<gtkmm.h>
#include<nistk.h>
#include"imagearray.h"
#include"simdataarray.h"

void test_copy_constructer(Nistk::SimDataArray in);
void test_pointer(Nistk::SimData *in);

int main(int argc,char *argv[])
{
  Nistk::SimDataArray t_data,t_data2;
  double value,out_data;
  int a_width,a_height,s_width,s_height;
  int buf;

  // SimData型配列(2×3)を作り、各SimDataのサイズを(2×3)
  // として,そこに０から順番に値を代入し,表示
  std::cout << "pass 1 \n";
  t_data.create_data_size(3,2,3,2);
  a_width = t_data.get_width();
  a_height = t_data.get_height();
  value = 0.0;
  for(int i=0; i<a_height; i++){
    for(int j=0; j<a_width; j++){
      s_width = t_data.get_array_width(j,i);
      s_height = t_data.get_array_height(j,i);
      std::cout << "   position=" << i << ' ' <<j << '\n';
      std::cout << "     width=" << s_width << " height="<< s_height << '\n';
      // 各SimData(位置(j,i))の値を表示
      for(int k=0; k<s_height; k++){
	for(int l=0; l<s_width; l++){
	  t_data.put_data(j,i,l,k,value);
	  out_data = t_data.get_data(j,i,l,k);
	  std::cout << out_data << ' ';
	  value += 1.0;
	}
	std::cout << '\n';
      }
      std::cout << '\n';
    }
  }

  // 大きさ変更
  // SimData型配列(2×2)を作り、各SimDataのサイズを(2×2)
  // として,そこに０から順番に値を代入し,表示
  std::cout << "pass 2 \n";
  t_data.recreate_data_size(2,2,2,2);
  a_width = t_data.get_width();
  a_height = t_data.get_height();

  std::cout << "     test of recreate \n";
  value = 0.0;
  for(int i=0; i<a_height; i++){
    for(int j=0; j<a_width; j++){
      s_width = t_data.get_array_width(j,i);
      s_height = t_data.get_array_height(j,i);
      std::cout << "   position=" << i << ' ' << j << '\n';
      // 各SimData(位置(j,i))の値を表示
      for(int k=0; k<s_height; k++){
	for(int l=0; l<s_width; l++){
	  t_data.put_data(j,i,l,k,value);
	  out_data = t_data.get_data(j,i,l,k);
	  std::cout << out_data << ' ';
	  value += 1.0;
	}
	std::cout << '\n';
      }
      std::cout << '\n';
    }
  }

  // 代入演算子オーバーロードを使ってpass2と同じ事をする
  std::cout << "pass 3 \n";
  t_data2 = t_data;
  std::cout << "    test of t_data2=t_data \n";
  a_width = t_data2.get_width();
  a_height = t_data2.get_height();
  value = 0.0;
  for(int i=0; i<a_height; i++){
    for(int j=0; j<a_width; j++){
      s_width = t_data2.get_array_width(j,i);
      s_height = t_data2.get_array_height(j,i);
      std::cout << "   position=" << i << ' ' << j << '\n';
      // 各SimData(位置(j,i))の値を表示
      for(int k=0; k<s_height; k++){
	for(int l=0; l<s_width; l++){
	  t_data2.put_data(j,i,l,k,value);
	  out_data = t_data2.get_data(j,i,l,k);
	  std::cout << out_data << ' ';
	  value += 1.0;
	}
	std::cout << '\n';
      }
      std::cout << '\n';
    }
  }


  std::cout << '\n';

  // コピーコンストラクタのテスト
  // ポインタではなくオブジェクトそのものを関数に渡す
  std::cout << "pass 4 \n";
  std::cout << "    test of copy constructer \n";
  test_copy_constructer(t_data);

  // 位置(0,0)のSimDataのポインタを関数に渡すテスト
  std::cout << "pass 5 \n";
  std::cout << "    test of pointer \n";
  test_pointer(t_data.get_simdata_ptr(0,0));

  return 0;
}

void test_copy_constructer(Nistk::SimDataArray in)
{
  double out_data;
  int a_width,a_height,s_width,s_height;
  a_width = in.get_width();
  a_height = in.get_height();

  for(int i=0; i<a_height; i++){
    for(int j=0; j<a_width; j++){
      s_width = in.get_array_width(j,i);
      s_height = in.get_array_height(j,i);
      std::cout << "   position=" << i << ' ' << j << '\n';
      // 各SimData(位置(j,i))の値を表示
      for(int k=0; k<s_height; k++){
	for(int l=0; l<s_width; l++){
	  out_data = in.get_data(j,i,l,k);
	  std::cout << out_data << ' ';
	}
	std::cout << '\n';
      }
      std::cout << '\n';
    }
  }
  std::cout << '\n';

  return;
}

void test_pointer(Nistk::SimData *in)
{
  double out_data;
  int length;
  
  length = in->get_length();
  for(int i=0; i<length; i++){
          out_data = in->get_data_1d(i);
	  std::cout << out_data << ' ';
  }
  std::cout << '\n';

  return;
}
