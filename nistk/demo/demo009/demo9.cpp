// SimDataArrayクラスのテストデモ
#include<iostream>
#include<gtkmm.h>
#include<nistk.h>

int main(int argc,char *argv[])
{
 // gtkmmを使う際には必ず必要。まず、最初にやる。
  Gtk::Main kit(argc,argv);
  // オブジェクトの宣言
  Nistk::SimDataArray t_data;
  Nistk::ImageArray array_window;
  Nistk::SimData s_data,*simdata_ptr;
  Nistk::RandGene t_rand;
  Glib::RefPtr<Gdk::Pixbuf> m_pixbuf;
  Nistk::ImageView i_window;
  // 変数の宣言
  int s_width,s_height,a_width_num,a_height_num;
  int x_c,y_c;
  char array_name[][15] = {"test_array", "test_array2"};
  char win_name[] = "single test";

  // SimData型配列(10×10)を作り、各SimDataのサイズを(201×201)
  // として生成,配列の幅と高さを取得
  t_data.create_data_size(10,10,201,201);
  s_width = t_data.get_width();
  s_height = t_data.get_height();
  std::cout << "pass 0-1 \n";
  // １つひとつSimDataのポインタを取り出して中心をランダムとして
  // DoGを格納
  for(int y=0; y<s_height; y++){
    for(int x=0; x<s_height; x++){
      // DoGの中心の決定(ランダム)
      x_c = t_rand.get_rand_i(0,100,6);
      y_c = t_rand.get_rand_i(0,100,6);
      // ポインタの取得
      simdata_ptr = t_data.get_simdata_ptr(x,y);
      // DoGの格納
      Nistk::MathTools::simdata_from_DoG(simdata_ptr, (double)x_c, 
					 (double)y_c, 1.0, 1.0, 1.0, 
					 1.0, 5.0, 10.0, true);
    }
  }
  std::cout << "pass 0-2 \n";
  // SimDataArrayをImageArrayに格納
  array_window.array_from_simdataarray(&t_data,10,1.0,s_width);
  std::cout << "pass 1 \n";
  // アレイウィンドウの表示
  array_window.set_array_name(array_name[0]);
  Gtk::Main::run(array_window);
  std::cout << "pass 2 \n";

  // SimDataArrayを使わずにSimDataを使ってImageArrayに張り付け
  // (今回は同じものを張り付ける)
  // アレイ(5×5)を作り、描画領域のサイズを(201×201)
  // として生成
  array_window.create_array_area(201,201,10,5,5);
  a_width_num = array_window.get_array_w_num();
  a_height_num = array_window.get_array_h_num();
  // データの作成
  s_data.create_data_size(201,201);
  x_c=50; y_c=50;
  Nistk::MathTools::simdata_from_DoG(&s_data, (double)x_c, 
				     (double)y_c, 1.0, 1.0, 1.0, 
				     1.0, 5.0, 10.0, true);
  // とりあえず表示してみる
  m_pixbuf = Nistk::ImageData::create_pixbuf_new(s_data.get_width(),
                                                  s_data.get_height());
  Nistk::ImageData::pixbuf_from_simdata(&s_data,m_pixbuf);
  i_window.set_name(win_name);
  i_window.set_image(m_pixbuf);
  Gtk::Main::run(i_window);
  // 同じSimDataをアレイウィンドウに格納
  for(int y=0; y<a_height_num; y++){
    for(int x=0; x<a_width_num; x++){
      array_window.set_simdata_to_array(&s_data,x,y);
    }
  }
 std::cout << "pass 3 \n";
  // アレイウィンドウの表示
  array_window.set_array_name(array_name[1]);
  Gtk::Main::run(array_window);
  std::cout << "pass 4 \n";

  return 0;
}


