// ImageArrayクラスのテストデモ
#include<iostream>
#include<gtkmm.h>
#include<nistk.h>
#include"imagearray.h"

int main(int argc,char *argv[])
{
  // gtkmmを使う際には必ず必要。まず、最初にやる。
  Gtk::Main kit(argc,argv);
  // オブジェクトの宣言
  Glib::RefPtr<Gdk::Pixbuf> m_pixbuf,m_pixbuf2;
  Nistk::ImageLoader i_loader;
  Nistk::ImageView window;
  Nistk::ImageArray array_window;
  // 変数の宣言
  char w_name[] = "test0";
  char array_name[] = "test_array";
  char i_file_name[] = "i_file3.txt";
  char save_file_name[] = "test-array";
  char file_type[] = "png";
  int f_num;

  // 画像リストファイルを読み込む
  // そして、ファイル数をf_numにセット
  i_loader.set_input_file(i_file_name);
  f_num=i_loader.get_input_file_num();

  // ローダを利用して画像ファイルを
  // 読み込んで表示
  for(int i=0; i<f_num; i++){
    m_pixbuf = i_loader.load_image_next();
    window.set_name(w_name);
    window.set_image(m_pixbuf);
    Gtk::Main::run(window);
  }
  std::cout << "pass 1 \n";
  array_window.set_array_name(array_name);
  array_window.array_from_file(i_loader,10,0.1,6);
  Gtk::Main::run(array_window);
  std::cout << "pass 2 \n";

  // ローダを使わずに一つずつ画像を張り付け
  // アレイウィンドウをリサイズ
  array_window.create_array_area(300,200,10,3,2);
  // 画像を張り付け
  for(int i=0; i<2; i++){
    for(int j=0; j<3; j++){
      m_pixbuf = i_loader.load_image_next();
      m_pixbuf = Nistk::ImageData::resize_pixbuf(m_pixbuf,300,200);
      array_window.set_pixbuf_to_array(m_pixbuf,j,i);
    }
  }
  Gtk::Main::run(array_window);
  std::cout << "pass 3 \n";

  // アレイ画像をセーブ
  m_pixbuf2 = array_window.get_pixbuf(m_pixbuf2);
  Nistk::ImageData::save_pixbuf(m_pixbuf2,save_file_name,file_type);
  return 0;
}
