#include<iostream>
#include<gtkmm.h>
// #include<imageview.h>
// #include<imagedata.h>
// #include<simdata.h>
#include<nistk.h>

int main(int argc,char *argv[])
{
  // gtkmmを使う際には必ず必要。まず、最初にやる。
  Gtk::Main kit(argc,argv);
  // オブジェクトの宣言
  Glib::RefPtr<Gdk::Pixbuf> m_pixbuf,m_pixbuf2;
  Nistk::GrayData s_data;
  char win_name[] = "test_window";

  // ファイルよりpixbufを生成してウィンドウを生成と同時に
  // 張り付ける。そして,イベント処理を開始。
  m_pixbuf=Gdk::Pixbuf::create_from_file("../image/test-fig.jpg");
  Nistk::ImageView window(m_pixbuf);
  window.set_name(win_name);
  Gtk::Main::run(window);
  std::cout << "pass 1 \n";

  // pixbufよりグレイデータを生成し、高さと幅を表示
  // まずは、画像のサイズよりグレイデータクラスの
  // サイズを設定する必要がある。
  s_data.create_data_size(m_pixbuf->get_width(),m_pixbuf->get_height());
  std::cout << "pass 2 width=" << m_pixbuf->get_width() << " height=" 
	    <<m_pixbuf->get_height() << '\n';
  Nistk::ImageData::graydata_from_pixbuf(&s_data,m_pixbuf,2);
  std::cout << "pass 3 \n";

  // 新しく空のpixbufを大きさを指定して生成
  // ここでは、グレイデータの大きさに設定
  m_pixbuf2 = Nistk::ImageData::create_pixbuf_new(s_data.get_width(),
						  s_data.get_height());
  std::cout << "pass 4 width=" << m_pixbuf2->get_width() << " height=" 
	    << m_pixbuf2->get_height() << '\n';

  // グレイデータをpixbufに張り付け
  Nistk::ImageData::pixbuf_from_graydata(&s_data,m_pixbuf2);
  window.set_image(m_pixbuf2);
  Gtk::Main::run(window);
  std::cout << "pass 4-2 \n";

  // 大きさを1/4に変更
  m_pixbuf2=Nistk::ImageData::resize_pixbuf(m_pixbuf2, 
	     m_pixbuf2->get_width()/4, m_pixbuf2->get_height()/4);
  std::cout << "pass 5 "<< m_pixbuf2->get_width() << " height=" 
	    << m_pixbuf2->get_height() << '\n';

  // ウィンドウに新しいpixbufをセットして、イベント処理を開始
  window.set_image(m_pixbuf2);
  Gtk::Main::run(window);
  std::cout << "pass 6 \n";

  return 0;
}
