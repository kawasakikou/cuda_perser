#include<iostream>
#include<gtkmm.h>
// #include<imageview.h>
// #include<imagedata.h>
// #include<simdata.h>
#include<nistk.h>

int main(int argc,char *argv[])
{
  // gtkmmを使う際には必ず必要。まず、最初にやる。
  Gtk::Main kit(argc,argv);
  // オブジェクトの宣言
  Glib::RefPtr<Gdk::Pixbuf> m_pixbuf,m_pixbuf2;
  Nistk::ImageLoader i_loader;
  Nistk::SimData rgb_data[3];
  Nistk::ImageView window;
  // 変数の宣言
  char w_name[] = "test0";
  char file_path[] = "../image/i_file.txt";
  int f_num;

  // 画像リストファイルを読み込む
  // そして、ファイル数をf_numにセット
  i_loader.set_input_file(file_path);
  f_num=i_loader.get_input_file_num();

  // ローダを利用して画像ファイルを
  // 読み込んで表示
  for(int i=0; i<f_num; i++){
    m_pixbuf = i_loader.load_image_next();
    window.set_name(w_name);
    window.set_image(m_pixbuf);
    Gtk::Main::run(window);
  }

  // pixbufよりグレイデータを生成し、高さと幅を表示
  // まずは、画像のサイズよりグレイデータクラスの
  // サイズを設定する必要がある。
  rgb_data[0].create_data_size(m_pixbuf->get_width(),m_pixbuf->get_height());
  rgb_data[1].create_data_size(m_pixbuf->get_width(),m_pixbuf->get_height());
  rgb_data[2].create_data_size(m_pixbuf->get_width(),m_pixbuf->get_height());
  // 生成したデータクラスの大きさの表示
  std::cout << "pass 2 width=" << m_pixbuf->get_width() << " height=" 
	    <<m_pixbuf->get_height() << '\n';
  // pixbufからRGBデータを生成
  Nistk::ImageData::simdata_from_pixbuf_RGB(&rgb_data[0],&rgb_data[1],
					             &rgb_data[2],m_pixbuf);
  std::cout << "pass 3 \n";
  
  // 新しく空のpixbufを大きさを指定して生成
  // ここでは、グレイデータの大きさに設定
  m_pixbuf2 = Nistk::ImageData::create_pixbuf_new(rgb_data[0].get_width(),
                                                  rgb_data[0].get_height());
  // RGBデータをループを使ってそれぞれpixbufに張り付け表示
  for(int i=0; i<3; i++){
    Nistk::ImageData::pixbuf_from_simdata_RGB(&rgb_data[i],m_pixbuf2,i);
    window.set_image(m_pixbuf2);
    Gtk::Main::run(window);
  }
  std::cout << "pass 4 \n";

  // 最後にRGB全てを張り付ける
  Nistk::ImageData::pixbuf_from_simdata_RGB_full(&rgb_data[0],&rgb_data[1],
					             &rgb_data[2],m_pixbuf2);
  window.set_image(m_pixbuf2);
  Gtk::Main::run(window);
  std::cout << "pass 5 \n";
  return 0;
}
