#include<iostream>
#include<nistk/tools.h>

int main()
{
  double max_data,min_data,data_diff,data; // データ用
  double round_off_data;                   // 指定次数で切り捨てたデータ
  int order;                               // オーダ用
  int max_order;                           // max_dataの最大次数
  char o_str[20];                          // データを文字列に変換した結果の格納用

  // データ入力
  std::cout << "max_data=";
  std::cin >> max_data;
  std::cout << "min_data=";
  std::cin >> min_data;

  // max_dataとmin_dataの差をもとめ、そのオーダを求める
  data_diff = max_data - min_data;
  order = Nistk::Tools::calc_order(data_diff);
  // max_dataのオーダを求める
  max_order = Nistk::Tools::calc_order(max_data);
  // 表示
  std::cout << "data_diff=" << data_diff << '\n';
  std::cout << "data_diff order=" << order << '\n';
  std::cout << "max_data order=" << max_order << '\n';
  
  // orderで指定した次数でmax_dataを切り捨てて表示
  round_off_data = Nistk::Tools::round_off_data(max_data,order);
  std::cout << "round off max_data=" << round_off_data << '\n';

  // round_off_dataを文字列に変換して表示
  Nistk::Tools::num_to_str(round_off_data, o_str, max_order, order, 11, false);
  std::cout << "round off max_data string =" << o_str << '\n';

  // データを入力し、指数系で文字列に変換して表示
  std::cout << "exp part of data=";
  std::cin >> data;
  order = Nistk::Tools::calc_order(data);
  std::cout << " exp part of data order" << order <<'\n';
  Nistk::Tools::num_to_str(data, o_str, order, order-3, 11, true);
  std::cout << "exp mode data string =" << o_str << '\n';

  return 0;
}

