FROM debian
MAINTAINER kawasaki kouji

LABEL version="1.0"
LABEL description="First image with Dockerfile."


# install basic apps, one per line for better caching
RUN apt-get update && apt-get install -y \
    automake \
    build-essential \
    dpkg-sig \
    libgtkmm-2.4-dev \
    libboost-all-dev \
    vim \
    gdb \
 && apt-get clean \
 && rm -rf /var/lib/apt/lists/*

# cleanup
RUN apt-get -qy autoremove

# add the application to the container
COPY nistk /nistk
RUN  mkdir app
RUN  mkdir user_app
COPY dotfiles/ /root/

RUN cd /nistk/src && \
    make          && \
    make install  && \
    make clean
