hi Pmenu ctermbg=255 ctermfg=0 guifg=#000000 guibg=#999999
hi PmenuSel ctermbg=blue ctermfg=black
hi PmenuSbar ctermbg=0 ctermfg=9
hi PmenuSbar ctermbg=255 ctermfg=0 guifg=#000000 guibg=#FFFFFF

" vimにcoffeeファイルタイプを認識させる
au BufRead,BufNewFile,BufReadPre *.coffee   set filetype=coffee


autocmd ColorScheme * highlight rubyModule ctermfg=200
autocmd ColorScheme * highlight rubyClass ctermfg=180

hi Comment ctermfg=gray
autocmd! FileType markdown hi! def link markdownItalic LineNr

let g:markdown_syntax_conceal = ''
