[ -f ~/.path ] && . ~/.path

export PATH=${PATH}:/app/bin

alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../../..'
alias .....='cd ../../../..'
alias ht='ls -a'
alias htt='ls -lta'
alias htg='ls|grep'
